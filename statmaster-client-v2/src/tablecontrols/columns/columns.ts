import { Action, ActionStatus, ActionTypeKey } from '../../core/actions';
import { Category, Topic } from 'statmaster-common';

export interface ColumnState {
  columnGroups: Set<Topic>;
}

export const initialColumnState: ColumnState = {
  columnGroups: new Set([Topic.RESOURCE, Topic.OPPOSING])
};

export type ColumnAction = ToggleColumnGroupAction;

export interface ToggleColumnGroupAction extends Action {
  topic: Topic;
  enabled: boolean;
}

export const makeToggleColumnGroupAction = (topic: Topic, enabled: boolean): ToggleColumnGroupAction => {
  return {
    type: ActionTypeKey.TOGGLE_COLUMN_GROUP,
    state: ActionStatus.NO_STATE,
    topic,
    enabled
  };
};

export const columnsReducer = (
  state: ColumnState = initialColumnState,
  action: ColumnAction): ColumnState => {
  switch (action.type) {
    case ActionTypeKey.TOGGLE_COLUMN_GROUP:
      return toggleColumnGroup(state, action as ToggleColumnGroupAction);
    default:
      return state;
  }
};

const toggleColumnGroup = (state: ColumnState, action: ToggleColumnGroupAction): ColumnState => {
  let newGroups: Set<Topic> = new Set(state.columnGroups);
  if (action.enabled) {
    newGroups.add(action.topic);
  } else {
    newGroups.delete(action.topic);
  }
  return { columnGroups: newGroups };
};

export const createTopicDisplay = (topic: Topic): string => {
  let displayName;
  switch (topic) {
    case Topic.RESOURCE:
      displayName = 'Team';
      break;
    case Topic.OPPOSING:
      displayName = `Opponent`;
      break;
    case Topic.RESOURCE_VS_OPPOSING:
      displayName = `Team vs. Opp.`;
      break;
    case Topic.OPPOSING_VS_RESOURCE:
      displayName = `Opp. vs.Team`;
      break;
    default:
      displayName = 'Team';
  }
  return displayName;
};

export const createCategoryDisplay = (category: Category): string => {
  return category[0].toUpperCase() + category.slice(1);
};

export const createHeaderLabel = (topic: Topic, label: string): string => {
  let displayName;
  switch (topic) {
    case Topic.RESOURCE:
      displayName = label;
      break;
    case Topic.OPPOSING:
      displayName = `Opp. ${label}`;
      break;
    case Topic.RESOURCE_VS_OPPOSING:
      displayName = `${label} vs. Opp.`;
      break;
    case Topic.OPPOSING_VS_RESOURCE:
      displayName = `Opp. ${label} vs.`;
      break;
    default:
      displayName = label;
  }
  return displayName;
};
