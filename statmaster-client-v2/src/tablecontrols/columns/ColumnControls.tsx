import * as React from 'react';
import { Topic, Topics } from 'statmaster-common';
import { createTopicDisplay } from './columns';
import './ColumnControls.css';

export interface ColumnControlsProps {
  columnGroups: Set<Topic>;
  onColumnChecked: (topic: Topic, checked: boolean) => void;
}

export class ColumnControls extends React.Component<ColumnControlsProps> {
  static defaultColumnsProps: ColumnControlsProps = {
    columnGroups: new Set(),
    onColumnChecked: (t, c) => null
  };

  createCheckBox = (topic: Topic, checked: boolean, onChecked: () => void) => {
    let label = createTopicDisplay(topic);
    return (
      <label>
        <input
          type="checkbox"
          value={label}
          checked={checked}
          onChange={onChecked}
        />
        {label}
      </label>
    );
  }

  render() {
    let { columnGroups, onColumnChecked } = this.props;
    return (
      <div className="column-controls">
        <strong>Visible Stat groups:</strong>
        {
          Topics.map(topic => {
            let checked = columnGroups.has(topic);
            return (
              <div key={topic}>
                {this.createCheckBox(topic, checked, () => onColumnChecked(topic, !checked))}
              </div>
            );
          })
        }
      </div>
    );
  }
}
