import { Action, ActionStatus, ActionTypeKey } from '../../core/actions';
import {
  Category,
  Field,
  FieldKey,
  FieldValue,
  ResourceId,
  Sort,
  SortStyle,
  Topic,
  makeFieldKey
} from 'statmaster-common';

import ResourceContainer from '../../statsdata/ResourceContainer';
import { createHeaderLabel } from '../columns/columns';
import { isEqual } from 'lodash';

export function getSortDisplay(sort: Sort, field: Field | undefined): string {
  let style = sort.style === SortStyle.ASCENDING ? 'ascending' : 'descending';

  if (field === undefined) {
    return 'Unknown';
  }
  return `${createHeaderLabel(sort.columnKey.topic, field.label)}, ${style}`;
}

export const DEFAULT_SORT: Sort = { 
  columnKey: makeFieldKey(Topic.RESOURCE, Category.INFO, 'name'),
  style: SortStyle.DESCENDING
};

export interface SortState {
  sort: Sort;
}

export const DEFAULT_STATE: SortState = {
  sort: DEFAULT_SORT
};

export interface UpdateSortAction extends Action {
  columnKey: FieldKey;
}

export const sortResourceContainers = 
(containers: Map<ResourceId, ResourceContainer>, sort: Sort): ResourceContainer[] => {
  return Array.from(containers.values()).sort((a, b) => compareContainers(a, b, sort));
};

export const compareContainers = (a: ResourceContainer, b: ResourceContainer, sort: Sort): number => {
  let aTableData = a.data.get(sort.columnKey.flatKey);
  let aVal = aTableData !== undefined ? aTableData.value : undefined;
  let bTableData = b.data.get(sort.columnKey.flatKey);
  let bVal = bTableData !== undefined ? bTableData.value : undefined;
  return compareAllowNull(aVal, bVal, sort.style === SortStyle.DESCENDING);
};

export const compareAllowNull = (aVal?: FieldValue, bVal?: FieldValue, reverse?: boolean): number => {
  let flip = reverse ? -1 : 1;
  if (aVal === null || aVal === undefined) {
    return 1;
  }

  if (bVal === null || bVal === undefined) {
    return -1;
  }

  if (aVal === bVal) {
    return 0;
  }

  if (aVal < bVal) {
    return -1 * flip;
  }

  if (aVal > bVal) {
    return 1 * flip;
  }

  throw new Error('Should not reach here');
};

export const makeUpdateSortAction = (columnKey: FieldKey): UpdateSortAction => {
  return {
    type: ActionTypeKey.UPDATE_SORT,
    state: ActionStatus.NO_STATE,
    columnKey
  };
};

export const sortReducer = (currentState: SortState = {sort: DEFAULT_SORT}, action: UpdateSortAction): SortState => {
  switch (action.type) {
    case ActionTypeKey.UPDATE_SORT:
      return updateSort(currentState, action);
    default: 
      return currentState;
  }
};

const updateSort = (state: SortState, action: UpdateSortAction): SortState => {
  // reverse the sort if currently sorted on that column otherwise default to desc
  let newStyle: SortStyle = SortStyle.DESCENDING;
  let newColumnKey = action.columnKey;
  if (state.sort 
    && isEqual(state.sort.columnKey, newColumnKey) 
    && state.sort.style === SortStyle.DESCENDING) {
    newStyle = state.sort.style = SortStyle.ASCENDING;
  }
  return {
    sort: {
      columnKey: newColumnKey,
      style: newStyle
    }
  };
};
