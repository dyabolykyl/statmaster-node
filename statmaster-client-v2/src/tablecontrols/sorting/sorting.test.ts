import * as tdf from '../../util/testDataFactory';

import { ActionStatus, ActionTypeKey } from '../../core/actions';
import { Category, Sort, SortStyle, Topic, makeFieldKey } from 'statmaster-common';
import {
  DEFAULT_SORT,
  DEFAULT_STATE,
  SortState,
  UpdateSortAction,
  compareAllowNull,
  compareContainers,
  makeUpdateSortAction,
  sortReducer
} from './sorting';

import ResourceContainer from '../../statsdata/ResourceContainer';

describe('SortState', () => {
  test('initial state is empty', () => {
    let action: UpdateSortAction = {
      type: ActionTypeKey.UPDATE_FILTER,
      state: ActionStatus.FAILED,
      columnKey: makeFieldKey(Topic.RESOURCE, Category.INFO, 'a')
    };

    let state: SortState = sortReducer(undefined, action);
    expect(state.sort).toEqual(DEFAULT_SORT);
  });

  test('update sort with new key', () => {
    let action: UpdateSortAction = makeUpdateSortAction(makeFieldKey(Topic.RESOURCE, Category.INFO, 'a'));
    let state: SortState = sortReducer(DEFAULT_STATE, action);

    expect(state.sort).toBeDefined();
    let sort: Sort = state.sort as Sort;
    expect(sort.columnKey.flatKey).toBe('resource_info_a');
    expect(sort.style).toBe(SortStyle.DESCENDING);
  });

  test('update sort with same key', () => {
    let action: UpdateSortAction = makeUpdateSortAction(makeFieldKey(Topic.RESOURCE, Category.INFO, 'a'));
    let state: SortState = sortReducer(DEFAULT_STATE, action);
    
    // same action
    let sameAction: UpdateSortAction = makeUpdateSortAction(makeFieldKey(Topic.RESOURCE, Category.INFO, 'a'));        
    state = sortReducer(state, sameAction);

    expect(state.sort).toBeDefined();
    let sort: Sort = state.sort as Sort;
    expect(sort.columnKey.flatKey).toBe('resource_info_a');
    expect(sort.style).toBe(SortStyle.ASCENDING);

    // same action again (different instance)
    let sameActionAgain: UpdateSortAction = makeUpdateSortAction(makeFieldKey(Topic.RESOURCE, Category.INFO, 'a'));    
    state = sortReducer(state, sameActionAgain);
    sort = state.sort as Sort;
    expect(sort.style).toBe(SortStyle.DESCENDING);
    expect(sort.columnKey.flatKey).toBe('resource_info_a');
  });
});

describe('compare', () => {
  test('equal', () => {
    expect(compareAllowNull(0, 0, false)).toBe(0);
    expect(compareAllowNull(1, 1, true)).toBe(0);
  });

  test('less than', () => {
    expect(compareAllowNull(0, 1, false)).toBe(-1);
    expect(compareAllowNull(0, 1, true)).toBe(1);
  });

  test('greater than', () => {
    expect(compareAllowNull(1, -1, false)).toBe(1);
    expect(compareAllowNull(1, -1, true)).toBe(-1);
  });

  test('undefined', () => {
    expect(compareAllowNull(undefined, -1, false)).toBe(1);
    expect(compareAllowNull(undefined, -1, true)).toBe(1);
    expect(compareAllowNull(1, undefined, false)).toBe(-1);
    expect(compareAllowNull(1, undefined, true)).toBe(-1);
  });
});

describe('compareTableRowItems', () => {
  test('compare rows less than', () => {
    let sort: Sort = tdf.makeSort('city', SortStyle.ASCENDING);
    let a = new ResourceContainer(tdf.arrangeTeam('a'));
    let b = new ResourceContainer(tdf.arrangeTeam('b'));

    let result = compareContainers(a, b, sort);
    expect(result).toBe(-1);

    sort.style = SortStyle.DESCENDING;
    result = compareContainers(a, b, sort);
    expect(result).toBe(1);
  });

  test('compare rows greater than', () => {
    let sort: Sort = tdf.makeSort('city', SortStyle.ASCENDING);
    let a = new ResourceContainer(tdf.arrangeTeam('b'));
    let b = new ResourceContainer(tdf.arrangeTeam('a'));

    let result = compareContainers(a, b, sort);
    expect(result).toBe(1);

    sort.style = SortStyle.DESCENDING;
    result = compareContainers(a, b, sort);
    expect(result).toBe(-1);
  });

  test('compare rows undefined', () => {
    let sort: Sort = { columnKey: makeFieldKey(Topic.OPPOSING, Category.INFO, 'city'), style: SortStyle.DESCENDING };
    let a = new ResourceContainer(tdf.arrangeTeam('a'));
    let b = new ResourceContainer(tdf.arrangeTeam('b'), tdf.arrangeTeam('o'));

    let result = compareContainers(a, b, sort);
    expect(result).toBe(1);
    result = compareContainers(b, a, sort);
    expect(result).toBe(-1);

    sort.style = SortStyle.ASCENDING;
    result = compareContainers(a, b, sort);
    expect(result).toBe(1);
    result = compareContainers(b, a, sort);
    expect(result).toBe(-1);
  });

});