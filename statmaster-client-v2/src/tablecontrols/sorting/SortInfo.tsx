import './SortInfo.css';

import * as React from 'react';

import { DEFAULT_SORT, getSortDisplay } from './sorting';
import { Field, FieldId, Sort } from 'statmaster-common';

export interface SortInfoProps {
  sort: Sort;
  fields: Map<FieldId, Field>;
}

export class SortInfo extends React.Component<SortInfoProps> {
  static defaultProps: SortInfoProps = {
    sort: DEFAULT_SORT,
    fields: new Map()
  };

  render() {
    let { sort, fields } = this.props;
    return (
      <div className="sort-info">
        <p><strong>Sort: </strong>Sorting on {getSortDisplay(sort, fields.get(sort.columnKey.fieldId))}</p>
      </div>
    );
  }
}
