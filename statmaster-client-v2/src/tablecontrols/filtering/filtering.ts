import { FilterExpression, FilterOperator, PLAYING_KEY } from 'statmaster-common';

import { createHeaderLabel } from '../columns/columns';

export function getFilterOperatorLabel(operator: FilterOperator): string {
  switch (operator) {
    case FilterOperator.EQUAL_TO:
      return 'is';
    case FilterOperator.NOT_EQUAL_TO:
      return 'is not';
    case FilterOperator.GREATER_THAN:
      return 'is more than';
    case FilterOperator.LESS_THAN:
      return 'is less than';
    default:
      return '?';
  }
}

export function getFilterDisplay(filter: FilterExpression): string {
  if (filter.matchesField(PLAYING_KEY)) {
    return `${filter.value === true ? 'P' : 'Not p'}laying Today`;
  }

  return `
  ${createHeaderLabel(filter.fieldKey.topic, filter.fieldKey.fieldId)} 
  ${getFilterOperatorLabel(filter.operator)} ${filter.value}
  `;
}
