import { Action, ActionStatus, ActionTypeKey } from '../../core/actions';
import { FieldKey, FilterExpression, FilterNode, FilterTree, FlatFilterTree } from 'statmaster-common';

import getLogger from '../../util/log';

const log = getLogger('filtering/reducer');

export interface FilterState {
  filterTree: FilterTree;
}

export const initialState: FilterState = {
  filterTree: new FlatFilterTree()
};

export interface AddFilterAction extends Action {
  filter: FilterExpression;
}

export interface UpdateFilterAction extends Action {
  filter: FilterNode;
}

export interface RemoveFilterAction extends Action {
  key: string;
}

export interface RemoveFiltersForFieldAction extends Action {
  key: FieldKey;
}

export type FilterAction = UpdateFilterAction | RemoveFilterAction |RemoveFiltersForFieldAction | AddFilterAction;

export function makeUpdateFilterAction(filter: FilterNode): UpdateFilterAction {
  return { type: ActionTypeKey.UPDATE_FILTER, state: ActionStatus.NO_STATE, filter };
}

export function makeRemoveFilterAction(filterKey: string): RemoveFilterAction {
  return { type: ActionTypeKey.REMOVE_FILTER, state: ActionStatus.NO_STATE, key: filterKey };
}

export function makeRemoveAllFiltersAction(field: FieldKey): RemoveFiltersForFieldAction {
  return { type: ActionTypeKey.REMOVE_FILTERS_FOR_FIELD, state: ActionStatus.NO_STATE, key: field };
}

export function makeAddFilterAction(filter: FilterExpression): AddFilterAction {
  return { type: ActionTypeKey.ADD_FILTER, state: ActionStatus.NO_STATE, filter };
}

export function filterReducer(state: FilterState = initialState, action: FilterAction) {
  switch (action.type) {
    case ActionTypeKey.UPDATE_FILTER:
      return updateFilter(state, action as UpdateFilterAction);
    case ActionTypeKey.REMOVE_FILTER:
      return removeFilter(state, action as RemoveFilterAction);
    case ActionTypeKey.REMOVE_FILTERS_FOR_FIELD:
      return removeFiltersForField(state, action as RemoveFiltersForFieldAction);
    case ActionTypeKey.ADD_FILTER:
      return addFilter(state, action as AddFilterAction);
    default:
      return state;
  }
}

function updateFilter(state: FilterState, action: UpdateFilterAction): FilterState {
  log.warn(`updateFilter is not supported`);
  return state;
}

function removeFilter(state: FilterState, action: RemoveFilterAction): FilterState {
  log.trace(`Removing filter on: ${action.key}`);
  let newFilterTree = state.filterTree.clone();
  newFilterTree.removeFilter(action.key);
  return { filterTree: newFilterTree };
}

function removeFiltersForField(state: FilterState, action: RemoveFiltersForFieldAction): FilterState {
  log.trace(`Removing filter on: ${action.key}`);
  let newFilterTree = state.filterTree.clone();
  newFilterTree.removeFiltersForField(action.key);
  return { filterTree: newFilterTree };
}

function addFilter(state: FilterState, action: AddFilterAction): FilterState {
  log.debug(`Adding filter: ${action.filter}`);
  let newFilterTree = state.filterTree.clone();
  newFilterTree.addFilter(action.filter);
  return { filterTree: newFilterTree };
}
