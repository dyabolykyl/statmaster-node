import { ActionStatus, ActionTypeKey } from '../../core/actions';
import { EMPTY_FILTER, PLAYING_KEY, filterOnPlayingExpression } from 'statmaster-common';
import { UpdateFilterAction, filterReducer, initialState } from './reducer';
import { makeAddFilterAction, makeRemoveAllFiltersAction } from './reducer';

import ResourceContainer from '../../statsdata/ResourceContainer';
import { arrangeTeam } from '../../util/test/stats';

test('initial state', () => {
  expect(initialState.filterTree.accept).toBeTruthy();
});

test('unknown action returns intial state', () => {
  let dummyAction: UpdateFilterAction = {
    type: ActionTypeKey.FETCH_STATS_IN_PROGRESS,
    state: ActionStatus.FAILED,
    filter: EMPTY_FILTER
  };

  let state = filterReducer(undefined, dummyAction);
  expect(state).toEqual(initialState);
});

test('toggle filter playing', () => {
  let filterOnPlaying = filterOnPlayingExpression(true);
  let filterAction = makeAddFilterAction(filterOnPlaying);
  let state = filterReducer(undefined, filterAction);

  let container: ResourceContainer = new ResourceContainer(arrangeTeam('1'), arrangeTeam('2'));
  let container2: ResourceContainer = new ResourceContainer(arrangeTeam('3'));
  expect(state.filterTree.accept(container.data)).toBeTruthy();
  expect(state.filterTree.accept(container2.data)).toBeFalsy();
  
  state = filterReducer(state, makeRemoveAllFiltersAction(PLAYING_KEY));
  expect(state.filterTree.accept(container.data)).toBeTruthy();
  expect(state.filterTree.accept(container2.data)).toBeTruthy();
});
