import './Filters.css';
import {
} from './filtering';

import * as React from 'react';
import * as _ from 'lodash';

import {
  Category,
  DataType,
  Field,
  FieldId,
  FieldKey,
  FieldValue,
  FilterExpression,
  FilterOperator,
  FilterOperators,
  PLAYING_KEY,
  Topic,
  Topics,
  filterOnPlayingExpression
} from 'statmaster-common';
import { Dispatch, connect } from 'react-redux';
import { FilterTree, FlatFilterTree, makeFieldKey } from 'statmaster-common';
import { getFields, getFilterOnPlaying, getFilterState } from '../selectors';
import { getFilterDisplay, getFilterOperatorLabel } from './filtering';
import { makeAddFilterAction, makeRemoveAllFiltersAction, makeRemoveFilterAction } from './reducer';

import { RootState } from '../../core/reducer';
import { createTopicDisplay } from '../columns/columns';
import getLogger from '../../util/log';

const log = getLogger('FilterControls');

interface StateProps {
  fields: Map<FieldId, Field>;
  filterTree: FilterTree;
  filterOnPlaying: boolean;
}

interface DispatchProps {
  toggleFilterOnPlaying: (filterOnPlaying: boolean) => void;
  addFilter: (key: FieldKey, operator: string, value: number) => void;
  removeFilter: (key: string) => void;
}

export function validateNumericalInput(value: string): boolean {
  if (value === undefined || value === null || value === '') {
    log.warn('Need to define a value');
    return false;
  }

  if (!/^\d*\.?\d+$/.test(value)) {
    log.warn('Value must be a number');
    return false;
  }

  return true;
}

export class SimpleFilterControls extends React.Component<StateProps & DispatchProps> {
  static defaultProps = {
    statFields: new Map(),
    filterTree: new FlatFilterTree(),
    filterOnPlaying: false,
    toggleFilterOnPlaying: (f: boolean) => null,
    addFilter: (key: FieldKey, operator: string, value: FieldValue) => null,
    removeFilter: (key: string) => null
  };
  private topic: HTMLSelectElement;
  private statId: HTMLSelectElement;
  private operator: HTMLSelectElement;
  private value: HTMLInputElement;

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    event.stopPropagation();
    let value = this.value.value;
    if (!validateNumericalInput(value)) {
      return;
    }
    
    let field = this.props.fields.get(this.statId.value) as Field;
    let category = field.category;
    let filterKey: FieldKey = makeFieldKey(this.topic.value as Topic, category, this.statId.value);
    this.props.addFilter(filterKey, this.operator.value, parseFloat(value));
  }

  createAddFilterForm = () => {
    return (
      <div className="add-filter-form">
        <span>Add New Filter: </span>
        <form onSubmit={this.handleSubmit}>
          <select name="topic" ref={topic => this.topic = topic as HTMLSelectElement}>
            {
              Topics.map(topic => {
                return (
                  <option key={topic} value={topic}>{createTopicDisplay(topic)}</option>
                );
              })
            }
          </select>
          <select name="field" ref={statId => this.statId = statId as HTMLSelectElement}>
            {
              Array.from(this.props.fields.values())
                .filter(field => field.category !== Category.INFO)
                .map(stat => {
                  return (
                    <option key={stat.id} value={stat.id} data-category={stat.category}>{stat.label}</option>
                  );
                })
            }
          </select>
          <select name="operator" ref={operator => this.operator = operator as HTMLSelectElement}>
            {
              FilterOperators.map(operator => {
                return (
                  <option
                    key={operator}
                    value={operator}
                  >{getFilterOperatorLabel(operator)}
                  </option>
                );
              })
            }
          </select>
          <input
            id="filter-value"
            type="text"
            name="value"
            placeholder="0.300"
            ref={value => this.value = value as HTMLInputElement}
          />
          <input type="submit" value="Add" />
        </form>
      </div>
    );
  }

  createFilterPlayingCheckBox = () => {
    let { filterOnPlaying, toggleFilterOnPlaying } = this.props;
    let label = `Playing today`;
    return (
      <div className="filter-playing-checkbox" >
        <label>
          <input
            type="checkbox"
            value={label}
            checked={filterOnPlaying}
            onChange={() => toggleFilterOnPlaying(!filterOnPlaying)}
          />
          {label}
        </label>
      </div>
    );
  }

  createFilterList() {
    let { filterTree } = this.props;
    return filterTree.getFilters().filter(filter => !_.isEqual(filter.fieldKey, PLAYING_KEY)).map(filter => {
      return (
        <div key={filter.key} className="filter-checkbox" >
          <label>
            <input
              type="checkbox"
              value={filter.key}
              checked={true}
              onChange={() => this.props.removeFilter(filter.key)}
            />
            {getFilterDisplay(filter)}
          </label>
        </div>
      );
    });
  }

  render() {
    return (
      <div className="filters">
        <p><strong>Filters: </strong></p>
        {this.createAddFilterForm()}
        {this.createFilterPlayingCheckBox()}
        {this.createFilterList()}
      </div>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => {
  let fields = getFields(state);
  let filterState = getFilterState(state);
  return {
    fields,
    filterTree: filterState.filterTree,
    filterOnPlaying: getFilterOnPlaying(state)
  };
};

const mapDispatchToProps = (dispatch: Dispatch<RootState>): DispatchProps => {
  return {
    toggleFilterOnPlaying: (filter: boolean): void => {
      if (filter) {
        dispatch(makeAddFilterAction(filterOnPlayingExpression(filter)));
      } else {
        dispatch(makeRemoveAllFiltersAction(PLAYING_KEY));
      }
    },
    addFilter: (key: FieldKey, operator: FilterOperator, value: FieldValue): void => {
      dispatch(makeAddFilterAction(new FilterExpression(key, operator, value, DataType.NUMBER)));
    },
    removeFilter: (key: string): void => {
      dispatch(makeRemoveFilterAction(key));
    }
  };
};

export const FilterControls = connect(mapStateToProps, mapDispatchToProps)(SimpleFilterControls);