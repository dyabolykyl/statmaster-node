import './TableControls.css';

import * as React from 'react';

import { Dispatch, connect } from 'react-redux';
import { FilterTree, FlatFilterTree, Sort, Topic } from 'statmaster-common';
import { SortInfo, SortInfoProps } from './sorting/SortInfo';
import { getColumnGroups, getFields, getFilterState } from './selectors';

import { ColumnControls } from './columns/ColumnControls';
import { FilterControls } from './filtering/FilterControls';
import { RootState } from '../core/reducer';
import { getSort } from '../statstable/selectors';
import { makeAddSelectorAction } from '../selectors/reducer';
import { makeToggleColumnGroupAction } from './columns/columns';

interface StateProps {
  sortInfoProps: SortInfoProps;
  columnGroups: Set<Topic>;
  filterTree: FilterTree;
}

interface DispatchProps {
  onColumnChecked: (topic: Topic, checked: boolean) => void;
  addSelector: (filterTree: FilterTree, feature: Sort) => void;
}

export class ConnectedTableControls extends React.Component<DispatchProps & StateProps> {
  static defaultProps: DispatchProps & StateProps = {
    sortInfoProps: SortInfo.defaultProps,
    columnGroups: ColumnControls.defaultColumnsProps.columnGroups,
    filterTree: new FlatFilterTree(),
    onColumnChecked: ColumnControls.defaultColumnsProps.onColumnChecked,
    addSelector: (filterTree: FilterTree, feature: Sort) => null,
  };

  renderAddSelectorButton(): JSX.Element {
    const { filterTree, sortInfoProps, addSelector} = this.props;
    return (
      <div className="add-selector">
        <button onClick={() => addSelector(filterTree, sortInfoProps.sort)}>Create Selector</button>
      </div>
    );
  }

  render() {
    let { sortInfoProps, columnGroups, onColumnChecked } = this.props;
    return (
      <div className="table-controls">
        <ColumnControls columnGroups={columnGroups} onColumnChecked={onColumnChecked}/>
        <SortInfo sort={sortInfoProps.sort} fields={sortInfoProps.fields} />
        <FilterControls />
        {this.renderAddSelectorButton()}
      </div>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => {
  let fields = getFields(state);
  let sortState = getSort(state);
  let filterTree = getFilterState(state).filterTree;
  return {
    sortInfoProps: { sort: sortState.sort, fields }, 
    columnGroups: getColumnGroups(state),
    filterTree
  };
};

const mapDispatchToProps = (dispatch: Dispatch<RootState>): DispatchProps => {
  return {
    onColumnChecked: (topic: Topic, checked: boolean): void => {
      dispatch(makeToggleColumnGroupAction(topic, checked));
    },
    addSelector: (filterTree: FilterTree, feature: Sort): void => {
      dispatch(makeAddSelectorAction(filterTree, feature));
    }
  };
};

export const TableControls = connect(mapStateToProps, mapDispatchToProps)(ConnectedTableControls);
