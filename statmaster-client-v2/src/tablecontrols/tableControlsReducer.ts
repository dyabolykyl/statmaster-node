import { FilterState, filterReducer } from './filtering/reducer';
import { SortState, sortReducer } from './sorting/sorting';
import { Reducer, combineReducers } from 'redux';
import { ColumnState, columnsReducer } from './columns/columns';

export interface TableState {
  columnState: ColumnState;
  filterState: FilterState;
  sortState: SortState;
}

export const tableStateReducer: Reducer<TableState> = combineReducers({
  columnState: columnsReducer,
  filterState: filterReducer,
  sortState: sortReducer,
});
