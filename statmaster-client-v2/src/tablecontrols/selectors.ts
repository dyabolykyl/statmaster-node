import {
  Category,
  Field,
  FieldId,
  FieldKey,
  Topic,
  Topics,
  isFilteredOnPlaying,
  makeFieldKey
} from 'statmaster-common';
import { FilterState, initialState } from './filtering/reducer';

import { COLUMN_WIDTH } from '../statstable/table/StatsTable';
import { RootState } from '../core/reducer';
import { SizedHeaderItem } from '../statstable/table/FixedTableHeaderRow';
import { createSelector } from 'reselect';
import getLogger from '../util/log';

const log = getLogger('filtering');

export const getColumnGroups = (state: RootState): Set<Topic> => {
  return state.tableState ? state.tableState.columnState.columnGroups : new Set();
};

export const getFilterState = (state: RootState): FilterState => {
  return state.tableState ? state.tableState.filterState : initialState;
};

export const getFilterOnPlaying = (state: RootState): boolean => {
  return isFilteredOnPlaying(getFilterState(state).filterTree);
};

// Only here
export function getFields(state: RootState): Map<string, Field> {
  return state.dataState ? state.dataState.fields : new Map();
}

export const getStatFields = createSelector(
  getFields,
  (fields: Map<FieldId, Field>): Field[] => {
    log.trace('getStatFields');
    return Array.from(fields.values()).filter(field => field.category !== Category.INFO);
  }
);

export const getColumns = createSelector(
  getColumnGroups,
  getStatFields,
  (columnGroups: Set<Topic>, statFields: Field[]): FieldKey[] => {
    log.trace('createColumns');
    const columns: FieldKey[] = [];

    // always add resource and opposing info
    columns.push(makeFieldKey(Topic.RESOURCE, Category.INFO, 'abbreviation'));
    columns.push(makeFieldKey(Topic.OPPOSING, Category.INFO, 'abbreviation'));

    // add stats
    Topics.forEach(topic => {
      if (columnGroups.has(topic)) {
        statFields.forEach(stat => {
          columns.push(makeFieldKey(topic, stat.category, stat.id));
        });
      }
    });

    return columns;
  }
);

export const getSizedFixedHeaders = (): SizedHeaderItem[] => {
  return [
    {
      header: {
        fieldKey: makeFieldKey(Topic.RESOURCE, Category.INFO, 'abbreviation'),
        label: 'Team',
        description: 'Team'
      },
      width: COLUMN_WIDTH * 1.5 + 'px'
    },
    {
      header: {
        fieldKey: makeFieldKey(Topic.OPPOSING, Category.INFO, 'abbreviation'),
        label: 'Opponent',
        description: 'Opposing Team'
      },
      width: COLUMN_WIDTH * 1.5 + 'px'
    }
  ];
};

export const getScrollableColumns = createSelector(
  getStatFields,
  (statFields: Field[]): FieldKey[] => {
    // TODO: save columns to state
    const columns: FieldKey[] = [];

    // Categories.forEach(category => {
    Topics.forEach(topic => {
      statFields.forEach(field => {
        columns.push(makeFieldKey(topic, Category.BATTING, field.id));
      });
    });
    // });

    return columns;
  }
);
