import * as React from 'react';

import { RouteComponentProps, withRouter } from 'react-router-dom';

import { LOGIN_PATH } from '../core/routes';
import { RootState } from '../core/reducer';
import { User } from 'statmaster-common';
import { connect } from 'react-redux';

interface StateProps {
  user?: User;
}

class ProfileContainer extends React.Component<RouteComponentProps<{}> & StateProps & null> {

  renderUser(user: User) {
    return (
      <p>Email: {user.email}</p>
    );
  }

  render() {
    const { user, history } = this.props;
    if (user === undefined) {
      history.push(LOGIN_PATH);
      return (
        <div>
          Unreachable
        </div>
      );
    }
    return (
      <div id="profile">
        <h2>Profile</h2>
        {
          this.renderUser(user)
        }
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    user: state.sessionState.user
  };
}

export const Profile = withRouter(connect(mapStateToProps, null)(ProfileContainer) as any);
