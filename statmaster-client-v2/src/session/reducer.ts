import { DatetimeAction, SessionAction, UpdateUserAction } from './actions';
import { STATIC_DATE, User } from 'statmaster-common';

import { ActionTypeKey } from '../core/actions';
import { LocalDateTime } from 'js-joda';

export interface SessionState {
  datetime: LocalDateTime;
  user?: User;
}

const initialState: SessionState = {
  datetime: STATIC_DATE
};

export function sessionReducer(state: SessionState = initialState, action: SessionAction): SessionState {
  switch (action.type) {
    case ActionTypeKey.UPDATE_USER:
      return updateUser(state, action);
    case ActionTypeKey.UPDATE_DATE:
      return updateDatetime(state, action as DatetimeAction);
    case ActionTypeKey.LOGOUT_USER:
      return deleteUser(state);
    default:
      return state;
  }
}

// TODO: refresh all user info

function updateUser(state: SessionState, action: UpdateUserAction): SessionState {
  if (action.user) {
    return Object.assign({}, state, { user: action.user });
  }
  return state;
}

function deleteUser(state: SessionState): SessionState {
  return Object.assign({}, state, { user: undefined });
}

function updateDatetime(state: SessionState, action: DatetimeAction): SessionState {
  return Object.assign({}, state, { datetime: action.datetime });
}