import { Action, ActionStatus, ActionTypeKey } from '../core/actions';
import { fetchGamesAndMatchups, fetchStatGroupsForTeams } from '../statsdata/fetch';

import { Dispatch } from 'react-redux';
import { LocalDateTime } from 'js-joda';
import { RootState } from '../core/reducer';
import { User } from 'statmaster-common';

export type SessionAction = UpdateUserAction | UserAction | DatetimeAction;

export interface DatetimeAction extends Action {
  datetime: LocalDateTime;
}

export interface UserAction extends Action {
  message?: string;
}

export interface UpdateUserAction extends Action {
  user?: User;
}

export function makeUpdateUserAction(user: User): UpdateUserAction {
  return {
    type: ActionTypeKey.UPDATE_USER,
    user
  };
}

export function makeLogoutUserAction(): UserAction {
  return {
    type: ActionTypeKey.LOGOUT_USER
  };
}

export function makeUserInProgressAction(): UserAction {
  return {
    type: ActionTypeKey.USER,
    state: ActionStatus.IN_PROGRESS
  };
}

export function makeUserSucceededAction(): UserAction {
  return {
    type: ActionTypeKey.USER,
    state: ActionStatus.SUCCEEDED
  };
}

export function makeUserFailedAction(): UserAction {
  return {
    type: ActionTypeKey.USER,
    state: ActionStatus.FAILED
  };
}
 
export function makeUpdateDatetimeAction(datetime: LocalDateTime): DatetimeAction {
  return {
    type: ActionTypeKey.UPDATE_DATE,
    datetime: datetime
  };
}

export function updateDatetime(datetime: LocalDateTime) {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    dispatch(makeUpdateDatetimeAction(datetime));
    dispatch(fetchGamesAndMatchups());
    dispatch(fetchStatGroupsForTeams());
  };
}