import { LOGIN_PATH, PROFILE_PATH } from '../core/routes';
import {
  makeUpdateUserAction,
  makeUserFailedAction,
  makeUserInProgressAction,
  makeUserSucceededAction
} from './actions';

import { Dispatch } from 'react-redux';
import { RootState } from '../core/reducer';
import { Service } from '../service/service';
import { User } from 'statmaster-common';
import getLogger from '../util/log';
import { makeLogoutUserAction } from './actions';
import { push } from 'react-router-redux';

const log = getLogger('userService');
export const service = new Service();

export function initiateLogin(email: string, password: string) {
  return intiateUserAction(email, password, false);
}

export function initiateRegister(email: string, password: string) {
  return intiateUserAction(email, password, true);
}

function intiateUserAction(email: string, password: string, registering: boolean) {
  const action = registering ? 'REGISTER' : 'LOGIN';
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Initiating ${action}`);
    dispatch(makeUserInProgressAction());
    try {
      const user = registering ? await register(email, password) : await login(email, password);
      if (user) {
        log.debug(`${action} successful: ${JSON.stringify(user)}`);
        dispatch(makeUserSucceededAction());
        dispatch(makeUpdateUserAction(user));
        dispatch(push(PROFILE_PATH));
      } else {
        dispatch(makeUserFailedAction());
      }
    } catch (err) {
      log.warn(`${action} failed: ${err.response.data}`);
      dispatch(makeUserFailedAction());
    }
  };
}

export async function login(email: string, password: string): Promise<User> {
  const res = await service.post(`/auth/login`, {
    email,
    password
  });
  return res.data;
}

export async function register(email: string, password: string): Promise<User> {
  const res = await service.post(`/auth/register`, {
    email,
    password
  });
  return res.data;
}

export function logout() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    try {
      dispatch(makeLogoutUserAction());
      dispatch(push(LOGIN_PATH));
      await service.post(`/auth/logout`);
    } catch (err) {
      log.error(err);
    }
  };
}
