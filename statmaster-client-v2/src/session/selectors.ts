import { LocalDate, LocalDateTime } from 'js-joda';

import { RootState } from '../core/reducer';

export function getDateTime(state: RootState): LocalDateTime {
  return state.sessionState ? state.sessionState.datetime : LocalDateTime.now();
} 

export function getDate(state: RootState): LocalDate {
  return getDateTime(state).toLocalDate();
}
