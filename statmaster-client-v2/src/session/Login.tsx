import './Login.css';

import * as React from 'react';

import { initiateLogin, initiateRegister } from './userService';

import { Dispatch } from 'redux';
import { RootState } from '../core/reducer';
import { connect } from 'react-redux';
import getLogger from '../util/log';

const log = getLogger('Login');

interface DispatchProps {
  login(email: string, password: string): void;
  register(email: string, password: string): void;
}

class LoginContainer extends React.Component<null & DispatchProps> {

  private email: HTMLInputElement;
  private password: HTMLInputElement;

  handleLogin = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    event.stopPropagation();
    if (!this.email.value || !this.password.value) {
      log.warn('Missing fields');
      return;
    }

    this.props.login(this.email.value, this.password.value);
  }

  handleRegister = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    event.stopPropagation();
    if (!this.email.value || !this.password.value) {
      log.warn('Missing fields');
      return;
    }

    this.props.register(this.email.value, this.password.value);
  }

  render() {
    return (
      <div className="login">
        <h2>Welcome Back!</h2>
        <form className="login-form">
          <label>
            <span>Email</span>
            <input type="text" name="email" ref={email => this.email = email as HTMLInputElement} />
          </label>
          <label>
            <span>Password</span>
            <input type="password" name="password" ref={password => this.password = password as HTMLInputElement} />
          </label>
          <div id="user-buttons">
            <button name="register" onClick={this.handleRegister}>Register</button>
            <button name="login" onClick={this.handleLogin}>Login</button>
          </div>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch: Dispatch<RootState>): DispatchProps {
  return {
    login: (email, password) => dispatch(initiateLogin(email, password)),
    register: (email, password) => dispatch(initiateRegister(email, password))
  };
}

export const Login = connect(null, mapDispatchToProps)(LoginContainer);
