import * as React from 'react';

import { Login } from './Login';

export class LoginMain extends React.Component {
  render() {
    return (
      <main id="login-main">
        <Login />
      </main>
    );
  }
}