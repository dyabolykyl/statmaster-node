import './Nav.css';

import * as React from 'react';

import {
  ADMIN_PATH,
  GAMES_PATH,
  LOGIN_PATH,
  SELECTORS_PATH,
  STATS_PATH
} from '../core/routes';
import { Dispatch, connect } from 'react-redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

import { LocalDateTime } from 'js-joda';
import { RootState } from '../core/reducer';
import { SessionState } from '../session/reducer';
import { logout } from '../session/userService';

interface StateProps {
  sessionState: SessionState;
}

interface DispatchProps {
  logout: () => Promise<void>;
}

class NavContainer extends React.Component<RouteComponentProps<{}> & StateProps & DispatchProps> {
  static defaultProps: StateProps = {
    sessionState: {
     datetime: LocalDateTime.now()
    }
  };

  createAdminElement() {
    const user = this.props.sessionState.user;
    if (user && user.admin) {
      return this.createNavElement('ADMIN', ADMIN_PATH);
    }

    return null;
  }

  getUserElement(): React.ReactElement<{}> {
    const user = this.props.sessionState.user;
    if (user !== undefined) {
      return (
        <i className={`user fa fa-sign-out`} onClick={() => this.props.logout()} />
      );
    } else {
      return (
        <Link className={`user fa fa-user ${this.getActiveClassName(LOGIN_PATH)}`} to={LOGIN_PATH}/>
      );
    }
  }

  createNavElement(title: string, path: string): React.ReactElement<{}> {
    return (
      <Link className={this.getActiveClassName(path)} to={path}>
        {title}
      </Link>
    );
  }
  
  render() {
    return (
      <nav>
        {this.createAdminElement()}
        {this.createNavElement('STATS', STATS_PATH)}
        {this.createNavElement('GAMES', GAMES_PATH)}
        {this.createNavElement('SELECTORS', SELECTORS_PATH)}
        {this.getUserElement()}
      </nav>
    );
  }

  private getActiveClassName(path: string): string {
    const currentPath = this.props.location.pathname;
    // Homepage defaults to stats
    const isActive = (currentPath === path) || (currentPath === '/' && path === STATS_PATH);
    return isActive ? 'active' : '';
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    sessionState: state.sessionState
  };
}

function mapDispatchToProps(dispatch: Dispatch<RootState>) {
  return {
    logout: () => dispatch(logout())
  };
}

export const Nav = withRouter(connect(mapStateToProps, mapDispatchToProps)(NavContainer) as any);
