import * as React from 'react';

import { Link } from 'react-router-dom';
import { SELECTORS_PATH } from '../core/routes';
import { Selector } from 'statmaster-common';

interface Props {
  selector: Selector;
}

export class SelectorPreview extends React.Component<Props> {

  render() {
    const selector = this.props.selector;
    const selectorPath = `${SELECTORS_PATH}/${selector.id}`;
    return (
      <div className="selector-preview">
        <h4>
          <Link to={selectorPath}>
            {selector.name}
          </Link>
        </h4>
        {/* <p>Selector preview here</p> */}
      </div>
    );
  }
}
