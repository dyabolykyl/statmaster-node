import { Action, ActionTypeKey } from '../core/actions';
import { FilterTree, SelectionList, Selector, SelectorId, Sort } from 'statmaster-common';

import getLogger from '../util/log';
import { getNextSelectorId } from './api';

const log = getLogger('selectors.reducer');

export interface SelectorState {
  selectors: Map<SelectorId, Selector>;
  selections: Map<SelectorId, SelectionList>;
}

export const initialState = {
  selectors: new Map(),
  selections: new Map()
};

export interface AddSelectorAction extends Action {
  filterTree: FilterTree;
  feature: Sort;
}

export interface RemoveSelectorAction extends Action {
  selectorId: SelectorId;
}

export interface SetSelectorsAction extends Action {
  selectors: Selector[];
  selectionLists: SelectionList[];
}

export type SelectorAction = AddSelectorAction | RemoveSelectorAction | SetSelectorsAction;

export function makeAddSelectorAction(filterTree: FilterTree, feature: Sort): AddSelectorAction {
  return {
    type: ActionTypeKey.ADD_SELECTOR,
    filterTree, 
    feature,
  };
}

export function makeRemoveSelectorAction(selectorId: SelectorId): RemoveSelectorAction {
  return {
    type: ActionTypeKey.REMOVE_SELECTOR,
    selectorId
  };
}

export function makeSetSelectorsAction(selectors: Selector[], selectionLists: SelectionList[]): SetSelectorsAction {
  return {
    type: ActionTypeKey.SET_SELECTORS,
    selectors,
    selectionLists
  };
}

export function selectorReducer(state: SelectorState = initialState, action: SelectorAction): SelectorState {
  switch (action.type) {
    case ActionTypeKey.ADD_SELECTOR:
      return addSelector(state, action as AddSelectorAction);
    case ActionTypeKey.REMOVE_SELECTOR:
      return removeSelector(state, action as RemoveSelectorAction);
    case ActionTypeKey.SET_SELECTORS:
      return setSelectors(state, action as SetSelectorsAction); 
    default:
      return state;
  }
}

function addSelector(state: SelectorState, action: AddSelectorAction): SelectorState {
  const id = getNextSelectorId();
  const name = `Selector ${id}`;
  const selector = new Selector(id, name, action.filterTree, action.feature);
  log.debug(`Adding selector: ${selector}`);

  // TODO: Check if selector already exists
  const newSelectors = new Map(state.selectors);
  newSelectors.set(selector.id, selector);
  const newSelections = new Map(state.selections);
  newSelections.set(selector.id, {selectorId: selector.id, selections: new Map()});
  return {
    selectors: newSelectors,
    selections: state.selections
  };
}

function removeSelector(state: SelectorState, action: RemoveSelectorAction): SelectorState {
  log.debug(`Adding selector: ${action.selectorId}`);
  const newSelectors = new Map(state.selectors);
  newSelectors.delete(action.selectorId);
  const newSelections = new Map(state.selections);
  newSelections.delete(action.selectorId);
  return {
    selectors: newSelectors,
    selections: newSelections
  };
}

function setSelectors(state: SelectorState, action: SetSelectorsAction): SelectorState {
  log.debug(`Setting selectors`);
  const selectorsMap = new Map();
  action.selectors.forEach(selector => selectorsMap.set(selector.id, selector));
  const selectionListsMap = new Map();
  action.selectionLists.forEach(selectionList => selectionListsMap.set(selectionList.selectorId, selectionList));
  return {
    selectors: selectorsMap,
    selections: selectionListsMap
  };
}
