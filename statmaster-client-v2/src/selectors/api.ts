import {  } from './types';
import {  } from '../tablecontrols/sorting/sorting';

import {
  Category,
  DataType,
  FilterExpression,
  FilterOperator,
  FilterTree,
  FlatFilterTree,
  PLAYING_KEY,
  Selection,
  SelectionList,
  Selector,
  Sort,
  SortStyle,
  Topic,
  makeFieldKey,
  mockTeams
} from 'statmaster-common';

import { getCurrentDateTime } from '../util/date';

let nextSelectorId = 0;
export function getNextSelectorId() {
  return ++nextSelectorId + '';
}

// TODO: Use user id and validate login
export function getSelectors(): Selector[] {
  return STATIC_SELECTORS;
}

export function getSelectionLists(): SelectionList[] {
  return STATIC_SELECTIONS;
}

const STATIC_SELECTIONS: SelectionList[] = [
  new SelectionList('1', [
    new Selection(getCurrentDateTime().toLocalDate(), [
      {resource: mockTeams[0].id},
      {resource: mockTeams[1].id}
    ]),
    new Selection(getCurrentDateTime().toLocalDate().minusDays(1), [
      {resource: mockTeams[2].id},
      {resource: mockTeams[0].id}
    ])
  ])
];

const STATIC_SELECTORS: Selector[] = [
  createSelector(
    [new FilterExpression(PLAYING_KEY, FilterOperator.EQUAL_TO, true, DataType.BOOLEAN)],
    {
      columnKey: makeFieldKey(Topic.RESOURCE, Category.BATTING, 'avg'),
      style: SortStyle.DESCENDING
    }),
  createSelector(
    [
      new FilterExpression(PLAYING_KEY, FilterOperator.EQUAL_TO, true, DataType.BOOLEAN),
      new FilterExpression(
        makeFieldKey(Topic.RESOURCE_VS_OPPOSING, Category.BATTING, 'avg'),
        FilterOperator.GREATER_THAN, 0.28, DataType.NUMBER)
    ],
    {
      columnKey: makeFieldKey(Topic.RESOURCE, Category.BATTING, 'avg'),
      style: SortStyle.DESCENDING
    },
    new FilterExpression(
      makeFieldKey(Topic.RESOURCE, Category.STANDINGS, 'w'),
      FilterOperator.GREATER_THAN, 0, DataType.NUMBER))
];

function createSelector(filters: FilterExpression[], sort: Sort, success?: FilterExpression): Selector {
  const id = getNextSelectorId();
  const filterTree: FilterTree = new FlatFilterTree();
  filters.forEach(filterTree.addFilter);
  return new Selector(id, 'Selector ' + id, filterTree, sort, success);
}
