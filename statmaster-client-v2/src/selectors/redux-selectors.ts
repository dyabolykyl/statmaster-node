import { SelectionList, Selector, SelectorId } from 'statmaster-common';

import { RootState } from '../core/reducer';

export function getSelectors(state: RootState): Map<SelectorId, Selector> {
  return state.selectorState ? state.selectorState.selectors : new Map();
}

export function getSelectionLists(state: RootState): Map<SelectorId, SelectionList> {
  return state.selectorState ? state.selectorState.selections : new Map();
}
