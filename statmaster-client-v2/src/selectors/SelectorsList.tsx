import * as React from 'react';

import { Selector, SelectorId } from 'statmaster-common';

import { RootState } from '../core/reducer';
import { SelectorPreview } from './SelectorPreview';
import { connect } from 'react-redux';
import { getSelectors } from './redux-selectors';

interface StateProps {
  selectors: Map<SelectorId, Selector>;
}

export class SelectorsListContainer extends React.Component<StateProps & null> {

  render() {
    return (
      <div className="selectors">
        <h2>Selectors </h2>
        {
          Array.from(this.props.selectors).map(([key, selector]) => (
            <SelectorPreview key={key} selector={selector} />
          ))
        }
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    selectors: getSelectors(state)
  };
}

export const SelectorsList = connect(mapStateToProps, null)(SelectorsListContainer);