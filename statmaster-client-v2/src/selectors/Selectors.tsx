import * as React from 'react';

import { Route, Switch } from 'react-router-dom';
import { Selector, SelectorId } from 'statmaster-common';

import { RootState } from '../core/reducer';
import { SELECTORS_PATH } from '../core/routes';
import { SelectorInfo } from './SelectorInfo';
import { SelectorsList } from './SelectorsList';
import { connect } from 'react-redux';
import { getSelectors } from './redux-selectors';

interface StateProps {
  selectors: Map<SelectorId, Selector>;
}

export class SelectorsContainer extends React.Component<StateProps & null> {

  render() {
    return (
      <div className="selectors">
        <Switch>
          <Route exact={true} path={SELECTORS_PATH} component={SelectorsList} />
          <Route exact={true} path={`${SELECTORS_PATH}/:id`} component={SelectorInfo} />
        </Switch>
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    selectors: getSelectors(state)
  };
}

export const Selectors = connect(mapStateToProps, null)(SelectorsContainer);
