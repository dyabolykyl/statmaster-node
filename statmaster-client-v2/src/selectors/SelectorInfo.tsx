import 'react-datepicker/dist/react-datepicker.css';

import * as React from 'react';

import {
   Field,
   FieldId,
   FilterTree,
   SelectionList,
   Selector,
   SelectorId,
   makeDateKey,
   momentToJoda
} from 'statmaster-common';
import { getSelectionLists, getSelectors } from './redux-selectors';

import DatePicker from 'react-datepicker';
import { LocalDate } from 'js-joda';
import { Moment } from 'moment-timezone';
import { RootState } from '../core/reducer';
import { RouteComponentProps } from 'react-router-dom';
import { SelectionSnapshot } from './SelectionSnapshot';
import { connect } from 'react-redux';
import { getDate } from '../session/selectors';
import { getFields } from '../tablecontrols/selectors';
import { getFilterDisplay } from '../tablecontrols/filtering/filtering';
import { getSortDisplay } from '../tablecontrols/sorting/sorting';

interface OwnProps extends RouteComponentProps<any> {
}

interface StateProps {
  date: LocalDate;
  fields: Map<FieldId, Field>;
  selectors: Map<SelectorId, Selector>;
  selectionLists: Map<SelectorId, SelectionList>;
}

type Props = OwnProps & StateProps & null;

interface State {
  currentDate: LocalDate;
}

// interface DispatchProps {

// }

export class SelectorInfoContainer extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      currentDate: props.date,
    };
  }

  renderFilterList(filterTree: FilterTree): JSX.Element[] {
    return filterTree.getFilters().map(filter => {
      return (
        <div key={filter.key} className="filter" >
          <span>{getFilterDisplay(filter)}</span>
        </div>
      );
    });
  }

  renderSuccessCriteria(selector: Selector) {
    if (selector.successCriteria !== undefined) {
      return (
        <div>{getFilterDisplay(selector.successCriteria)}</div>
      );
    }

    return <div>Undefined</div>;
  }

  renderSelectorDefinition(selector: Selector): JSX.Element {
    const { fields } = this.props;
    return (
      <div className="selector-definition">
        <div className="selector-feature">
          <strong>Rank by:</strong>
          <div>{getSortDisplay(selector.feature, fields.get(selector.feature.columnKey.fieldId))}</div>
        </div>
        <div className="selector-filters">
          <strong>Filter on:</strong>
          {this.renderFilterList(selector.filterTree)}
        </div>
        <div className="selector-success">
          <strong>Success criteria:</strong>
          {this.renderSuccessCriteria(selector)}
        </div>
      </div>
    );
  }

  renderError(): JSX.Element {
    return (
      <div>
        <h2>Error</h2>
        <p>Unknown Selector</p>
      </div>
    );
  }

  getSelection() {
    const { match, selectionLists } = this.props;
    const selectionList = selectionLists.get(match.params.id) || new SelectionList(match.params.id, []);
    return selectionList.selections.get(makeDateKey(this.state.currentDate));
  }

  handleOnDateChanged = (date: LocalDate) => {
    this.setState({
      currentDate: date
    });
  }

  render() {
    const { selectors, match } = this.props;
    const selector = selectors.get(match.params.id);

    if (selector === undefined) {
      return this.renderError();
    }

    return (
      <div className="selector-info">
        <h2>{selector.name}</h2>
        {this.renderSelectorDefinition(selector)}
        <div className="selector-picks">
          <DatePicker onChange={(date: Moment) => this.handleOnDateChanged(momentToJoda(date).toLocalDate())} />
          <SelectionSnapshot selection={this.getSelection()} date={this.state.currentDate} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    date: getDate(state),
    selectors: getSelectors(state),
    selectionLists: getSelectionLists(state),
    fields: getFields(state)
  };
}

export const SelectorInfo = connect(mapStateToProps, null)(SelectorInfoContainer);
