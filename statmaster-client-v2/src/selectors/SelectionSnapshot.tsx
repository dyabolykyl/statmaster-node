import './Selectors.css';

import * as React from 'react';

import { ResourceId, Selection, Team } from 'statmaster-common';

import { LocalDate } from 'js-joda';
import { RootState } from '../core/reducer';
import { connect } from 'react-redux';
import { displayDate } from '../util/date';
import { getTeams } from '../statstable/selectors';

interface OwnProps {
  selection?: Selection;
  date: LocalDate;
}

interface StateProps {
  teams: Map<ResourceId, Team>;
}

type Props = OwnProps & StateProps & null;

export class SelectionSnapshotContainer extends React.Component<Props> {

  constructor(props: Props) {
    super(props);
  }

  renderSelection() {
    const { teams, selection } = this.props;
    if (selection === undefined) {
      return (
        <p>
          No picks found
        </p>
      );
    }

    return (
      <ol>
        {
          selection.picks.map(pick => (
            <li>
              {
                (teams.get(pick.resource) as Team).name
              }
            </li>
          ))
        }
      </ol>
    );
  }

  render() {
    const date = this.props.date;

    return (
      <div className="selection-snapshot">
        Selections for {displayDate(date)}
        {this.renderSelection()}
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    teams: getTeams(state)
  };
}

export const SelectionSnapshot = connect(mapStateToProps, null)(SelectionSnapshotContainer);
