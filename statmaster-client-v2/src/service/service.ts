import { DateTimeFormatter, LocalDate } from 'js-joda';
import { Response, ResponseStatus } from './response';
import axios, { AxiosInstance, AxiosResponse } from 'axios';

import getLogger from '../util/log';

const log = getLogger('service');

// const hostname = (process.env.NODE_ENV === 'production') :
const API_URL = process.env.NODE_ENV === 'production'
  ? `https://statmaster.herokuapp.com`
  : `http://localhost:${process.env.PORT || 3001}`;

export class Service {

  private readonly service: AxiosInstance;
  private readonly dateFormatter = DateTimeFormatter.ofPattern('yyyy-MM-dd');

  constructor() {
    let service = axios.create({
      headers: { csrf: 'token' },
      withCredentials: true
    });
    // service.interceptors.response.use(this.handleSuccess);
    this.service = service;
  }

  formatDate(date: LocalDate): string {
    return this.dateFormatter.format(date);
  }

  handleSuccess(response: AxiosResponse): AxiosResponse {
    return response;
  }

  handleError = (error: any) => {
    // Disabling because it causes endless reloading of components
    /*
    if (error.response) {
      switch (error.response.status) {
        case 401:
          this.redirectTo('/');
          break;
        case 404:
          this.redirectTo('/404');
          break;
        default:
          this.redirectTo('/500');
          break;
      }
    }
    */
    return Promise.reject(error);
  }

  redirectTo = (path: string) => {
    window.location.href = path;
  }

  async get(path: string, data?: any): Promise<Response<any>> {
    const url = `${API_URL}${path}`;
    log.debug(`Querying ${url}`);

    let config = undefined;
    if (data) {
      config = { data };
    }

    const res = await this.service.get(url, config);
    return this.processResponse(res);
  }

  async post(path: string, data?: any): Promise<Response<any>> {
    const url = `${API_URL}${path}`;
    log.debug(`Sending POST to ${url}`);
    const res = await this.service.post(url, data);
    return this.processResponse(res);
  }

  private processResponse(res: AxiosResponse): Response<any> {
    let status;
    if (res.status === 401 || res.status === 403) {
      status = ResponseStatus.UNAUTHORIZED;
    } else if (res.status >= 400 && res.status < 500) {
      status = ResponseStatus.CLIENT_ERROR;
    } else if (res.status >= 500) {
      status = ResponseStatus.SERVER_ERROR;
    } else {
      status = ResponseStatus.SUCCESS;
    }

    return {
      status,
      data: res.data
    };
  }

  // patch<T>(path: string, payload: T, callback: any) {
  //   return this.service.request<T>({
  //     method: 'PATCH',
  //     url: path,
  //     responseType: 'json',
  //     data: payload
  //   }).then((response) => callback(response.status, response.data));
  // }

  // post(path, payload, callback) {
  //   return this.service.request({
  //     method: 'POST',
  //     url: path,
  //     responseType: 'json',
  //     data: payload
  //   }).then((response) => callback(response.status, response.data));
  // }
}
