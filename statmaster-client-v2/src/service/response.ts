
export enum ResponseStatus {
  SUCCESS,
  SERVER_ERROR,
  CLIENT_ERROR,
  UNAUTHORIZED,
  CANCELLED
}

export interface Response<T> {
  data?: T;
  status: ResponseStatus;
}