import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as mockReducer from '../core/reducer';
import * as mockSessionSelectors from '../session/selectors';

import { Category, Topic, makeFieldKey, mockFields } from 'statmaster-common';
import { LocalDateTime, ZoneOffset } from 'js-joda';
import {
   makeUpdateFieldsSucceededAction,
   makeUpdateStatsSucceededAction,
   makeUpdateTeamsSucceededAction
} from '../statsdata/actions';
import { mockStatGroups, mockTeams } from 'statmaster-common';

import App from './App';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { RootState } from '../core/reducer';
import { Store } from 'redux';
import { Table } from '../table/Table';
import configureStore from 'redux-mock-store';
import { createStore } from 'redux';
import { makeUpdateSortAction } from '../tablecontrols/sorting/sorting';
import { mount } from 'enzyme';
import reducer from '../core/reducer';
import toJson from 'enzyme-to-json';

// set static date
const mockDate: LocalDateTime = LocalDateTime.ofEpochSecond(0, ZoneOffset.UTC);
const mockDateSupplier = (state: RootState) => mockDate;

Object.defineProperty(mockSessionSelectors, 'getDateTime', { value: mockDateSupplier });

// create app with a static router
const createApp = (store: Store<RootState>) => {
  return (
    <Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/stats', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider >
  );
};

describe('App renders', () => {
  const mockStore = configureStore();
  /* tslint:disable:no-any */
  let store: Store<any>;

  beforeAll(() => {
    Object.defineProperty(mockReducer, 'setInitialState', { value: jest.fn() });
  });

  beforeEach(() => {
    store = mockStore({});
  });

  test('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(createApp(store), div);
    expect(mockReducer.setInitialState).toHaveBeenCalledTimes(1);
  });
});

describe('<App /> Functional test', () => {
  let store: Store<RootState>;

  const setStore = (state?: RootState) => {
    store = createStore(reducer, state) as Store<RootState>;
  };

  beforeEach(() => {
    setStore();
  });

  test('renders table with empty state', () => {
    const app = mount(createApp(store));

    expect(app.find(Table)).toHaveLength(1);
    expect(app.find(Table).prop('rows')).toEqual([]);
    expect(app.find(Table).prop('headers')).toHaveLength(2);
    expect(app.find(Table).prop('onColumnClick')).toBeDefined();
  });

  test('renders with data', () => {
    let updateStatsAction = makeUpdateStatsSucceededAction(mockStatGroups);
    store.dispatch(updateStatsAction);
    store.dispatch(makeUpdateTeamsSucceededAction(mockTeams));
    // store.dispatch(makeUpdateMatchupsSucceededAction(mockMatchups));
    store.dispatch(makeUpdateFieldsSucceededAction(mockFields));

    const app = mount(createApp(store));

    expect(toJson(app)).toMatchSnapshot();
  });

  test('sorting on column', () => {
    store.dispatch(makeUpdateStatsSucceededAction(mockStatGroups));
    store.dispatch(makeUpdateTeamsSucceededAction(mockTeams));
    // store.dispatch(makeUpdateMatchupsSucceededAction(mockMatchups));
    store.dispatch(makeUpdateFieldsSucceededAction(mockFields));
    store.dispatch(makeUpdateSortAction(makeFieldKey(Topic.RESOURCE, Category.INFO, 'city')));
    store.dispatch(makeUpdateSortAction(makeFieldKey(Topic.RESOURCE, Category.INFO, 'city')));

    const app = mount(createApp(store));

    expect(toJson(app)).toMatchSnapshot();
  });

});