import './App.css';

import * as React from 'react';

import { Dispatch, connect } from 'react-redux';
import { RootState, setInitialState } from '../core/reducer';

import Header from '../header/Header';
import { Main } from '../main/Main';
import { Nav } from '../nav/Nav';
import { withRouter } from 'react-router-dom';

interface DispatchProps {
  setInitialState: () => void;
}

class App extends React.Component<DispatchProps> {

  componentDidMount() {
    this.props.setInitialState();
  }

  render() {
    return (
      <div className="app">
        <Header />
        <Nav />
        <Main />
      </div>
    );
  }
}

// tslint:disable no-any
export default withRouter(connect(null, (dispatch: Dispatch<RootState>) => {
  return {
    setInitialState: () => setInitialState(dispatch)
  };
})(App) as any);
