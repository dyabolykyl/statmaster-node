import './Header.css';

import * as React from 'react';

import { Link } from 'react-router-dom';

class Header extends React.Component {
  render() {
    return (
      <header>
        <h1>
          <Link to="/">STATMASTER</Link>
        </h1>
      </header>
    );
  }
}

export default Header;
