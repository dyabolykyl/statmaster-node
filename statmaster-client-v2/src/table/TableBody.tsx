import * as React from 'react';
import { TableHeaderItem, TableRowItem } from './types';
import { TableRow } from './TableRow';

export interface TableBodyProps {
  headers: Array<TableHeaderItem>;
  rows: Array<TableRowItem>;
}

export class TableBody extends React.Component<TableBodyProps> {
  static defaultProps = {
    headers: [],
    rows: []
  };

  render() {
    let { headers, rows } = this.props;

    return (
      <div className="table-body">
        {
          rows.map(row => (
            <TableRow key={row.key} row={row} headers={headers} />
          ))
        }
      </div>
    );
  }
}
