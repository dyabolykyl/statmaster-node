import * as React from 'react';

import { TableHeaderItem, TableRowItem } from './types';

import { FieldValue } from 'statmaster-common';

export interface TableRowProps {
  row: TableRowItem;
  headers: Array<TableHeaderItem>;
}

export class TableRow extends React.Component<TableRowProps> {
  static defaultProps: TableRowProps = {
    row: {key: 'empty', data: new Map()},
    headers: []
  };

  render() {
    let { row, headers } = this.props;

    return (
      <div className="table-row">
        {
          headers.map(header => {
            let rowData = row.data.get(header.fieldKey.flatKey);
            let value: FieldValue = '!';
            if (rowData) {
              value = rowData.value;
            }
            return (
              <div className="table-cell" key={header.fieldKey.flatKey}>
                {value}
              </div>
            );
          })
        }
      </div>
    );
  }
}
