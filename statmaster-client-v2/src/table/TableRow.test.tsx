import { shallow } from 'enzyme';
import * as React from 'react';
import * as tdf from '../util/testDataFactory';

import { TableRow, TableRowProps } from './TableRow';
import { TableHeaderItem, TableRowItem } from './types';
import toJson from 'enzyme-to-json';

describe('<TableRow />', () => {
  const defaultProps: TableRowProps = TableRow.defaultProps;

  test('default props', () => {
    const tableRow = shallow(<TableRow row={defaultProps.row} headers={defaultProps.headers} />);
    expect(tableRow.find('div.table-row')).toHaveLength(1);
  });

  test('renders with data', () => {
    let header: TableHeaderItem = tdf.arrangeTableHeaderItem('f1');
    let header2: TableHeaderItem = tdf.arrangeTableHeaderItem('f2');
    let header3: TableHeaderItem = tdf.arrangeTableHeaderItem('f3');
    let headers: TableHeaderItem[] = [header, header2, header3];

    let key = 'id1';
    let row: TableRowItem = tdf.tableRowItem(key, tdf.tableRowData('f1', 5), tdf.tableRowData('f2', 10));

    const tableRow = shallow(<TableRow row={row} headers={headers} />);
    expect(toJson(tableRow)).toMatchSnapshot();
  });
});
