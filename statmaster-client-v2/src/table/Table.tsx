import * as React from 'react';
import { TableHeaderItem, TableRowItem } from './types';
import { TableHeader } from './TableHeader';
import { TableBody } from './TableBody';

export interface TableProps {
  headers: Array<TableHeaderItem>;
  rows: Array<TableRowItem>;
  onColumnClick: (header: TableHeaderItem) => void;
}

export class Table extends React.Component<TableProps> {
  static defaultProps = {
    headers: [],
    rows: [],
    onColumnClick: (key: TableHeaderItem) => null,
  };

  render() {
    let { headers, rows, onColumnClick, } = this.props;

    return (
      <div className="table">
        <TableHeader onColumnClick={onColumnClick} headers={headers} />
        <TableBody headers={headers} rows={rows} />
      </div>
    );
  }
}
