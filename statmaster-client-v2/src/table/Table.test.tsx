import { shallow } from 'enzyme';
import * as React from 'react';
import * as tdf from '../util/testDataFactory';
import toJson from 'enzyme-to-json';
import { TableProps, Table } from './Table';
import { TableRowItem, TableHeaderItem } from './types';

describe('<Table />', () => {
  const defaultProps: TableProps = Table.defaultProps;

  test('default props', () => {
    const tableRow = shallow((
      <Table
        rows={defaultProps.rows}
        headers={defaultProps.headers}
        onColumnClick={jest.fn()}
      />
    ));
    expect(tableRow.find('div.table')).toHaveLength(1);
  });

  test('renders with data', () => {
    let headers: TableHeaderItem[] = [tdf.arrangeTableHeaderItem('1'), tdf.arrangeTableHeaderItem('2')];
    let row1: TableRowItem = tdf.tableRowItem(
      'row1',
      tdf.tableRowData('1', 1), tdf.tableRowData('2', 'value2'));
    let row2: TableRowItem = tdf.tableRowItem(
      'row2',
      tdf.tableRowData('1', 15));

    const tableRow = shallow(<Table rows={[row1, row2]} headers={headers} onColumnClick={jest.fn()} />);
    expect(toJson(tableRow)).toMatchSnapshot();
  });
});
