import { shallow } from 'enzyme';
import * as React from 'react';
import * as tdf from '../util/testDataFactory';

import { TableHeaderItem, TableRowItem } from './types';
import toJson from 'enzyme-to-json';
import { TableBody, TableBodyProps } from './TableBody';
import { TableHeader } from './TableHeader';
import { TableRow } from './TableRow';

describe('<TableBody />', () => {
  const defaultProps: TableBodyProps = TableBody.defaultProps;

  test('default props', () => {
    const tableBody = shallow(<TableBody rows={defaultProps.rows} headers={defaultProps.headers} />);
    expect(tableBody.find(TableHeader)).toHaveLength(0);
    expect(tableBody.find(TableRow)).toHaveLength(0);
  });

  test('renders with data', () => {
    let headers: TableHeaderItem[] = [tdf.arrangeTableHeaderItem('1'), tdf.arrangeTableHeaderItem('2')];
    let row1: TableRowItem = tdf.tableRowItem(
      'row1',
      tdf.tableRowData('1', 1), tdf.tableRowData('2', 'value2'));
    let row2: TableRowItem = tdf.tableRowItem(
      'row2',
      tdf.tableRowData('1', 15));

    const tableRow = shallow(<TableBody rows={[row1, row2]} headers={headers} />);
    expect(toJson(tableRow)).toMatchSnapshot();
  });
});
