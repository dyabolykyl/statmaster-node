import './Table.css';

import * as React from 'react';

import { TableHeaderItem } from './types';
import getLogger from '../util/log';

const log = getLogger('StatsTableTest');

export interface TableHeaderProps {
  headers: Array<TableHeaderItem>;
  onColumnClick: (header: TableHeaderItem) => void;
}

export class TableHeader extends React.Component<TableHeaderProps> {
  static defaultProps = {
    headers: [],
    onColumnClick: (key: TableHeaderItem) => null,
  };

  renderDescription(header: TableHeaderItem) {
    if (header.description) {
      return (
        <span className="header-description">
          {header.description}
        </span>
      );
    }
    return '';
  }

  render() {
    let { headers, onColumnClick } = this.props;
    log.debug(`Rendering headers: ${headers}`);
    return (
      <div className="table-header">
        {
          headers.map((header: TableHeaderItem) => (
            <div
              key={header.fieldKey.flatKey}
              // TODO default value should be defined elsewhere
              className={`${header.fieldKey.topic} ${header.fieldKey.flatKey} table-head`}
              onClick={() => onColumnClick(header)}
            >
              <div className="header-display">
                {header.label}
              </div>
              {this.renderDescription(header)}
            </div>
          ))
        }
      </div>
    );
  }
}
