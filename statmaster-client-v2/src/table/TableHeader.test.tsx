import { shallow } from 'enzyme';
import * as React from 'react';
import * as tdf from '../util/testDataFactory';
import { TableHeader } from './TableHeader';
import { TableHeaderItem } from './types';
import { Field } from 'statmaster-common';
import toJson from 'enzyme-to-json';

describe('<TableHeader />', () => {
  test('onColumnClick', () => {
    let onColumnClick = jest.fn();
    let headerItem1: TableHeaderItem = tdf.arrangeTableHeaderItem();
    let headerItem2: TableHeaderItem = tdf.arrangeTableHeaderItem();
    let tableHeader = shallow(<TableHeader headers={[headerItem1, headerItem2]} onColumnClick={onColumnClick} />);

    tableHeader.find(`.${headerItem1.fieldKey.flatKey}`).simulate('click');
    expect(onColumnClick).toHaveBeenCalledTimes(1);
    expect(onColumnClick).toBeCalledWith(headerItem1);
    
    tableHeader.find(`.${headerItem2.fieldKey.flatKey}`).simulate('click');
    expect(onColumnClick).toHaveBeenCalledTimes(2);
    expect(onColumnClick).toBeCalledWith(headerItem2);
  });

  test('should render with headers', () => {
    let field1: Field = tdf.arrangeField('f1');
    let field2: Field = tdf.arrangeField('f2');
    
    const tableHeader = shallow((
    <TableHeader 
      headers={[tdf.tableHeaderItemFromField(field1), tdf.tableHeaderItemFromField(field2)]} 
      onColumnClick={jest.fn()} 
    />
    ));
    
    expect(toJson(tableHeader)).toMatchSnapshot();
  });
});
