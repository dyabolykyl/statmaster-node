import { FieldKey, FieldValue } from 'statmaster-common';

export interface TableRowItem {
  key: string;
  data: Map<string, TableRowData>;
}

export interface TableRowData {
  key: string;
  value: FieldValue;
}

export interface TableHeaderItem {
  fieldKey: FieldKey;
  label: string;
  description?: string;
}
