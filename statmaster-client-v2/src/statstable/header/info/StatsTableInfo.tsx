import './StatsTableInfo.css';

import * as React from 'react';

import { Game } from 'statmaster-common';
import { LocalDateTime } from 'js-joda';
import { RootState } from '../../../core/reducer';
import { connect } from 'react-redux';
import { displayDate } from '../../../util/date';
import { getDateTime } from '../../../session/selectors';
import { getGames } from '../../selectors';

interface StateProps {
  datetime: LocalDateTime;
  games: Game[];
}

class StatsTableInfoContainer extends React.Component<StateProps & null> {
  private sport: HTMLSelectElement;
  private statsType: HTMLSelectElement;

  render() {
    const { games, datetime } = this.props;

    return (
      <div className="stats-table-info">
        <div>
          <select name="sport" ref={sport => this.sport = sport as HTMLSelectElement}>
            {/* TODO: Replace with dynamic sports list */}
            <option key="MLB" value="MLB">MLB</option>
          </select>
          <select name="type" ref={statsType => this.statsType = statsType as HTMLSelectElement}>
            <option key="team" value="team">Team</option>
          </select>
          <span>Stats & Games</span>
        </div>
        <div>
          <p>{displayDate(datetime.toLocalDate())}</p>
        </div>
        <div className="games-info">
          <p>{games.length} Games</p>
          {/* TODO: Get lineups */}
          <p>{games.length} Confirmed Lineups</p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return { 
    datetime: getDateTime(state) ,
    games: getGames(state),
  };
}

export const StatsTableInfo = connect(mapStateToProps, null)(StatsTableInfoContainer);
