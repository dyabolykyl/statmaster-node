import './StatsTableColumnControls.css';

import * as React from 'react';

import { Categories, Category, Field, Multimap, Topics } from 'statmaster-common';
import { Dispatch, connect } from 'react-redux';

import { RootState } from '../../../core/reducer';
import { createTopicDisplay } from '../../../tablecontrols/columns/columns';
import { getFieldsByCategory } from '../../selectors';

interface StateProps {
  // topics: Set<Topic>;
  // categories: Set<Category>;
  fields: Multimap<Category, Field>;
}

interface DispatchProps {
  toggleHeatMap: (checked: boolean) => void;
}

class StatsTableColumnControlsContainer extends React.Component<StateProps & DispatchProps> {
  private categorySelect: HTMLSelectElement;
  private topicSelect: HTMLSelectElement;
  private fieldSelect: HTMLSelectElement;

  renderSelection(name: string, options: Map<string, string>, assignment: (val: HTMLSelectElement) => void) {
    const optionElements: any[] = [];
    options.forEach((display, key) => optionElements.push(
        <option key={key} value={key}>{display}</option>
      )
    );

    return (
      <select id={name} name={name} ref={assignment}>
        {
          optionElements
        }
      </select>
    );
  }

  renderCategorySelection() {
    const optionsMap = new Map<string, string>();
    Categories
    .filter(c => c !== Category.INFO)
    .forEach(c => optionsMap.set(c, c.charAt(0).toUpperCase() + c.slice(1)));

    return this.renderSelection(
      'categorySelect',
      optionsMap,
      categorySelect => this.categorySelect = categorySelect);
  }

  renderTopicSelection() {
    const optionsMap = new Map<string, string>();
    Topics.forEach(t => optionsMap.set(t, createTopicDisplay(t)));

    return this.renderSelection(
      'topicSelect', 
      optionsMap, 
      topicSelect => this.topicSelect = topicSelect);
  }

  renderFieldSelection(fields: Set<Field>) {
    const optionsMap = new Map<string, string>();
    fields.forEach(field => optionsMap.set(field.id, field.label));

    // https://jsfiddle.net/88cxzhom/27/
    return this.renderSelection(
      'fieldSelect',
      optionsMap,
      val => this.fieldSelect = val);
  }

  getCurrentCategory(): Category {
    if (this.categorySelect && this.categorySelect.value) {
      return this.categorySelect.value as Category;
    }

    return Category.BATTING;
  }

  onHeatMapToggled = (event: any) => {
    this.props.toggleHeatMap(event.target.checked);
  }

  render() {
    const { fields } = this.props;
    const categoryFields = fields.getAll(this.getCurrentCategory());

    return (
      <div className="stats-table-column-controls">
        <h2>Column Controls</h2>
        <div className="column-selections">
          <div>
            {this.renderCategorySelection()}
            {this.renderTopicSelection()}
            {this.renderFieldSelection(categoryFields)}
          </div>
          <div>
            <button>Add Stat Group ></button>
            <button>Add Stat ></button>
          </div>
        </div>
        <div>
          <label>
            <input id="heatmap-checkbox" type="checkbox" className="checkbox" onClick={this.onHeatMapToggled}/>
            Heat Map
          </label>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  let fields = getFieldsByCategory(state);
  return {
    fields
  };
}

function mapDispatchToProps(dispatch: Dispatch<RootState>): DispatchProps {
  return {
    toggleHeatMap: checked => null // TODO: implement heat map
  };
}

export const StatsTableColumnControls = connect(mapStateToProps, mapDispatchToProps)(StatsTableColumnControlsContainer);
