import './StatsTableHeader.css';

import * as React from 'react';

import { StatsTableColumnControls } from './columns/StatsTableColumnControls';
import { StatsTableConfiguration } from './configuration/StatsTableConfiguration';
import { StatsTableInfo } from './info/StatsTableInfo';

export class StatsTableHeader extends React.Component {

  render() {
    return (
      <div className="stats-table-header">
        <StatsTableInfo />
        <StatsTableConfiguration />
        <StatsTableColumnControls />
      </div>
    );
  }
}
