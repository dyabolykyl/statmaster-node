import './StatsTableConfiguration.css';

import * as React from 'react';

import { Field, FieldId, FilterTree, Sort } from 'statmaster-common';
import { getFields, getFilterState } from '../../../tablecontrols/selectors';

import { RootState } from '../../../core/reducer';
import { connect } from 'react-redux';
import { getSort } from '../../selectors';
import { getSortDisplay } from '../../../tablecontrols/sorting/sorting';

interface StateProps {
  sort: Sort;
  fields: Map<FieldId, Field>;
  filterTree: FilterTree;
}

class StatsTableConfigurationContainer extends React.Component<StateProps & null> {

  render() {
    const { sort, fields } = this.props;

    return (
      <div className="stats-table-configuration">
        <p>Sorting on <span>{getSortDisplay(sort, fields.get(sort.columnKey.fieldId))}</span></p>
        <ul>
          <p>Filters</p>
          <li><input type="checkbox" className="checkbox"/>Playing today</li>
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  let fields = getFields(state);
  let sortState = getSort(state);
  let filterTree = getFilterState(state).filterTree;
  return {
    sort: sortState.sort,
    fields,
    filterTree
  };
}

export const StatsTableConfiguration = connect(mapStateToProps, null)(StatsTableConfigurationContainer);
