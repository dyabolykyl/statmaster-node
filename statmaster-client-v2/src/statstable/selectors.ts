import {
  Category,
  Field,
  FieldKey,
  Game,
  Matchup,
  Multimap,
  ResourceId,
  StatGroup,
  Team,
  Topic,
  makeMatchupKey
} from 'statmaster-common';
import { DEFAULT_SORT, SortState, sortResourceContainers } from '../tablecontrols/sorting/sorting';
import { TableHeaderItem, TableRowData, TableRowItem } from '../table/types';
import {
  getColumns,
  getFields,
  getFilterState,
  getScrollableColumns,
  getSizedFixedHeaders
} from '../tablecontrols/selectors';

import { FilterState } from '../tablecontrols/filtering/reducer';
import ResourceContainer from '../statsdata/ResourceContainer';
import { ResourceDataValue } from 'statmaster-common';
import { RootState } from '../core/reducer';
import { SizedHeaderItem } from './table/FixedTableHeaderRow';
import { createHeaderLabel } from '../tablecontrols/columns/columns';
import { createSelector } from 'reselect';
import getLogger from '../util/log';

const log = getLogger('filtering');

export function getStatGroups(state: RootState): Map<ResourceId, StatGroup> {
  return state.dataState ? state.dataState.statGroups : new Map();
}
export function getTeams(state: RootState): Map<ResourceId, Team> {
  return state.dataState ? state.dataState.teams : new Map();
}
export function getMatchups(state: RootState): Map<ResourceId, Matchup> {
  return state.dataState ? state.dataState.matchups : new Map();
}
export function getGames(state: RootState): Game[] {
  return state.dataState ? state.dataState.games : [];
}
export function getSort(state: RootState): SortState {
  return state.tableState ? state.tableState.sortState : { sort: DEFAULT_SORT };
}

export const getFieldsByCategory = createSelector(
  getFields,
  (fields: Map<string, Field>): Multimap<Category, Field> => {
    const fieldsByCategory = new Multimap<Category, Field>();
    fields.forEach((field, fieldId) => {
      fieldsByCategory.put(field.category, field);
    });
    return fieldsByCategory;
  }
);

/** DEPRECATED */
export const createHeaders = createSelector(
  getFields, getColumns,
  (fields: Map<string, Field>, columns: FieldKey[]): TableHeaderItem[] => {
    log.trace('createHeaders');
    const headers: TableHeaderItem[] = [];
    if (!fields || !columns) {
      return headers;
    }

    columns.forEach((fieldKey) => {
      const field: Field = fields.get(fieldKey.fieldId) as Field;
      const header: TableHeaderItem = {
        // TODO add columns to state and combine with fields to 
        // get headers
        fieldKey,
        label: createHeaderLabel(fieldKey.topic, field ? field.label : 'Unknown'),
        description: createHeaderLabel(fieldKey.topic, field ? field.description : 'Unknown')
      };
      headers.push(header);
    });
    return headers;
  }
);

export const createScrollableHeaders = createSelector(
  getFields, getScrollableColumns,
  (fields: Map<string, Field>, columns: FieldKey[]): TableHeaderItem[] => {
    log.trace('createHeaders');
    const headers: TableHeaderItem[] = [];
    if (!fields || !columns) {
      return headers;
    }

    columns.forEach((fieldKey) => {
      const field: Field = fields.get(fieldKey.fieldId) as Field;
      const header: TableHeaderItem = {
        fieldKey,
        label: field ? field.label : 'Unknown',
        description: createHeaderLabel(fieldKey.topic, field ? field.description : 'Unknown')
      };
      headers.push(header);
    });
    return headers;
  }
);

/**
 * Takes teams, stats, matchups, and other metadata and combines into a single structure
 * that is easily sorted, filtered, and otherwise operated on
 */
// TODO: Move to state and create in reducer. Doesn't need to be a selector
export const createResourceContainers = createSelector(
  getTeams, getMatchups, getStatGroups,
  ( teams: Map<ResourceId, Team>,
    matchups: Map<ResourceId, Matchup>,
    statGroups: Map<ResourceId, StatGroup>): Map<ResourceId, ResourceContainer> => {
    log.trace('createResourceContainers');
    const resourceContainers: Map<ResourceId, ResourceContainer> = new Map();

    teams.forEach((team, teamId, map) => {
      let matchup: Matchup = matchups.get(teamId) as Matchup;
      let opponent: Team | undefined = undefined;
      if (matchup !== undefined) {
        opponent = teams.get(matchup.opponent);
      }

      const container: ResourceContainer = new ResourceContainer(team, opponent);
      container.addStatGroupData(statGroups.get(teamId), Topic.RESOURCE);

      if (matchup !== undefined) {
        container.addStatGroupData(statGroups.get(matchup.opponent), Topic.OPPOSING);
        container.addStatGroupData(statGroups.get(makeMatchupKey(matchup)), Topic.RESOURCE_VS_OPPOSING);
        let opposingMatchup = matchups.get(matchup.opponent);
        if (opposingMatchup !== undefined && opposingMatchup.opponent === teamId) {
          container.addStatGroupData(statGroups.get(makeMatchupKey(opposingMatchup)), Topic.OPPOSING_VS_RESOURCE);
        }
      }
      resourceContainers.set(teamId, container);
    });
    return resourceContainers;
  }
);

export const createSortedResourceContainers = createSelector(
  getSort,
  createResourceContainers,
  (sortState: SortState, containers: Map<ResourceId, ResourceContainer>): ResourceContainer[] => {
    log.trace('createSortedResourceContainers');
    return sortResourceContainers(containers, sortState.sort);
  }
);

export const createFilteredResourceContainers = createSelector(
  getFilterState,
  createSortedResourceContainers,
  (filterState: FilterState, containers: ResourceContainer[]): ResourceContainer[] => {
    log.trace('createFilteredResourceContainers');
    return containers.filter(container => filterState.filterTree.accept(container.data));
  }
);

export const createFixedTableRows = createSelector(
  createFilteredResourceContainers,
  getSizedFixedHeaders,
  (containers: ResourceContainer[], fixedHeaders: SizedHeaderItem[]): ResourceDataValue[][] => {
    return containers.map(container => {
      return fixedHeaders.map(fixedHeader => 
        container.data.get((fixedHeader.header as TableHeaderItem).fieldKey.flatKey) as ResourceDataValue);
    });
  }
);

export const createScrollableTableRows = createSelector(
  createFilteredResourceContainers,
  createScrollableHeaders,
  (containers: ResourceContainer[], headers: TableHeaderItem[]): ResourceDataValue[][] => {
    return containers.map(container => {
      return headers.map(header => 
        container.data.get(header.fieldKey.flatKey) as ResourceDataValue);
    });
  }
);

/** DEPRECATED */
export const createStatsTableRows = createSelector(
  createFilteredResourceContainers,
  (containers: ResourceContainer[]): TableRowItem[] => {
    log.trace('createStatsTableRows');
    const rows: TableRowItem[] = [];
    if (!containers) {
      return rows;
    }

    containers.forEach((container) => {
      const rowData: Map<string, TableRowData> = new Map();
      container.data.forEach((dataValue, key) => {
        rowData.set(key, { key: key, value: dataValue.value });
      });
      let row: TableRowItem = {
        key: container.resourceId,
        data: rowData,
      };
      rows.push(row);
    });
    return rows;
  }
);
