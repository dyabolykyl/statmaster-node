import * as React from 'react';

import { StatsTable } from './table/StatsTable';
import { StatsTableHeader } from './header/StatsTableHeader';

export class StatsTableMain extends React.Component {

  render() {
    return (
      <main id="stats-table-main">
        <StatsTableHeader />
        <StatsTable />
      </main>
    );
  }
}
