import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Table } from '../table/Table';
import { TableHeaderItem, TableRowItem } from '../table/types';
import { RootState } from '../core/reducer';
import './StatsTable.css';
import { createHeaders, createStatsTableRows } from './selectors';
import { makeUpdateSortAction } from '../tablecontrols/sorting/sorting';
import getLogger from '../util/log';
const log = getLogger('statstable');

interface StateProps {
  headers: Array<TableHeaderItem>;
  rows: Array<TableRowItem>;
}

interface DispatchProps {
  onColumnClick: (header: TableHeaderItem) => void;
}

export class StatsTableWrapper extends React.Component<StateProps & DispatchProps> {
  render() {
    return (
      <div className="stats-table">
        <Table headers={this.props.headers} rows={this.props.rows} onColumnClick={this.props.onColumnClick} />
      </div>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => {
  log.debug(`Constructing table from stats data`);
  let headers: Array<TableHeaderItem> = createHeaders(state);
  let rows: TableRowItem[] = createStatsTableRows(state);
  let stateProps = {
    headers,
    rows
  };
  return stateProps;
};

const mapDispatchToProps = (dispatch: Dispatch<RootState>): DispatchProps => {
  return {
    onColumnClick: (column: TableHeaderItem) => {
      dispatch(makeUpdateSortAction(column.fieldKey));
    }
  };
};

export const StatsTable = connect(mapStateToProps, mapDispatchToProps)(StatsTableWrapper);