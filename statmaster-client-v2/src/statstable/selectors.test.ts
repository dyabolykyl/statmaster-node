import * as tdf from '../util/testDataFactory';

import {
   Field,
   FieldKey,
   Matchup,
   ResourceId,
   StatGroup,
   Team,
   Topic,
   makeFieldKey,
   makeMatchupKey
} from 'statmaster-common';
import { TableHeaderItem, TableRowItem } from '../table/types';
import { createHeaders, createResourceContainers, createStatsTableRows } from './selectors';

import ResourceContainer from '../statsdata/ResourceContainer';
import { ResourceDataValue } from 'statmaster-common';
import { createHeaderLabel } from '../tablecontrols/columns/columns';

jest.mock('../statsdata/ResourceContainer');

describe('HeadersTest', () => {

  test('EmptyFieldsProduceEmptyHeaders', () => {
    let headers: TableHeaderItem[] = createHeaders.resultFunc(new Map(), []);
    expect(headers.length).toBe(0);
  });

  test('FieldsProduceHeaders', () => {
    let field1: Field = tdf.arrangeField();
    let field2: Field = tdf.arrangeField();
    let fields: Map<string, Field> = new Map([
      [field1.id, field1],
      [field2.id, field2]
    ]);
    let column1: FieldKey = makeFieldKey(Topic.RESOURCE, field1.category, field1.id);
    let column2: FieldKey = makeFieldKey(Topic.OPPOSING, field1.category, field1.id);
    let column3: FieldKey = makeFieldKey(Topic.RESOURCE, field2.category, field2.id);
    let columns = [column1, column2, column3];

    let headers: TableHeaderItem[] = createHeaders.resultFunc(fields, columns);
    expect(headers.length).toBe(3);
    assertHeader(headers[0], column1, field1);
    assertHeader(headers[1], column2, field1);
  });
});

const assertHeader = (header: TableHeaderItem, column: FieldKey, field: Field): void => {
  expect(header.fieldKey).toEqual(column);
  expect(header.description).toBe(createHeaderLabel(column.topic, field.description));
  expect(header.label).toBe(createHeaderLabel(column.topic, field.label));
};

describe('createResourceContainers', () => {
  let teams: Map<ResourceId, Team>;
  let resourceStatGroups: Map<ResourceId, StatGroup>;
  let matchups: Map<ResourceId, Matchup>;

  beforeEach(() => {
    teams = new Map();
    resourceStatGroups = new Map();
    matchups = new Map();
  });

  test('EmptyTeamsTest', () => {
    let statGroup1 = tdf.arrangeStatGroup(
      'team1',
      's1',
      tdf.arrangeStat(tdf.arrangeFieldKey(tdf.arrangeField('f1')), 4));
    resourceStatGroups.set('team1', statGroup1);
    const resourceContainers = createResourceContainers.resultFunc(teams, matchups, resourceStatGroups);
    expect(resourceContainers.size).toBe(0);
  });

  test('TeamsWithMatchupsAndstats', () => {
    let team1 = tdf.arrangeTeam('1');
    let team2 = tdf.arrangeTeam('2');
    teams.set(team1.id, team1);
    teams.set(team2.id, team2);
    let matchup = tdf.arrangeMatchup(team1.id, team2.id);
    let matchup2 = tdf.arrangeMatchup(team2.id, team1.id);
    matchups.set(team1.id, matchup);
    matchups.set(team2.id, matchup2);
    let statGroup1 = tdf.randomStatGroup(team1.id);
    let statGroup2 = tdf.randomStatGroup(team2.id);
    let statGroup3 = tdf.randomStatGroup(makeMatchupKey(matchup);
    resourceStatGroups.set(team1.id, statGroup1);
    resourceStatGroups.set(team2.id, statGroup2);
    resourceStatGroups.set(makeMatchupKey(matchup), statGroup3);
    resourceStatGroups.set('unknown team', tdf.randomStatGroup('unknown team'));

    const containers = createResourceContainers.resultFunc(teams, matchups, resourceStatGroups);

    expect(containers.size).toBe(2);
    assertContainer(containers.get(team1.id) as ResourceContainer, statGroup1, statGroup2, statGroup3, undefined);
    assertContainer(containers.get(team2.id) as ResourceContainer, statGroup2, statGroup1, undefined, statGroup3);
  });

  const assertContainer = (
    container: ResourceContainer, 
    resourceStats: StatGroup | undefined,
    opposingStats: StatGroup | undefined,
    matchupStats: StatGroup | undefined,
    opposingMatchupStats: StatGroup | undefined) => {
    expect(container.addStatGroupData).toHaveBeenCalledTimes(4);
    expect(container.addStatGroupData).toHaveBeenCalledWith(resourceStats, Topic.RESOURCE);
    expect(container.addStatGroupData).toHaveBeenCalledWith(opposingStats, Topic.OPPOSING);
    expect(container.addStatGroupData).toHaveBeenCalledWith(matchupStats, Topic.RESOURCE_VS_OPPOSING);
    expect(container.addStatGroupData).toHaveBeenCalledWith(opposingMatchupStats, Topic.OPPOSING_VS_RESOURCE);
  };  

});

describe('RowsTest', () => {
  let containers: ResourceContainer[];

  beforeEach(() => {
    containers = [];
  });

  test('EmptyTeams', () => {
    const rows: TableRowItem[] = createStatsTableRows.resultFunc(containers);
    expect(rows.length).toBe(0);
  });

  test('Team', () => {
    let container1 = new ResourceContainer(tdf.arrangeTeam('a')) as jest.Mocked<ResourceContainer>;
    let data: Map<string, ResourceDataValue> = new Map([
      ['d1', { key: tdf.arrangeFieldKey(), value: 5} ]
    ]);
    Object.defineProperty(container1, 'data', {value: data});
    containers.push(container1);

    const rows: TableRowItem[] = createStatsTableRows.resultFunc(containers);

    assertRow(rows[0], container1);
    // assertRow(rows[1], opponent, team);

  });

  const assertRow = (row: TableRowItem, container: jest.Mocked<ResourceContainer>): void => {
    expect(row.key).toBe(container.resourceId);
    expect(row.data.size).toBe(container.data.size);
  };
});
