import { Store } from 'redux';
import * as React from 'react';
import { StatsTable, StatsTableWrapper } from './StatsTable';
import { shallow, mount } from 'enzyme';
import { Table } from '../table/Table';
import { RootState } from '../core/reducer';
import { Provider } from 'react-redux';
import { OutputSelector } from 'reselect';
import reducer from '../core/reducer';
import { createStore } from 'redux';
import * as tdf from '../util/testDataFactory';

/* tslint:disable:no-any */
const isSelector = (obj: any): obj is OutputSelector<any, any, any> => {
  return obj.resetRecomputations !== undefined;
};

describe('StatsTable', () => {
  beforeEach(() => {
    let selectors = require('./selectors');
    Object.keys(selectors).forEach(key => {
      let func = selectors[key];
      if (isSelector(func)) {
        /* tslint:disable:no-any */
        let selector: OutputSelector<any, any, any> = func;
        selector.resetRecomputations();
      }
    });
  });

  describe('<StatsTableWrapper />', () => {
    test('renders', () => {
      const onColumnClick = jest.fn();
      const statsTable = shallow(<StatsTableWrapper rows={[]} headers={[]} onColumnClick={onColumnClick} />);
      expect(statsTable.find(Table)).toHaveLength(1);
      expect(statsTable.find(Table).prop('rows')).toEqual([]);
      expect(statsTable.find(Table).prop('headers')).toEqual([]);
      expect(statsTable.find(Table).prop('onColumnClick')).toEqual(onColumnClick);
    });
  });

  describe('<StatsTable', () => {
    let store: Store<RootState>;

    const setStore = (state?: RootState) => {
      store = createStore(reducer, state) as Store<RootState>;
    };

    beforeEach(() => {
      setStore();
    });

    test('renders with data', () => {
      let teamMap = tdf.arrangeTeams('1', '2', '3');
      let fields = tdf.arrangeFields(tdf.arrangeField('city'), tdf.arrangeField('name'), tdf.arrangeField('unknown'));
      let dataState = tdf.arrangeDataState(teamMap, undefined, undefined, fields);
      let state: RootState = tdf.arrangeRootState(undefined, dataState);
      setStore(state);

      mount(<Provider store={store}><StatsTable /></Provider>);
    });
  });
});
