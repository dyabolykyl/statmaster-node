import * as React from 'react';

import { Sort } from 'statmaster-common';
import { TableHeaderItem } from '../../table/types';

export interface SizedHeaderItem {
  header: TableHeaderItem;
  width: string;
}

interface Props {
  headers: SizedHeaderItem[];
  onColumnClick?: (header: TableHeaderItem) => void;
  sort: Sort;
  left?: number;
}

export class FixedTableHeaderRow extends React.Component<Props> {

  createHeader = (headerItem: SizedHeaderItem, index: number) => {
    const { onColumnClick, sort } = this.props;
    const style: any = {
      width: headerItem.width,
    };
    // if (left !== undefined) {
    //   if (index === 0) {
    //     style.position = 'absolute';
    //   }
    //   style.left = -left + 'px';
    // }
    const classNames = ['fixed-table__th'];
    if (headerItem.header.fieldKey.flatKey === sort.columnKey.flatKey) {
      classNames.push('sorted');
    }

    // TODO: add onclick and hover
    return (
      <div
        key={headerItem.header.fieldKey.flatKey}
        style={style}
        className={classNames.join(' ')}
        onClick={onColumnClick ? () => onColumnClick(headerItem.header) : () => null}
      >
        {headerItem.header.label}
      </div>
    );

  }

  createHeaders() {
    const headers = this.props.headers;
    if (headers.length === 0) {
      return;
    }

    return headers.map((headerItem, index) => this.createHeader(headerItem, index));
  }

  render() {
    return (
      <div className="fixed-table__header-row">
        {this.createHeaders()}
      </div>
    );
  }
}
