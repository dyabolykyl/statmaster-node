import * as React from 'react';

import { FixedTableListRow } from './FixedTableListRow';
import ResourceContainer from '../../statsdata/ResourceContainer';
import { SizedHeaderItem } from './FixedTableHeaderRow';
import { TableSizing } from './FixedTable';

export interface Props {
  fixed: boolean;
  sizing: TableSizing;
  headers: SizedHeaderItem[];
  rows: ResourceContainer[];
  setScrollContainer?: (ref: HTMLDivElement) => void;
  setUpdateFixedListTop?: (updateTop: (top: number) => void) => void;
}

export interface State {
  top: number;
}

export class FixedTableList extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    if (props.setUpdateFixedListTop !== undefined) {
      props.setUpdateFixedListTop(this.updateTop);
    }
    this.state = { top: 0 };
  }

  setScrollRef = (ref: HTMLDivElement) => {
    if (this.props.setScrollContainer) {
      this.props.setScrollContainer(ref);
    }
  }

  updateTop = (top: number) => {
    console.log(`Setting top to ${top}`);
    this.setState({ top: top });
  }
  
  render() {
    const { fixed, rows, sizing, headers } = this.props;
    console.log(`Render FixedTableList--${fixed}`);

    const style: any = {};
    if (!fixed) {
      style.top = `calc(${sizing.headerRowCount * sizing.headerHeight}px)`;
      style.height = `calc(100% - ${sizing.headerHeight * sizing.headerRowCount}px)`;
    } else {
      style.top = this.state.top + 'px';
      style.height = '100%';
    }

    return (
      <div
        style={style}
        className={`fixed-table__list ${fixed ? 'fixed-table__list--fixed' : ''}`}
        ref={this.setScrollRef}
      >
        {
          rows.map((row, index) => (
            <FixedTableListRow key={index} headerItems={headers} row={row} sizing={sizing} />
          ))
        }
      </div>
    );
  }
}
