import * as React from 'react';

import { FixedTableHeader } from './FixedTableHeader';
import { FixedTableList } from './FixedTableList';
import ResourceContainer from '../../statsdata/ResourceContainer';
import { SizedHeaderItem } from './FixedTableHeaderRow';
import { Sort } from 'statmaster-common';
import { TableHeaderItem } from '../../table/types';
import { TableSizing } from './FixedTable';

export interface Props {
  sizing: TableSizing;
  headerItems: SizedHeaderItem[];
  rows: ResourceContainer[];
  setUpdateFixedListTop: (setUpdateTop: (top: number) => void) => void;
  onColumnClick: (header: TableHeaderItem) => void;
  sort: Sort;
}

export class FixedTableSidebar extends React.Component<Props> {
  render() {
    const { sizing, headerItems, rows, setUpdateFixedListTop, onColumnClick, sort } = this.props;

    const width = sizing.sidebarWidth + 'px';
    const sidebarStyle = {
      width,
    };

    const scrollableSidebarStyle = {
      height: `calc(100% - ${sizing.headerHeight * sizing.headerRowCount}px)`,
      top: sizing.headerRowCount * sizing.headerHeight + 'px',
    };

    const headerRows = Array(sizing.headerRowCount).fill([]);
    headerRows[sizing.headerRowCount - 1] = headerItems;

    return (
      <div className="fixed-table__sidebar" style={sidebarStyle}>
        <FixedTableHeader headerRows={headerRows} sizing={sizing} onColumnClick={onColumnClick} sort={sort} />
        <div className="fixed-table__scrollable-sidebar" style={scrollableSidebarStyle}>
          <FixedTableList
            setUpdateFixedListTop={setUpdateFixedListTop}
            headers={headerItems}
            fixed={true}
            rows={rows}
            sizing={sizing}
          />
        </div>
      </div>
    );
  }
}
