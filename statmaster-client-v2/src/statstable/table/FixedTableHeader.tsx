import * as React from 'react';

import { FixedTableHeaderRow, SizedHeaderItem } from './FixedTableHeaderRow';

import { Sort } from 'statmaster-common';
import { TableHeaderItem } from '../../table/types';
import { TableSizing } from './FixedTable';

interface Props {
  sizing: TableSizing;
  headerRows: SizedHeaderItem[][];
  setUpdateFixedHeaderLeft?: (updateLeft: (top: number) => void) => void;
  onColumnClick: (header: TableHeaderItem) => void;
  sort: Sort;
}

export interface State {
  left?: number;
}

export class FixedTableHeader extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    if (props.setUpdateFixedHeaderLeft !== undefined) {
      props.setUpdateFixedHeaderLeft(this.updateLeft);
    }
    this.state = { left: 0 };
  }

  updateLeft = (left: number) => {
    this.setState({ left: left });
  }

  /**
   * Divide header items into grouping maps
   * Determine total number of headers in each grouping
   */

  render() {
    const { sizing, headerRows, setUpdateFixedHeaderLeft, onColumnClick, sort } = this.props;

    const headerStyle: any = {
      height: sizing.headerRowCount * sizing.headerHeight + 'px',
    };

    const rows = [];

    for (let index = 0; index < headerRows.length; index++) {
      const headerItems = headerRows[index];
      let row;
      if (index === headerRows.length - 1) {
        row = (<FixedTableHeaderRow key={index} headers={headerItems} onColumnClick={onColumnClick} sort={sort} />);
      } else {
        row = (<FixedTableHeaderRow left={this.state.left} key={index} headers={headerItems} sort={sort} />);
      }
      rows.push(row);
    }
    headerRows.forEach((headerItems, index) => (
      <FixedTableHeaderRow key={index} headers={headerItems} onColumnClick={onColumnClick} sort={sort} />)
    );

    if (setUpdateFixedHeaderLeft) {
      const scrollableStyle = { left: this.state.left };
      return (
        <div style={headerStyle} className="fixed-table__header">
          <div style={scrollableStyle} className="fixed-table__scrollable-header">
            {rows}
          </div>
        </div>
      );
    }

    return (
      <div style={headerStyle} className="fixed-table__header">
        {rows}
      </div>
    );
  }
}
