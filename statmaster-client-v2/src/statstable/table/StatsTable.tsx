import './StatsTable.css';
import 'react-perfect-scrollbar/dist/css/styles.css';

import * as React from 'react';

import { Dispatch, connect } from 'react-redux';
import { FixedTable, TableData, TableSizing } from './FixedTable';
import { createFilteredResourceContainers, createScrollableHeaders, getSort } from '../selectors';

import { RootState } from '../../core/reducer';
import { Sort } from 'statmaster-common';
import { TableHeaderItem } from '../../table/types';
import { getSizedFixedHeaders } from '../../tablecontrols/selectors';
import { makeUpdateSortAction } from '../../tablecontrols/sorting/sorting';

export const COLUMN_WIDTH = 70;
export const HEADER_HEIGHT = 28;
export const SIDEBAR_WIDTH = 220;
export const HEADER_ROW_COUNT = 3;

interface StateProps {
  tableData: TableData;
  sort: Sort;
}

interface DispatchProps {
  onColumnClick: (header: TableHeaderItem) => void;
}

export class StatsTableContainer extends React.Component<StateProps & DispatchProps> {
  render() {
    const { tableData, onColumnClick, sort } = this.props;

    const sizing: TableSizing = {
      headerRowCount: HEADER_ROW_COUNT,
      headerHeight: HEADER_HEIGHT,
      columnWidth: COLUMN_WIDTH,
      sidebarWidth: SIDEBAR_WIDTH
    };

    return (
      <div id="stats-table-sticky">
        <FixedTable data={tableData} sizing={sizing} onColumnClick={onColumnClick} sort={sort}  />
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  const rows = createFilteredResourceContainers(state);
  return {
    tableData: {
      sidebarHeaderItems: getSizedFixedHeaders(),
      sidebarRows: rows,
      mainHeaderItems: createScrollableHeaders(state),
      mainRows: rows
    },
    sort: getSort(state).sort
  };
}

const mapDispatchToProps = (dispatch: Dispatch<RootState>): DispatchProps => {
  return {
    onColumnClick: (column: TableHeaderItem) => {
      dispatch(makeUpdateSortAction(column.fieldKey));
    }
  };
};

export const StatsTable = connect(mapStateToProps, mapDispatchToProps)(StatsTableContainer);