import * as React from 'react';

import ResourceContainer from '../../statsdata/ResourceContainer';
import { SizedHeaderItem } from './FixedTableHeaderRow';
import { TableSizing } from './FixedTable';

export interface Props {
  sizing: TableSizing;
  headerItems: SizedHeaderItem[];
  row: ResourceContainer;
}

function isNumber(n: any) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isDecimal(n: any) {
  return isNumber(n) && !Number.isInteger(n);
}

export class FixedTableListRow extends React.Component<Props> {

  createRow = (index: number) => {
    const { headerItems, row, sizing } = this.props;

    const headerItem = headerItems[index];
    const cell = row.data.get(headerItem.header.fieldKey.flatKey);
    const value = cell ? cell.value : '-';
    const style = {
      width: headerItem ? headerItem.width : sizing.columnWidth
    };

    let displayValue = value;
    if (isDecimal(value)) {
      displayValue = (value as Number).toFixed(3);
    }

    return (
      <div key={index} style={style} className="fixed-table__td">{displayValue}</div>
    );
  }

  render() {
    const { row, headerItems } = this.props;

    return (
      <div className={`fixed-table__list-row ${row.isPlaying ? 'playing' : ''}`}>
        {
          headerItems.map((header, index) => this.createRow(index))
        }
      </div>
    );
  }
}
