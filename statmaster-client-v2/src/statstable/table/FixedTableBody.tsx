import * as React from 'react';

import { Category, Sort, Topic, makeFieldKey } from 'statmaster-common';
import { createCategoryDisplay, createTopicDisplay } from '../../tablecontrols/columns/columns';

import { FixedTableHeader } from './FixedTableHeader';
import { FixedTableList } from './FixedTableList';
import ResourceContainer from '../../statsdata/ResourceContainer';
// import PerfectScrollbar from 'react-perfect-scrollbar';
import { SizedHeaderItem } from './FixedTableHeaderRow';
import { TableHeaderItem } from '../../table/types';
import { TableSizing } from './FixedTable';

const MIN_GROUP_WIDTH = 95;

export interface Props {
  sizing: TableSizing;
  headerItems: TableHeaderItem[];
  rows: ResourceContainer[];
  setScrollContainer: (ref: HTMLDivElement) => void;
  setUpdateFixedHeaderLeft: (updateLeft: (top: number) => void) => void;
  onColumnClick: (header: TableHeaderItem) => void;
  sort: Sort;
}

export class FixedTableBody extends React.Component<Props> {

  calculateSize = (count: number): number => {
    if (count === 1) {
      return MIN_GROUP_WIDTH;
    }
    
    const sizing = this.props.sizing;
    return count * sizing.columnWidth + (count - 1); // (-1 for border)
  }

  render() {
    const { sizing, headerItems, rows, 
      setScrollContainer, setUpdateFixedHeaderLeft, onColumnClick, sort } = this.props;

    // assumes headers are properly sorted
    const categoryMap = new Map<Category, Map<Topic, number>>();
    headerItems.forEach(headerItem => {
      let topicMap = categoryMap.get(headerItem.fieldKey.category);
      if (topicMap === undefined) {
        topicMap = new Map();
        categoryMap.set(headerItem.fieldKey.category, topicMap);
      }

      let topicCount = topicMap.get(headerItem.fieldKey.topic); 
      if (topicCount === undefined) {
        topicCount = 0;
      }
      topicMap.set(headerItem.fieldKey.topic, topicCount + 1);
    });

    const categoryHeaders: SizedHeaderItem[] = [];
    const topicHeaders: SizedHeaderItem[] = [];
    categoryMap.forEach((topicMap, category) => {
      let categorySize = 0;
      topicMap.forEach((count, topic) => {
        const topicSize = this.calculateSize(count);
        categorySize += topicSize;
        const topicHeader = {
          label: createTopicDisplay(topic),
          fieldKey: makeFieldKey(topic, Category.INFO, 'dummy')
        };
        topicHeaders.push({ header: topicHeader, width: topicSize + 'px' });
      });

      categorySize += topicMap.size - 1; // For last border
      const categoryHeader = {
        label: createCategoryDisplay(category),
        fieldKey: makeFieldKey(Topic.RESOURCE, category, 'dummy')
      };
      categoryHeaders.push({ header: categoryHeader, width: categorySize + 'px' });
    });

    const fieldHeaders: SizedHeaderItem[] = headerItems.map(headerItem => {
      let width = sizing.columnWidth + 'px';
      const topicMap = categoryMap.get(headerItem.fieldKey.category);
      if (topicMap !== undefined) {
        if (topicMap.get(headerItem.fieldKey.topic) === 1) {
          width = MIN_GROUP_WIDTH + 'px' ;
        }
      }
      
      return { header: headerItem, width };
    });

    const style = {
      marginLeft: sizing.sidebarWidth + 5 + 'px',
    };

    return (
      <div className="fixed-table__body" style={style}>
        <FixedTableHeader
          setUpdateFixedHeaderLeft={setUpdateFixedHeaderLeft} 
          headerRows={[categoryHeaders, topicHeaders, fieldHeaders]} 
          sizing={sizing} 
          onColumnClick={onColumnClick}
          sort={sort}
        />
        <FixedTableList 
          setScrollContainer={setScrollContainer}
          headers={fieldHeaders} 
          fixed={false} 
          rows={rows} 
          sizing={sizing} 
        />
      </div>
    );
  }
}
