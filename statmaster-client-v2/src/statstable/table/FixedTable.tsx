import './FixedTable.css';

import * as React from 'react';

import { FixedTableBody } from './FixedTableBody';
import { FixedTableSidebar } from './FixedTableSidebar';
import PerfectScrollBar from 'perfect-scrollbar';
import ResourceContainer from '../../statsdata/ResourceContainer';
import { SizedHeaderItem } from './FixedTableHeaderRow';
import { Sort } from 'statmaster-common';
import { TableHeaderItem } from '../../table/types';

export interface TableData {
  sidebarHeaderItems: SizedHeaderItem[];
  sidebarRows: ResourceContainer[]; // TODO: make array of maps, and pass keys separately
  mainHeaderItems: TableHeaderItem[];
  mainRows: ResourceContainer[]; // TODO: make array of maps, and pass keys separately
}

export interface TableSizing {
  headerRowCount: number;
  sidebarWidth: number;
  columnWidth: number;
  headerHeight: number;
}

interface TableProps {
  data: TableData;
  sizing: TableSizing;
  onColumnClick: (header: TableHeaderItem) => void;
  sort: Sort;
}

export class FixedTable extends React.Component<TableProps> {
  private scrollContainer: HTMLElement;
  private scrollbar: PerfectScrollBar;
  private updateFixedListTop: (top: number) => void;
  private updateFixedHeaderLeft: (left: number) => void;

  setScrollContainer = (element: HTMLElement) => {
    console.log('Setting scroll container');
    this.scrollContainer = element;
  }

  componentWillUnmount() {
    this.scrollbar.destroy();
    // this.scrollbar = null;
  }

  componentDidUpdate() {
    this.scrollbar.update();
  }

  componentDidMount() {
    if (!this.scrollContainer) {
      return;
    }

    this.scrollbar = new PerfectScrollBar(this.scrollContainer);

    this.scrollContainer.addEventListener('ps-scroll-y', this.scrollFixedColumn);
    this.scrollContainer.addEventListener('ps-y-reach-start', event => 
      this.scrollFixedColumn({ target: { scrollTop: 0 } }));
    this.scrollContainer.addEventListener('ps-scroll-x', this.scrollFixedHeader);
    this.scrollContainer.addEventListener('ps-x-reach-start', event =>
      this.scrollFixedHeader({ target: { scrollLeft: 0 } }));
  }

  scrollFixedColumn = (event: any) => {
    if (this.updateFixedListTop === undefined) {
      return;
    }
    this.updateFixedListTop(-(event.target.scrollTop as number));
  }

  scrollFixedHeader = (event: any) => {
    if (this.updateFixedHeaderLeft === undefined) {
      return;
    }
    this.updateFixedHeaderLeft(-(event.target.scrollLeft as number));
  }

  setUpdateFixedListTop = (updateFixedListTop: (top: number) => void) => {
    this.updateFixedListTop = updateFixedListTop;
  }

  setUpdateFixedHeaderLeft = (updateFixedHeaderLeft: (left: number) => void) => {
    this.updateFixedHeaderLeft = updateFixedHeaderLeft;
  }

  render() {
    const { sidebarHeaderItems, sidebarRows, mainHeaderItems, mainRows, } = this.props.data;
    const { sizing, onColumnClick, sort } = this.props;

    return (
      <div className="fixed-table">
        <FixedTableSidebar 
          setUpdateFixedListTop={this.setUpdateFixedListTop}
          sizing={sizing} 
          headerItems={sidebarHeaderItems} 
          rows={sidebarRows} 
          onColumnClick={onColumnClick}
          sort={sort}
        />
        <FixedTableBody 
          setScrollContainer={this.setScrollContainer} 
          setUpdateFixedHeaderLeft={this.setUpdateFixedHeaderLeft} 
          headerItems={mainHeaderItems} 
          rows={mainRows} 
          sizing={sizing} 
          onColumnClick={onColumnClick}
          sort={sort}
        />
      </div>
    );
  }
}
