import * as React from 'react';

import { Game, Team } from 'statmaster-common';

interface Props {
  game: Game;
  teams: Map<string, Team>;
}

export class GameInfo extends React.Component<Props> {

  getAbbreviation(teamId: string) {
    const { teams } = this.props;
    const team = teams.get(teamId);
    return team && team.abbreviation || 'Unknown';
  }

  render() {
    const { game } = this.props;
    const homeAbbreviation = this.getAbbreviation(game.home.team);
    const awayAbbreviation = this.getAbbreviation(game.away.team);
    
    return (
      <div className="game">
        <p key={game.id}>{homeAbbreviation} vs. {awayAbbreviation}</p>
      </div>
    );
  }
}
