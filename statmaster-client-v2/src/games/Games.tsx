import * as React from 'react';

import { Game, Team } from 'statmaster-common';

import { GameInfo } from './Game';
import { RootState } from '../core/reducer';
import { connect } from 'react-redux';

interface StateProps {
  games: Game[];
  teams: Map<string, Team>;
}

export class GamesContainer extends React.Component<StateProps & null> {

  render() {
    const { games, teams } = this.props;
    return (
      <div className="games">
        <h2>Games</h2>
        {
          games.map(g => (
            <GameInfo game={g} teams={teams} />
          ))
        }
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return {
    games: state.dataState.games,
    teams: state.dataState.teams
  };
}

export const Games = connect(mapStateToProps, null)(GamesContainer);
