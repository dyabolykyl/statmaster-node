/// <reference types="node" />

import 'font-awesome/css/font-awesome.min.css';
import './index.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { history, store }  from './core/store';

import App from './app/App';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history} >
      < App />
    </ConnectedRouter >
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
