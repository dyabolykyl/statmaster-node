import { ColumnAction } from '../tablecontrols/columns/columns';
import { DataAction } from '../statsdata/actions';
import { FilterAction } from '../tablecontrols/filtering/reducer';
import { UpdateSortAction } from '../tablecontrols/sorting/sorting';

export enum ActionTypeKey {
  'UPDATE_STATS',
  'UPDATE_SORT',
  'UPDATE_TEAMS',
  'UPDATE_STATGROUPS',
  'UPDATE_FIELDS',
  'UPDATE_MATCHUPS',
  'UPDATE_GAMES',

  'UPDATE_FILTER',
  'REMOVE_FILTER',
  'REMOVE_FILTERS_FOR_FIELD',
  'ADD_FILTER',

  'ADD_COLUMN',
  'REMOVE_COLUMN',
  'SET_COLUMNS',
  'TOGGLE_COLUMN_GROUP',

  'ADD_SELECTOR',
  'REMOVE_SELECTOR',
  'SET_SELECTORS',
  
  'USER',
  'UPDATE_USER',
  'LOGOUT_USER',
  'UPDATE_DATE',

  'FETCH_STATS_IN_PROGRESS',
  'DATA_LOAD_INITIAL_STATE'
}

export enum ActionStatus {
  'IN_PROGRESS',
  'SUCCEEDED',
  'FAILED',
  'NO_STATE'
}

export interface Action {
  type: ActionTypeKey;
  state?: ActionStatus;
  // payload?: T;
}

export type Actions = FilterAction | DataAction | UpdateSortAction | ColumnAction;
