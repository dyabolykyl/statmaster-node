import { applyMiddleware, compose, createStore } from 'redux';

import createHistory from 'history/createBrowserHistory'
import { createLogger } from 'redux-logger';
import reducer from './reducer';
import { routerMiddleware } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk';

export const history = createHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(
    createLogger(),
    thunkMiddleware,
    routerMiddleware(history))
));
