import { DataState, dataStateReducer } from '../statsdata/reducer';
import { RouterState, routerReducer } from 'react-router-redux';
import { SelectorState, makeSetSelectorsAction, selectorReducer } from '../selectors/reducer';
import { SessionState, sessionReducer } from '../session/reducer';
import { TableState, tableStateReducer } from '../tablecontrols/tableControlsReducer';
import { getSelectionLists, getSelectors } from '../selectors/api';

import { Dispatch } from 'react-redux';
import { combineReducers } from 'redux';
import { fetchData } from '../statsdata/fetch';
import getLogger from '../util/log';

const log = getLogger('core.reducer');

export interface RootState {
  dataState: DataState;
  tableState: TableState;
  selectorState: SelectorState;
  sessionState: SessionState;
  routerState: RouterState;
}

export default combineReducers<RootState>({
  dataState: dataStateReducer,
  tableState: tableStateReducer,
  selectorState: selectorReducer,
  sessionState: sessionReducer,
  routerState: routerReducer
});

export const setInitialState = (dispatch: Dispatch<any>): void => {
  log.debug(`Setting initial state`);
  dispatch(fetchData());
  dispatch(makeSetSelectorsAction(getSelectors(), getSelectionLists()));
};
