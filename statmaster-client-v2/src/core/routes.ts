/**
 * Used to navigate app
 */
export const SELECTORS_PATH = '/selectors';
export const GAMES_PATH = '/games';
export const STATS_PATH = '/stats';
export const PICKS_PATH = '/picks';
export const PROFILE_PATH = '/profile';
export const LOGIN_PATH = '/login';
export const LOGOUT_PATH = '/logout';
export const ADMIN_PATH = '/admin';
