import * as log4javscript from 'log4javascript';

const logMap = {};

function getLogger(name: string): log4javscript.Logger {
  if (name in logMap) {
    return logMap[name];
  }

  const log = log4javscript.getLogger(name);

  const browserAppender = new log4javscript.BrowserConsoleAppender();
  browserAppender.setThreshold(log4javscript.Level.TRACE);
  const layout = new log4javscript.PatternLayout('%d %5p %c - %m');
  browserAppender.setLayout(layout);

  // no logging in tests
  let TEST_LOGGING = false; // TODO: make this an env variable
  if (process.env.NODE_ENV !== 'test' || TEST_LOGGING) {
    log.setLevel(log4javscript.Level.DEBUG);
    log.addAppender(browserAppender);
    log.trace(`Created new logger`);
  }

  logMap[name] = log;
  return log;

}

export default getLogger;
