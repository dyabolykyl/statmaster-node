import { LocalDate, LocalDateTime } from 'js-joda';

import { toMoment } from 'statmaster-common';

export const EST_TIMEZONE: string = 'America/New_York';

export function getCurrentDateTime(): LocalDateTime {
  return LocalDateTime.now();
}

export function displayDateTime(datetime: LocalDateTime): string {
  return toMoment(datetime).format('MMMM Do YYYY, h:mm:ss a'); // December 25th 2017, 11:25:50 pm;
}

export function displayDate(date: LocalDate): string {
  return toMoment(date).format('MMMM Do YYYY'); // December 25th 2017
}
