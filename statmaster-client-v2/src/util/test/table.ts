import * as tdf from '../testDataFactory';

import {
  Category,
  Field,
  FieldId,
  FieldKey,
  FieldValue,
  Sort,
  SortStyle,
  Topic,
  makeFieldKey
} from 'statmaster-common';
import { TableRowData, TableRowItem } from '../../table/types';

import { TableHeaderItem } from '../../table';

export function tableHeaderItemFromField(field: Field): TableHeaderItem {
  let fieldKey: FieldKey = tdf.arrangeFieldKey(field);

  return {
    fieldKey,
    label: field.label,
    description: field.description
  };
}
export function arrangeTableHeaderItem(fieldId?: string): TableHeaderItem {
  let field: Field = tdf.arrangeField(fieldId);
  return tableHeaderItemFromField(field);
}

export const tableRowData = (flatKey: string, fieldValue: FieldValue): TableRowData => {
  let value = fieldValue;
  let key = flatKey;
  
  return {
    key,
    value
  };
};

export const tableRowItem = (key: string, ...cells: TableRowData[]): TableRowItem => {
  let rowDataMap = new Map();
  cells.forEach(cell => rowDataMap.set(cell.key, cell));
  return {
    key,
    data: rowDataMap
  };
};

export const makeSort = (fieldId: FieldId, style: SortStyle, topic?: Topic, category?: Category): Sort => {
  return {
    columnKey: makeFieldKey(topic || Topic.RESOURCE, category || Category.INFO, fieldId),
    style,
  };
};
