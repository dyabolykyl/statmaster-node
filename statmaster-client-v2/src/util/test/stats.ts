import {
  Category,
  Field,
  FieldId,
  FieldKey,
  Matchup,
  ResourceId,
  Stat,
  StatGroup,
  Team,
  Topic
} from 'statmaster-common';

import { getNextIdentifier } from '../testDataFactory';
import { makeFieldKey } from 'statmaster-common';

export const arrangeTeam = (id: string): Team => {
  return {
    id: 'team' + id,
    name: 'name' + id,
    city: 'city' + id,
    abbreviation: 'TM' + id
  };
};

export const arrangeTeams = (...ids: string[]): Map<ResourceId, Team> => {
  let teams = ids.map(arrangeTeam);
  let teamsMap = new Map();
  teams.forEach(team => teamsMap.set(team.id, team));
  return teamsMap;
};

export const arrangeMatchup = (resource?: ResourceId, opponent?: ResourceId, id?: ResourceId): Matchup => {
  let num = getNextIdentifier();
  return {
    resource: resource || 'resource' + num,
    opponent: opponent || 'opponent' + num,
  };
};

export const arrangeStat = (key: FieldKey, value: number): Stat => {
  return {key, value};
};

export const arrangeStats = (fields: Field[], values: number[]): Map<string, Stat> => {
  let stats = new Map();
  for (let i = 0; i < fields.length; i++) {
    let stat = arrangeStat(makeFieldKey(Topic.RESOURCE, fields[i].category, fields[i].id), values[i]);
    stats.set(stat.key.flatKey, stat);
  }
  return stats;
};

export const randomStatGroup = (resourceId?: ResourceId): StatGroup => {
  let num = getNextIdentifier();
  return arrangeStatGroup(
    resourceId || 'resource' + num, 
    'id' + num, 
    arrangeStat(arrangeFieldKey(arrangeField('f' + getNextIdentifier())), getNextIdentifier()),
    arrangeStat(arrangeFieldKey(arrangeField('f' + getNextIdentifier())), getNextIdentifier()));
};

export const arrangeStatGroup = (resourceId: ResourceId, id?: ResourceId, ...stats: Stat[]): StatGroup => {
  let statsMap: Map<string, Stat> = new Map();
  stats.forEach(stat => statsMap.set(stat.key.flatKey, stat));
  return {
    id: id || 'id' + getNextIdentifier(),
    resource: resourceId,
    stats: statsMap
  };
};

export const arrangeFieldKey = (field?: Field): FieldKey => {
  field = field || arrangeField();
  return makeFieldKey(
    Topic.RESOURCE, 
    field.category,
    field.id
  );
};

export const arrangeField = (fieldId?: string, category?: Category): Field => {
  let id = fieldId ? fieldId : getNextIdentifier() + '';
  return {
    id: id,
    category: category || Category.INFO,
    label: 'display' + id,
    description: 'description' + id
  };
};

export const arrangeFields = (...fields: Field[]): Map<FieldId, Field> => {
  let fieldMap = new Map();
  fields.forEach(field => fieldMap.set(field.id, field));
  return fieldMap;
};

// export const mockFields = Array.from(INFO_FIELDS.values());
// mockFields.push(BATTING_FIELDS.get('hr'));
// mockFields.push(BATTING_FIELDS.get('so'));
// mockFields.push(BATTING_FIELDS.get()) 