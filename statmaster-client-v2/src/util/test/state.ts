import { Field, FlatFilterTree, Matchup, ResourceId, Sort, StatGroup, Team } from 'statmaster-common';

import { DEFAULT_SORT } from '../../tablecontrols/sorting/sorting';
import { DataState } from '../../statsdata/reducer';
import { LocalDateTime } from 'js-joda';
import { RootState } from '../../core/reducer';
import { SelectorState } from '../../selectors/reducer';
import { SessionState } from '../../session/reducer';
import { TableState } from '../../tablecontrols/tableControlsReducer';
import { initialColumnState } from '../../tablecontrols/columns/columns';

export const arrangeTableState = (filterNotPlaying?: boolean, sort?: Sort): TableState => {
  return {
    columnState: initialColumnState,
    filterState: { filterTree: new FlatFilterTree() },
    sortState: { sort: sort || DEFAULT_SORT }
  };
};

export const arrangeDataState = (teams?: Map<ResourceId, Team>,
                                 matchups?: Map<ResourceId, Matchup>,
                                 statGroups?: Map<ResourceId, StatGroup>,
                                 fields?: Map<string, Field>): DataState => {
  return {
    teams: teams || new Map(),
    matchups: matchups || new Map(),
    statGroups: statGroups || new Map(),
    fields: fields || new Map(),
    games: [],
  };
};

export const arrangeSessionState = (): SessionState => {
  return {
    datetime: LocalDateTime.parse('2017-12-11T14:22:48.561')
  };
};

export const arrangeSelectorState = (): SelectorState => {
  return {
    selectors: new Map(),
    selections: new Map()
  };
};

export const arrangeRootState = (tableState?: TableState, dataState?: DataState): RootState => {
  return {
    tableState: tableState || arrangeTableState(),
    dataState: dataState || arrangeDataState(),
    selectorState: arrangeSelectorState(),
    sessionState: arrangeSessionState(),
    routerState: { location: null }
  };
};
