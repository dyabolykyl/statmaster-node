export * from './test/stats';
export * from './test/state';
export * from './test/table';

let nextIdentifier = 0;
export const getNextIdentifier = () => nextIdentifier++;
