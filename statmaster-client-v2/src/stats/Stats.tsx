import * as React from 'react';

import { LocalDateTime } from 'js-joda';
import { RootState } from '../core/reducer';
import { StatsTable } from '../statstable/StatsTable';
import { TableControls } from '../tablecontrols/TableControls';
import { connect } from 'react-redux';
import { displayDateTime } from '../util/date';
import { getDateTime } from '../session/selectors';

interface StateProps {
  datetime: LocalDateTime;
}

class Stats extends React.Component<StateProps & null> {
  static defaultProps: StateProps = {
    datetime: LocalDateTime.now()
  };

  render() {
    return (
      <div className="stats">
        <p className="date-header">Showing stats for {displayDateTime(this.props.datetime)} </p>
        < TableControls />
        < StatsTable />
      </div>
    );
  }
}

function mapStateToProps(state: RootState): StateProps {
  return { datetime: getDateTime(state) };
}

// tslint:disable no-any
export default connect(mapStateToProps, null)(Stats);
