import './Main.css';

import * as React from 'react';

import { ADMIN_PATH, LOGIN_PATH, STATS_PATH } from '../core/routes';
import { Redirect, Route, Switch } from 'react-router-dom';

import { AdminOptions } from '../admin/AdminOptions';
import { LoginMain } from '../session/LoginMain';
import { StatsTableMain } from '../statstable/StatsTableMain';

export class Main extends React.Component {

  render() {
    return (
      
      <Switch>
        <Route path={STATS_PATH} component={StatsTableMain} />
        <Route path={ADMIN_PATH} component={AdminOptions} />
        {/* {/* <Route path={GAMES_PATH} component={Games} />
        <Route path={SELECTORS_PATH} component={Selectors} /> */}
        <Route path={LOGIN_PATH} component={LoginMain} />
    {/* <Route path={PROFILE_PATH} component={Profile} /> */} */}
        <Route exact={true} path="/" >
          <Redirect to={STATS_PATH} />
        </Route> 
        <Redirect to="/" />
      </Switch >

      // <div className="main" >
      //   <Switch >
      //     <Route path={STATS_PATH} component={Stats} />
      //     <Route path={GAMES_PATH} component={Games} />
      //     <Route path={SELECTORS_PATH} component={Selectors} />
      //     <Route path={LOGIN_PATH} component={Login} />
      //     <Route path={PROFILE_PATH} component={Profile} />
      //     <Route exact={true} path="/" >
      //       <Redirect to={STATS_PATH} />
      //     </Route>
      //     <Redirect to="/" />
      //   </Switch >
      // </div>
    );
  }
}
