import { DateTimeFormatter, LocalDate } from 'js-joda';
import {
  Field,
  Game,
  Matchup,
  StatGroup,
  StatGroupResponse,
  Team,
  statGroupResponseToStatGroup
} from 'statmaster-common';

import { Response } from '../service/response';
import { Service } from '../service/service';

export class StatsService {

  private service: Service = new Service();

  async fetchTeams(): Promise<Team[]> {
    const res = await this.get('/teams');
    return res.data;
  }

  async fetchStatGroups(resources: string[]): Promise<StatGroup[]> {
    // Use POST to allow passing request data in body
    const res = await this.post(`/stats/query`, { resources, type: 'team' });
    const statGroups: StatGroupResponse[] = res.data;
    return statGroups.map(statGroupResponseToStatGroup);
  }

  async fetchMatchupStatGroups(matchups: Matchup[]): Promise<StatGroup[]> {
    // Use POST to allow passing request data in body
    const res = await this.post(`/stats/query/matchups`, { matchups });
    const statGroups: StatGroupResponse[] = res.data;
    return statGroups.map(statGroupResponseToStatGroup);
  }

  async fetchFields(): Promise<Field[]> {
    const res = await this.get('/fields');
    return res.data;
  }

  async fetchGames(date: LocalDate): Promise<Game[]> {
    const res = await this.get(`/games?date=${DateTimeFormatter.ofPattern('yyyy-MM-dd').format(date)}`);
    return res.data;
  }

  private get = (path: string, data?: any): Promise<Response<any>> => {
    return this.service.get(`/api/v1${path}`, data);
  }

  private post = (path: string, data?: any): Promise<Response<any>> => {
    return this.service.post(`/api/v1${path}`, data);
  }

}
