import { ActionStatus, ActionTypeKey } from '../core/actions';
import {
  DataAction,
  UpdateFieldsAction,
  UpdateGamesAction,
  UpdateMatchupsAction,
  UpdateStatsAction,
  UpdateTeamsAction
} from './actions';
import { Field, Game, Matchup, ResourceId, StatGroup, Team } from 'statmaster-common';

export interface DataState {
  teams: Map<ResourceId, Team>;
  matchups: Map<ResourceId, Matchup>;
  statGroups: Map<ResourceId, StatGroup>;
  fields: Map<string, Field>;
  games: Game[];
}

export const initialState: DataState = {
  teams: new Map(),
  matchups: new Map(),
  statGroups: new Map(),
  fields: new Map(),
  games: [],
};

export function dataStateReducer(currentState: DataState = initialState, action: DataAction) {
  switch (action.type) {
    case ActionTypeKey.UPDATE_TEAMS:
      if (action.state === ActionStatus.SUCCEEDED) {
        return updateTeams(currentState, action);
      }
      return currentState;
    case ActionTypeKey.UPDATE_MATCHUPS:
      if (action.state === ActionStatus.SUCCEEDED) {
        return updateMatchups(currentState, action);
      }
      return currentState;
    case ActionTypeKey.UPDATE_STATGROUPS:
      if (action.state === ActionStatus.SUCCEEDED) {
        return updateStatGroups(currentState, action);
      }
      return currentState;
    case ActionTypeKey.UPDATE_FIELDS:
      if (action.state === ActionStatus.SUCCEEDED) {
        return updateFields(currentState, action);
      }
      return currentState;
    case ActionTypeKey.UPDATE_GAMES:
      if (action.state === ActionStatus.SUCCEEDED) {
        return updateGames(currentState, action);
      }
      return currentState;
    default:
      return currentState;
  }
}

function updateTeams(state: DataState, action: UpdateTeamsAction): DataState {
  if (action.teams) {
    let teams = new Map<ResourceId, Team>();
    action.teams.forEach(team => teams.set(team.id, team));
    return Object.assign({}, state, { teams });
  }
  return state;
}

function updateStatGroups(state: DataState, action: UpdateStatsAction): DataState {
  if (action.statGroups) {
    let statGroups = new Map<ResourceId, StatGroup>(state.statGroups);
    action.statGroups.forEach(statGroup => statGroups.set(statGroup.resource, statGroup));
    return Object.assign({}, state, { statGroups });
  }
  return state;
}

function updateFields(state: DataState, action: UpdateFieldsAction): DataState {
  if (action.fields) {
    let fields = new Map<ResourceId, Field>();
    action.fields.forEach(field => fields.set(field.id, field));
    return Object.assign({}, state, { fields });
  }
  return state;
}

function updateMatchups(state: DataState, action: UpdateMatchupsAction): DataState {
  if (action.matchups) {
    let matchups = new Map<ResourceId, Matchup>();
    action.matchups.forEach(matchup => matchups.set(matchup.resource, matchup));
    return Object.assign({}, state, { matchups });
  }
  return state;
}

function updateGames(state: DataState, action: UpdateGamesAction): DataState {
  if (action.games) {
    return Object.assign({}, state, { games: action.games });
  }
  return state;
}