import { Action, ActionStatus, ActionTypeKey } from '../core/actions';
import { Field, Game, Matchup, StatGroup, Team } from 'statmaster-common';

export type DataAction = UpdateTeamsAction | UpdateStatsAction
  | UpdateFieldsAction | UpdateMatchupsAction | UpdateGamesAction;

export interface UpdateTeamsAction extends Action {
  teams?: Array<Team>;
}

export function makeUpdateTeamsInProgressAction(): UpdateTeamsAction {
  return {
    type: ActionTypeKey.UPDATE_TEAMS,
    state: ActionStatus.IN_PROGRESS
  };
}

export function makeUpdateTeamsSucceededAction(teams: Array<Team>): UpdateTeamsAction {
  return {
    type: ActionTypeKey.UPDATE_TEAMS,
    state: ActionStatus.SUCCEEDED,
    teams
  };
}

export interface UpdateStatsAction extends Action {
  statGroups?: Array<StatGroup>;
}

export function makeUpdateStatsInProgressAction(): UpdateStatsAction {
  return {
    type: ActionTypeKey.UPDATE_STATGROUPS,
    state: ActionStatus.IN_PROGRESS
  };
}

export function makeUpdateStatsSucceededAction(statGroups: Array<StatGroup>): UpdateStatsAction {
  return {
    type: ActionTypeKey.UPDATE_STATGROUPS,
    state: ActionStatus.SUCCEEDED,
    statGroups
  };
}

export interface UpdateFieldsAction extends Action {
  fields?: Array<Field>;
}

export function makeUpdateFieldsInProgressAction(): UpdateFieldsAction {
  return {
    type: ActionTypeKey.UPDATE_FIELDS,
    state: ActionStatus.IN_PROGRESS
  };
}

export function makeUpdateFieldsSucceededAction(fields: Array<Field>): UpdateFieldsAction {
  return {
    type: ActionTypeKey.UPDATE_FIELDS,
    state: ActionStatus.SUCCEEDED,
    fields
  };
}

export interface UpdateMatchupsAction extends Action {
  matchups?: Array<Matchup>;
}

export function makeUpdateMatchupsInProgressAction(): UpdateMatchupsAction {
  return {
    type: ActionTypeKey.UPDATE_MATCHUPS,
    state: ActionStatus.IN_PROGRESS
  };
}

export function makeUpdateMatchupsSucceededAction(matchups: Array<Matchup>): UpdateMatchupsAction {
  return {
    type: ActionTypeKey.UPDATE_MATCHUPS,
    state: ActionStatus.SUCCEEDED,
    matchups
  };
}

export interface UpdateGamesAction extends Action {
  games?: Game[];
}

export function makeUpdateGamesInProgressAction(): UpdateGamesAction {
  return {
    type: ActionTypeKey.UPDATE_GAMES,
    state: ActionStatus.IN_PROGRESS
  };
}

export function makeUpdateGamepsSucceededAction(games: Game[]): UpdateGamesAction {
  return {
    type: ActionTypeKey.UPDATE_GAMES,
    state: ActionStatus.SUCCEEDED,
    games
  };
}
