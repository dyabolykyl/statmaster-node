import * as tdf from '../util/testDataFactory';

import { Action, ActionStatus, ActionTypeKey } from '../core/actions';
import {
   DataAction,
   makeUpdateMatchupsSucceededAction,
   makeUpdateStatsSucceededAction,
   makeUpdateTeamsSucceededAction
} from './actions';
import { Field, Matchup, StatGroup, Team } from 'statmaster-common';
import { dataStateReducer, initialState } from './reducer';

import { makeUpdateFieldsSucceededAction } from './actions';

test('initial state', () => {
  expect(initialState.teams).toEqual(new Map());
  expect(initialState.matchups).toEqual(new Map());
  expect(initialState.statGroups).toEqual(new Map());
});

test('unknown action returns intial state', () => {
  let dummyAction: Action = {
    type: ActionTypeKey.FETCH_STATS_IN_PROGRESS,
    state: ActionStatus.FAILED,
  };

  let state = dataStateReducer(undefined, dummyAction as DataAction);
  expect(state).toEqual(initialState);
});

test('update teams', () => {
  let team1: Team = tdf.arrangeTeam('1');
  let team2: Team = tdf.arrangeTeam('2');
  let action = makeUpdateTeamsSucceededAction([team1, team2]);

  let state = dataStateReducer(undefined, action);
  let teams = state.teams;
  expect(teams.size).toBe(2);
  expect(teams.get(team1.id)).toEqual(team1);
  expect(teams.get(team2.id)).toEqual(team2);
});

test('update matchups', () => {
  let matchup1: Matchup = tdf.arrangeMatchup();
  let matchup2: Matchup = tdf.arrangeMatchup();
  let action = makeUpdateMatchupsSucceededAction([matchup1, matchup2]);

  let state = dataStateReducer(undefined, action);
  let matchups: Map<string, Matchup> = state.matchups;
  expect(matchups.size).toBe(2);
  expect(matchups.get(matchup1.resource)).toEqual(matchup1);
  expect(matchups.get(matchup2.resource)).toEqual(matchup2);
});

test('update stats', () => {
  let statGroup1: StatGroup = tdf.randomStatGroup();
  let statGroup2: StatGroup = tdf.randomStatGroup();
  let action = makeUpdateStatsSucceededAction([statGroup1, statGroup2]);

  let state = dataStateReducer(undefined, action);
  let statGroups: Map<string, StatGroup> = state.statGroups;
  expect(statGroups.size).toBe(2);
  expect(statGroups.get(statGroup1.resource)).toEqual(statGroup1);
  expect(statGroups.get(statGroup2.resource)).toEqual(statGroup2);
});

test('update fields', () => {
  let field1: Field = tdf.arrangeField();
  let field2: Field = tdf.arrangeField();
  let action = makeUpdateFieldsSucceededAction([field1, field2]);

  let state = dataStateReducer(undefined, action);
  let fields: Map<string, Field> = state.fields;
  expect(fields.size).toBe(2);
  expect(fields.get(field1.id)).toEqual(field1);
  expect(fields.get(field1.id)).toEqual(field1);
});
