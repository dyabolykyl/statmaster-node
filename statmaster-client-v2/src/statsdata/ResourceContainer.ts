import {
  Category,
  PLAYING_KEY,
  ResourceDataValue,
  ResourceId,
  StatGroup,
  Team,
  Topic,
  makeFieldKey
} from 'statmaster-common';

/**
 * Holds arbitrary data about a resource in a flat map
 * Could be stats, info, metadata, etc.
 */
export default class ResourceContainer {
  readonly resourceId: ResourceId;
  readonly opponentId: ResourceId;
  readonly isPlaying: boolean;
  readonly data: Map<string, ResourceDataValue>;
  
  constructor(team: Team, opponent?: Team) {
    this.resourceId = team.id;
    this.data = new Map();
    this.addTeamData(team, Topic.RESOURCE);

    if (opponent !== undefined) {
      this.opponentId = opponent.id;
      this.addTeamData(opponent, Topic.OPPOSING);
      this.isPlaying = true;
    } else {
      this.isPlaying = false;
    }

    this.data.set(PLAYING_KEY.flatKey, { key: PLAYING_KEY, value: this.isPlaying} );
  }

  addStatGroupData(statGroup: StatGroup | undefined, topic: Topic) {
    if (statGroup === undefined) {
      return;
    }

    // TODO: Decide if such checks are worth it
    if (topic === Topic.RESOURCE) {
      if (statGroup.resource !== this.resourceId) {
        return;
      }
    }

    statGroup.stats.forEach((stat, statKey, statsMap) => {
      let fieldKey = makeFieldKey(topic, stat.key.category, stat.key.fieldId);
      let value: ResourceDataValue = { key: fieldKey, value: stat.value };
      this.data.set(fieldKey.flatKey, value);
    });
  }

  private addTeamData(team: Team | undefined, topic: Topic) {
    if (team === undefined) {
      return;
    }

    Object.keys(team).forEach(key => {
      let fieldKey = makeFieldKey(topic, Category.INFO, key);
      this.data.set(fieldKey.flatKey, { key: fieldKey, value: team[key] });
    });
  }
}
