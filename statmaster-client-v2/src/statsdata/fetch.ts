import { Game, Matchup, makeMatchup } from 'statmaster-common';
import {
  makeUpdateFieldsInProgressAction,
  makeUpdateFieldsSucceededAction,
  makeUpdateMatchupsInProgressAction,
  makeUpdateMatchupsSucceededAction,
  makeUpdateStatsInProgressAction,
  makeUpdateStatsSucceededAction,
  makeUpdateTeamsInProgressAction,
  makeUpdateTeamsSucceededAction
} from './actions';
import { makeUpdateGamepsSucceededAction, makeUpdateGamesInProgressAction } from './actions';

import { Dispatch } from 'redux';
import { RootState } from '../core/reducer';
import { StatsService } from './StatsService';
import getLogger from '../util/log';

const log = getLogger('stats.reducer');
const statsService = new StatsService();

export function fetchData() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Fetching data`);
    try {
      dispatch(fetchFields());
      dispatch(fetchGamesAndMatchups());
      await dispatch(fetchTeams());
      dispatch(fetchStatGroupsForTeams());
    } catch (err) {
      log.error(`Error while fetching data: ${err}`);
    }
  };
}

export function fetchGamesAndMatchups() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Refetching games and matchup stats`);
    try {
      await dispatch(fetchGames());
      return dispatch(fetchMatchups());
    } catch (err) {
      log.error(`Error while fetching data: ${err}`);
    }
  };
}

export function fetchTeams() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Beginning fetchTeams`);
    dispatch(makeUpdateTeamsInProgressAction());
    const teams = await statsService.fetchTeams();
    log.debug(`Teams fetched`);
    dispatch(makeUpdateTeamsSucceededAction(teams));
    // catch (e): dispatch failed action
  };
}

export function fetchStatGroupsForTeams() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Fetching statgroups for teams`);
    try {
      const teamIds = Array.from(getState().dataState.teams.keys());
      await dispatch(fetchStatGroups(teamIds));
    } catch (err) {
      log.error(`Error while fetching stat groups: ${err}`);
      // dispatch failed action
    }
  };
}

export function fetchStatGroups(resources: string[]) {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Fetching statgroups for ${resources}`);
    dispatch(makeUpdateStatsInProgressAction());
    const statGroups = await statsService.fetchStatGroups(resources);
    log.debug(`Statgroups fetched for ${resources}`);
    dispatch(makeUpdateStatsSucceededAction(statGroups));
  };
}

export function fetchFields() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    try {
      log.debug(`Beginning fetchFields`);
      dispatch(makeUpdateFieldsInProgressAction());
      const fields = await statsService.fetchFields();
      log.debug(`Fields fetched`);
      dispatch(makeUpdateFieldsSucceededAction(fields));
    } catch (err) {
      log.error(`Error while fetching fields: ${err}`);
      // dispatch failed to fetch fields action
    }
  };
}

export function fetchGames() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    try {
      log.debug(`Beginning fetchGames`);
      dispatch(makeUpdateGamesInProgressAction());

      const datetime = getState().sessionState.datetime;
      const games: Game[] = await statsService.fetchGames(datetime.toLocalDate());
      log.debug(`Games fetched: ${JSON.stringify(games)}`);

      dispatch(makeUpdateGamepsSucceededAction(games));
    } catch (err) {
      log.error(`Error while fetching games: ${err}`);
      // dispatch failed to fetch games action
    }
  };
}

export function fetchMatchups() {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    try {
      log.debug(`Beginning fetchMatchups`);
      dispatch(makeUpdateMatchupsInProgressAction());

      const matchups: Matchup[] = [];
      getState().dataState.games.forEach(game => {
        matchups.push(makeMatchup(game.home.team, game.away.team));
        matchups.push(makeMatchup(game.away.team, game.home.team));
      });

      dispatch(makeUpdateMatchupsSucceededAction(matchups));

      if (matchups.length > 0) {
        log.debug('Fetching statgroups for matchups');
        await dispatch(fetchMatchupStatGroups(matchups));
      } else {
        log.debug(`No matchups found`);
      }

    } catch (err) {
      log.error(`Error while fetching matchups: ${err}. ${err.response.data}`);
      // dispatch failed to fetch games action
    }
  };
}

export function fetchMatchupStatGroups(matchups: Matchup[]) {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    log.debug(`Fetching statgroups for ${matchups}`);
    dispatch(makeUpdateStatsInProgressAction());
    const statGroups = await statsService.fetchMatchupStatGroups(matchups);
    log.debug(`Statgroups fetched for ${matchups}`);
    dispatch(makeUpdateStatsSucceededAction(statGroups));
  };
}
