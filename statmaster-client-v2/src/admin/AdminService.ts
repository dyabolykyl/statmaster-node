import { Response, ResponseStatus } from '../service/response';
import { StatGroup, User } from 'statmaster-common';

import { LocalDate } from 'js-joda';
import { Service } from '../service/service';

const service = new Service();

export function isUserAuthorized(user: User): Promise<Response<any>> {
  return service.get(`/api/v1/users/${user.id}/admin`);
}

export async function updateTeams(): Promise<Response<any>> {
  return service.post('/api/v1/teams/update');
}

export async function updateStats(date: LocalDate): Promise<Response<{}>> {
  return service.post(`/api/v1/stats/team/${service.formatDate(date)}`);
}

export async function getTeamStats(date: LocalDate): Promise<Response<StatGroup[]>> {
  const query = { date: service.formatDate(date), type: 'team' }; // TODO: replace 'team' with enum
  return service.post(`/api/v1/stats/query`, query);
}

export async function deleteStats(date: LocalDate): Promise<Response<void>> {

  await new Promise(resolve => setTimeout(resolve, 1000));

  return {
    status: ResponseStatus.SUCCESS
  };
}

export async function deleteAllStats(): Promise<Response<void>> {

  await new Promise(resolve => setTimeout(resolve, 1000));

  return {
    status: ResponseStatus.SUCCESS
  };
}