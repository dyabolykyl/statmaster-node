import 'react-datepicker/dist/react-datepicker.css';
import './AdminOptions.css';

import * as React from 'react';
import * as adminService from './AdminService';

import { Dispatch, connect } from 'react-redux';
import { LocalDate, LocalDateTime, LocalTime } from 'js-joda';
import { Response, ResponseStatus } from '../service/response';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { StatGroup, User, momentToJoda, toMoment } from 'statmaster-common';

import DatePicker from 'react-datepicker';
import { Moment } from 'moment-timezone';
import { RootState } from '../core/reducer';
import getLogger from '../util/log';
import { updateDatetime } from '../session/actions';

const log = getLogger('admin');

// TODO: use in rest of app
enum RequestState {
  NO_REQUEST,
  PENDING,
  SUCCEEDED,
  FAILED,
  UNAUTHORIZED
}

// All state is contained within the component and not redux because this is admin-sensitive data
interface State {
  date: LocalDate;
  type: string;
  sport: string;
  currentDateStats: StatGroup[] | undefined;
  currentRequestState: RequestState;
}

// except user info
interface StateProps {
  user?: User;
  actualDate: LocalDateTime;
}

interface DispatchProps {
  changeDate: (date: LocalDateTime) => void;
}

type Props = RouteComponentProps<{}> & StateProps & DispatchProps;

export class AdminOptionsComponent extends React.Component<Props, State> {
  state: State = {
    date: LocalDate.now(),
    type: 'team',
    sport: 'mlb',
    currentDateStats: undefined,
    currentRequestState: RequestState.NO_REQUEST
  };

  async componentDidMount() {
    // ask the server if the user is authorized first
    const authorized = await this.determineAuthorization();
    if (!authorized) {
      log.warn('User is not authorized');
      return;
    }

    this.setState({ date: this.props.actualDate.toLocalDate() });
    return await this.getStats(this.props.actualDate.toLocalDate());
  }

  renderRequestStatus() {
    let className, text;
    switch (this.state.currentRequestState) {
      case RequestState.SUCCEEDED:
        className = 'fa fa-check-circle green';
        text = 'Success!';
        break;
      case RequestState.PENDING:
        className = 'fa fa-spinner fa-spin yellow';
        text = 'Pending';
        break;
      case RequestState.FAILED:
        className = 'fa fa-times-circle red';
        text = 'Failed!';
        break;
      case RequestState.UNAUTHORIZED:
        className = 'fa fa-universal-access red';
        text = 'Unauthorized';
        break;
      default:
        className = 'fa';
        text = 'No Request';
        break;
    }

    return (
      <p>
        <i className={className} /><span>{text}</span>
      </p>
    );
  }

  render() {
    const disabled = this.state.currentRequestState === RequestState.PENDING;

    return (
      <main id="admin-options-main">
        <h2>Admin Options</h2>
        <div id="admin-options">
          <div id="admin-info">
            <p>Stats {this.state.currentDateStats === undefined ? 'Missing' : 'Found'}</p>
            <p>{(this.state.currentDateStats || []).length / 2} Games</p>
            {this.renderRequestStatus()}
          </div>
          <div id="admin-selections">
            <DatePicker
              dateFormat="MM/DD/YYYY"
              showYearDropdown={true}
              scrollableYearDropdown={true}
              selected={toMoment(this.state.date)}
              onChange={this.onDateChanged}
            />
            <select name="stat-type-select" onChange={event => this.setState({ type: event.target.value })}>
              <option value="team">Team</option>
              <option value="player">Player</option>
            </select>
            <select name="sport-select" onChange={event => this.setState({ sport: event.target.value })}>
              <option value="mlb">MLB</option>
            </select>
          </div>
          <div id="admin-buttons">
            <button disabled={disabled} onClick={this.updateStats}>Update Stats</button>
            <button disabled={disabled} onClick={this.deleteStats}>Delete Stats</button>
            {/* <button disabled={disabled} onClick={this.deleteAllStats}>Delete All Stats</button> */}
            <button disabled={disabled} onClick={this.updateTeams}>Update Teams</button>
            <button disabled={disabled} onClick={this.updateDatetime}>Update Date</button>
          </div>
        </div>
      </main>
    );
  }

  private onDateChanged = (datetime: Moment) => {
    const date = momentToJoda(datetime as Moment).toLocalDate();
    this.setState({ date });
    this.getStats(date);
  }

  private setRequestState = (state: RequestState) => {
    this.setState({ currentRequestState: state });
  }

  private setRequestPending() {
    this.setRequestState(RequestState.PENDING);
  }

  private setRequestSucceeded() {
    this.setRequestState(RequestState.SUCCEEDED);
  }

  private setRequestFailed() {
    this.setRequestState(RequestState.FAILED);
  }

  private async determineAuthorization(): Promise<boolean> {
    this.setRequestPending();
    try {
      if (!this.props.user) {
        throw new Error('Unauthorized');
      }
      const authorized = await adminService.isUserAuthorized(this.props.user);
      if (!this.checkAuthorized(authorized)) {
        return false;
      }
      
      this.setRequestSucceeded();
      return true;
    } catch (err) {
      this.setRequestState(RequestState.UNAUTHORIZED);
      this.props.history.push('/');
      return false;
    }
  }

  private updateTeams = async () => {
    try {
      await this.doRequest(() => adminService.updateTeams());
    } catch (err) {
      log.error(err);
    }
  }

  private async getStats(date: LocalDate) {
    try {
      const statsResponse: Response<StatGroup[]> = await this.doRequest(() => adminService.getTeamStats(date));
      this.setState({ currentDateStats: statsResponse.data });
    } catch (err) {
      log.error(err);
    }
  }

  private updateStats = async () => {
    try {
      await this.doRequest(() => adminService.updateStats(this.state.date));
      await this.getStats(this.state.date);
    } catch (err) {
      log.error(err);
    }
  }

  private deleteStats = async () => {
    try {
      await this.doRequest(() => adminService.deleteStats(this.state.date));
      this.setState({ currentDateStats: undefined });
    } catch (err) {
      log.error(err);
    }
  }

  private updateDatetime = () => {
    const datetime = LocalDateTime.of(this.state.date, LocalTime.of(9, 0));
    this.props.changeDate(datetime);
  }

  private doRequest = async (request: () => Promise<Response<any>>): Promise<Response<any>> => {
    this.setRequestPending();
    try {
      const response = await request();
      if (!this.checkAuthorized(response)) {
        throw new Error('Unauthorized');
      }
      
      if (response.status !== ResponseStatus.SUCCESS) {
        this.setRequestFailed();
        throw new Error('Failed');
      } else {
        this.setRequestSucceeded();
        return response;
      }
    } catch (err) {
      this.setRequestFailed();
      throw err;
    }
  }

  // private deleteAllStats = async () => {
  //   this.setRequestPending();
  //   try {
  //     const response = await adminService.deleteAllStats();
  //     if (!this.checkAuthorized(response)) {
  //       return;
  //     }

  //     if (response.status === ResponseStatus.SUCCESS) {
  //       this.setRequestSucceeded();
  //       this.setState({ currentDateStats: undefined });
  //     } else {
  //       this.setRequestFailed();
  //     }
  //   } catch (err) {
  //     this.setRequestFailed();
  //   }
  // }

  private checkAuthorized(response: Response<any>) {
    const authorized = response.status !== ResponseStatus.UNAUTHORIZED;
    if (!authorized) {
      this.setRequestState(RequestState.UNAUTHORIZED);
      this.props.history.push('/');
    }
    return authorized;
  }
}

// export const AdminOptions = withRouter(AdminOptionsComponent);

const mapDispatchToProps = (dispatch: Dispatch<RootState>): DispatchProps => {
  return {
    changeDate: datetime => dispatch(updateDatetime(datetime))
  };
};

const mapStateToProps = (state: RootState): StateProps => {
  return {
    user: state.sessionState.user,
    actualDate: state.sessionState.datetime,
  };
};

export const AdminOptions = withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminOptionsComponent) as any);