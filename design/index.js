window.onload = function () {
  var container = document.querySelector('.fixed-table__body .fixed-table__list');
  var fixedColumn = document.querySelector('.fixed-table__list.fixed-table__list--fixed');
  var fixedHeader = document.querySelector('.fixed-table__scrollable-header');
  var ps = new PerfectScrollbar(container);

  container.addEventListener('ps-scroll-y', scrollFixedColumn);
  container.addEventListener('ps-y-reach-start', function () {
    scrollFixedColumn({ target: { scrollTop: 0 } })
  });
  container.addEventListener('ps-scroll-x', scrollFixedHeader);
  container.addEventListener('ps-x-reach-start', function () {
    scrollFixedHeader({ target: { scrollLeft: 0 } })
  });

  function scrollFixedColumn(event) {
    fixedColumn.style.top = `${-event.target.scrollTop}px`;
  }

  function scrollFixedHeader(event) {
    fixedHeader.style.left = `${-event.target.scrollLeft}px`;
  }

}

function playingTodayChanged(checkbox) {
  console.log(`Playing today? ${checkbox.checked}`);
  var sidebarRows = document.querySelectorAll('.fixed-table__sidebar .fixed-table__row');
  var mainRows = document.querySelectorAll('.fixed-table__body .fixed-table__row');

  for (let i = 0; i < sidebarRows.length; i++) {
    const sideRow = sidebarRows[i];
    const mainRow = mainRows[i];

    if (checkbox.checked && !sideRow.classList.contains('playing')) {
      sideRow.style.display = 'none';
      mainRow.style.display = 'none';
    } else {
      sideRow.style.display = 'flex';
      mainRow.style.display = 'flex';
    }    
  }
}

function toggleHeatMap(checkbox) {
  var cells = document.querySelectorAll('.fixed-table__body .fixed-table__col'); //>span');

  cells.forEach(cell => {
    if (checkbox.checked) {
      var rank = Math.random();
      var hue = ((1 - rank) * 120).toString(10);
      var hsl = `hsl(${hue}, 75%, 50%)`

      // cell.style.background = `linear-gradient(to right, ${hsl}, rgba(0,0,0,0))`;
      cell.style.background = hsl;
      cell.style.color = 'black';
    } else {
      cell.style.background = 'inherit';
      cell.style.color = 'white';
      // cell.style.background = `linear-gradient(to right, rgba(0,0,0,0), rgba(0,0,0,0))`;
    }
  });
}

function openTab(tab) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("table-controls-data");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].className = tabcontent[i].className.replace(" active", "");
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tab.getAttribute('data-controls')).className += " active";
  tab.className += " active";
}
