import { Matchup, StatGroupResponse, statGroupToResponse } from 'statmaster-common';
import { controller, httpPost, request, requestBody } from 'inversify-express-utils';

import { DataUpdater } from '../updater/Updater';
import { Request } from 'express';
import { StatsService } from './StatsService';
import TYPES from '../inversify/types';
import { clientError } from '../utils/ClientError';
import { getLoggerForModule } from '../utils/log';
import { inject } from 'inversify';
import { parseDate } from '../utils/date';
import { toStatGroup } from './StatGroupDocument';

const log = getLoggerForModule(module);

@controller('/api/v1/stats')
export class StatGroupController {

  @inject(TYPES.StatsService) private statsService: StatsService;
  @inject(TYPES.Updater) private updater: DataUpdater;

  // Use POST to allow request data in body
  @httpPost('/query')
  async getStats( @requestBody() body: any): Promise<StatGroupResponse[]> {
    log.debug(`Get stats called with options: ${JSON.stringify(body)}`);
    
    const resources = body.resources || [];
    if (resources && !(resources instanceof Array)) {
      throw clientError(400, `Missing or empty parameter 'resources'`);
    }

    // TODO: process type e.g. team or player

    let date = body.date;
    if (date) {
      date = parseDate(date);
    }

    const statGroups = date
      ? await this.statsService.getStats(resources, date)
      : await this.statsService.getAggregateStats(resources);
    return statGroups.map(toStatGroup).map(statGroupToResponse);
  }

  // Use POST to allow request data in body
  @httpPost('/query/matchups')
  async getMatchupStats( @requestBody() body: any): Promise<StatGroupResponse[]> {
    log.debug(`Get stats called for matchups: ${body.matchups}`);
    if (!body.matchups || !(body.matchups instanceof Array)) {
      throw clientError(400, `Missing or empty parameter 'resources'`);
    }
    (body.matchups as Array<any>).forEach(matchup => {
      if (!matchup.resource || !matchup.opponent) {
        throw clientError(400, `Misformatted matchup: ${matchup}`);
      }
    });
    const matchups = body.matchups as Matchup[];

    const statGroups = await this.statsService.getAggregateMatchupStats(matchups);
    log.debug(`Found ${statGroups.length} stats`);
    return statGroups.map(toStatGroup).map(statGroupToResponse);
  }

  @httpPost('/team/:date')
  async updateTeamStats( @request() req: Request ): Promise<{}> {
    log.debug(`Update team stats called for ${req.params.date}`);

    const date = parseDate(req.params.date);

    // TODO: wrap into single function
    await this.updater.updateDailyStatsByTeams(date);
    await this.updater.updateAggregateTeamStats();
    await this.updater.updateAggregateTeamMatchupStats();
    return {};
  }
}
