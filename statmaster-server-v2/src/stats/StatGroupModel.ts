import * as mongoose from 'mongoose';

import { Model, Schema } from 'mongoose';

import { StatGroupDocument } from './StatGroupDocument';

const StatGroupSchema: Schema = new Schema(
  {
    resource: {
      type: Schema.Types.ObjectId,
      required: true,
    },

    // TODO: add type (game, matchup, team, player, etc.)

    // optional game for game stats
    game: {
      type: Schema.Types.ObjectId,
      ref: 'game'
    },

    opposing: {
      type: Schema.Types.ObjectId,
    },

    matchup: {
      type: String,
    },

    // See StatGroupDocument for schema
    stats: Schema.Types.Mixed,
    // stats: [{
    //   category: {
    //     type: String,
    //     enum: [Categories],
    //     required: true
    //   },
    //   key: {
    //     type: String,
    //     required: true
    //   },
    //   value: Number
    // }]

  },
  {
    timestamps: true
  }
);

StatGroupSchema.index(
  {
    resource: 1,
    opposing: 1,
  },
);

StatGroupSchema.index(
  {
    resource: 1,
    game: 1
  },
);

StatGroupSchema.index(
  {
    matchup: 1,
    game: 1
  }
);

export const StatGroupModel: Model<StatGroupDocument> = mongoose.model<StatGroupDocument>('stats', StatGroupSchema);
