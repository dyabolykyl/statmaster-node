import * as mongoose from 'mongoose';

import { Category, Stat, StatGroup, Topic, makeFieldKey } from 'statmaster-common';

import { Document } from '../db/Document';
import { objectId } from '../db/MongooseAdaptor';

interface StatsDocument {
  [index: string]: {
    category: Category;
    key: string;
    value: number;
  };
}

export interface StatGroupDocument extends Document {
  resource: mongoose.Types.ObjectId;
  opposing?: mongoose.Types.ObjectId;
  matchup?: string;
  game?: mongoose.Types.ObjectId;
  stats: StatsDocument;
}

export function toStatsMap(topic: Topic, statsDoc: StatsDocument): Map<string, Stat> {
  const stats = new Map<string, Stat>();
  Object.keys(statsDoc).forEach(key => {
    const stat = statsDoc[key];
    const fieldKey = makeFieldKey(topic, stat.category, stat.key);
    stats.set(fieldKey.flatKey, { key: fieldKey, value: stat.value });
  });
  return stats;
}

export function toStatGroup(doc: StatGroupDocument): StatGroup {
  const topic: Topic = doc.opposing ? Topic.RESOURCE_VS_OPPOSING : Topic.RESOURCE;
  const stats = toStatsMap(topic, doc.stats);

  return {
    id: doc.id,
    resource: doc.matchup || doc.resource.toString(),
    stats
  };
}

/** FOR TESTING */
export function toStatGroupDocument(statGroup: StatGroup): StatGroupDocument {
  return {
    id: statGroup.id,
    resource: objectId(statGroup.resource),
    stats: toStatsDocument(statGroup)
  } as StatGroupDocument;
}

export function toStatsDocument(statGroup: StatGroup): StatsDocument {
  const statsDoc = { };
  statGroup.stats.forEach((stat, key) => {
    const statKey = makeStatKey(stat.key.category, stat.key.fieldId);
    statsDoc[statKey] = {
      key: stat.key.fieldId,
      category: stat.key.category,
      value: stat.value
    };
  });
  return statsDoc;
}

export function makeStatKey(category: Category, key: string): string {
  return `${category}_${key}`;
}
