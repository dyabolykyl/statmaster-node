import { Matchup, StatGroup, makeMatchup, makeMatchupKey, mockStatGroups, toDate } from 'statmaster-common';
import { StatGroupDocument, toStatGroupDocument, toStatsDocument } from './StatGroupDocument';

import { GameModel } from '../games/GameModel';
import { LocalDate } from 'js-joda';
import { StatGroupModel } from './StatGroupModel';
import { clientError } from '../utils/ClientError';
import { getLoggerForModule } from '../utils/log';
import { injectable } from 'inversify';

const log = getLoggerForModule(module);

export interface StatsService {
  getStats(resources: string[], date: LocalDate): Promise<StatGroupDocument[]>;
  getAggregateStats(resources: string[]): Promise<StatGroupDocument[]>;
  getAggregateMatchupStats(matchups: Matchup[]): Promise<StatGroupDocument[]>;
  // TODO: getGameStats in date range?
  getGameStats(resources: string[]): Promise<StatGroupDocument[]>;
  updateAggregateStats(statGroup: StatGroup): Promise<StatGroupDocument | null>;
  updateAggregateMatchupStats(statGroup: StatGroup, opposing: string): Promise<StatGroupDocument | null>;
  updateGameStats(
    resource: string, opposing: string, game: string, statGroup: StatGroup): Promise<StatGroupDocument | null>;

  deleteAllStats(): Promise<any>;
}

@injectable()
export class MongoStatsService implements StatsService {

  async getStats(resources: string[], date: LocalDate) {
    return await StatGroupModel.aggregate([
      // Ignore aggregate stats
      {
        $match: { game: { $exists: true } }
      },
      // join to games to get date
      {
        $lookup: {
          from: GameModel.collection.name,
          localField: 'game',
          foreignField: '_id',
          as: 'gameDoc',
        }
      },
      // flatten games
      {
        $unwind: '$gameDoc'
      },
      // filter on date
      {
        $match: {
          'gameDoc.date': { $gte: toDate(date), $lt: toDate(date.plusDays(1)) },
        }
      },
      // set game back to just being an id reference
      {
        $addFields: {
          game: '$gameDoc._id'
        }
      },
      // remove the game doc field
      {
        $project: {
          gameDoc: 0
        }
      }
    ]).exec();
  }

  async getAggregateStats(resources: string[]): Promise<StatGroupDocument[]> {
    const query = {
      resource: { $in: resources },
      opposing: { $exists: false },
      game: { $exists: false }
    };
    log.debug(`Retrieving stats for ${resources}`);

    try {
      const docs = await StatGroupModel.find(query).exec();
      return docs;
    } catch (err) {
      if (err.name === 'CastError' && err.kind === 'ObjectId') {
        log.warn(`Invalid id in ${resources}`);
        throw clientError(404, 'Unknown resource id');
      }
      throw err;
    }
  }

  getAggregateMatchupStats(matchups: Matchup[]): Promise<StatGroupDocument[]> {
    const matchupKeys = matchups.map(makeMatchupKey);
    const query = {
      matchup: { $in: matchupKeys },
      game: { $exists: false }
    };
    log.debug(`Retrieving stats for ${matchupKeys}`);

    try {
      return StatGroupModel.find(query).exec();
    } catch (err) {
      if (err.name === 'CastError' && err.kind === 'ObjectId') {
        log.warn(`Invalid id in ${matchupKeys}`);
        throw clientError(404, 'Unknown resource id');
      }
      throw err;
    }
  }

  async getGameStats(resources: string[]) {
    const query = {
      resource: { $in: resources }, // .map(r => new Schema.Types.ObjectId(r)) }, 
      game: { $exists: true }
    };
    log.debug(`Retrieving game stats for ${resources}`);

    try {
      const docs = await StatGroupModel.find(query).exec();
      return docs;
    } catch (err) {
      if (err.name === 'CastError' && err.kind === 'ObjectId') {
        log.warn(`Invalid id in ${resources}`);
        throw clientError(404, 'Unknown resource id');
      }
      throw err;
    }
  }

  updateAggregateStats(statGroup: StatGroup) {
    const query = {
      resource: statGroup.resource,
      opposing: { $exists: false },
      game: { $exists: false }
      // TODO: year
    };
    const updatedDocument = {
      resource: statGroup.resource,
      stats: toStatsDocument(statGroup)
    };
    const options = {
      upsert: true,
      new: true
    };
    log.debug(`Updating aggregate stats for resource: ${statGroup.resource}`);
    return StatGroupModel.findOneAndUpdate(query, updatedDocument, options).exec();
  }

  updateAggregateMatchupStats(statGroup: StatGroup, opposing: string) {
    const matchupKey = makeMatchupKey(makeMatchup(statGroup.resource, opposing));
    const query = {
      matchup: matchupKey,
      game: { $exists: false }
      // TODO: year
    };
    const updatedDocument = {
      resource: statGroup.resource,
      opposing,
      matchup: matchupKey,
      stats: toStatsDocument(statGroup)
    };
    const options = {
      upsert: true,
      new: true
    };
    log.debug(`Updating aggregate matchup stats for resource: ${statGroup.resource}, opposing: ${opposing}`);
    return StatGroupModel.findOneAndUpdate(query, updatedDocument, options).exec();
  }

  updateGameStats(resource: string, opposing: string, game: string, statGroup: StatGroup) {
    const query = {
      resource, game
    };
    const updatedDocument = {
      resource,
      opposing,
      game,
      stats: toStatsDocument(statGroup)
    };
    const options = {
      upsert: true,
      new: true
    };
    log.debug(`Updating stats for game: ${game}, resource: ${resource}, opposing: ${opposing}`);
    return StatGroupModel.findOneAndUpdate(query, updatedDocument, options).exec();
  }

  /** Dangerous methods */
  deleteAllStats() {
    return StatGroupModel.remove({}).exec();
  }
}

@injectable()
export class StaticStatsService implements StatsService {
  private readonly statGroups: Map<string, StatGroup> = new Map();

  constructor() {
    mockStatGroups.forEach(statGroup => this.statGroups.set(statGroup.id, statGroup));
    log.debug(`Initialized static stat group repository: ${Array.from(this.statGroups.keys())}`);
  }

  getStats(resources: string[], date: LocalDate) {
    return Promise.resolve([]);
  }

  getGameStats = (resources: string[]): Promise<StatGroupDocument[]> => {
    return Promise.resolve([]);
  }

  updateGameStats(resource: string, opposing: string, game: string, statGroup: StatGroup) {
    return Promise.resolve(null);
  }

  getAggregateStats(resources: string[]) {
    const resourcesSet = new Set(resources);
    log.debug(`Retrieving stats for ${resources}`);
    const filteredStatGroups = Array.from(this.statGroups.values())
      .filter(statGroup => resourcesSet.has(statGroup.resource))
      .map(toStatGroupDocument);
    return Promise.resolve(filteredStatGroups);
  }

  getAggregateMatchupStats(matchups: Matchup[]) {
    return Promise.resolve([]);
  }

  updateAggregateStats(statGroup: StatGroup) {
    return Promise.resolve(null);
  }
  updateAggregateMatchupStats(statGroup: StatGroup, opposing: string) {
    return Promise.resolve(null);
  }

  deleteAllStats() {
    return Promise.resolve(null);
  }
}