import * as bodyParser from 'body-parser';
import * as config from './config';
import * as cors from 'cors';
import * as mongoose from 'mongoose';
import * as passport from 'passport';
import * as session from 'express-session';

import { Application, NextFunction, Request, Response } from 'express';

import { ClientError } from './utils/ClientError';
import { DbAdaptor } from './db/index';
import { InversifyExpressServer } from 'inversify-express-utils';
import { Server } from 'http';
import { connectLogger } from 'log4js';
import { getLoggerForModule } from './utils/log';

const log = getLoggerForModule(module);

// Creates and configures an ExpressJS web server.
export class App {

  // ref to Express instance
  readonly server: InversifyExpressServer;
  readonly dbAdaptor: DbAdaptor;
  readonly port: any;
  expressApp: Application;
  private configured = false;
  private running = false;

  // Run configuration methods on the Express instance.
  constructor(server: InversifyExpressServer, dbAdaptor: DbAdaptor, port: number | string) {
    this.server = server;
    this.dbAdaptor = dbAdaptor;
    this.port = normalizePort(port);
  }

  configure = (): void => {
    log.info('Configuring Statmaster app');
    this.middleware();
    this.expressApp = this.server.build();
    this.expressApp.get('/', (err: any, req: Request, res: Response, next: NextFunction) => {
      log.info('GET on / called');
      next();
    });
    this.configured = true;
  }

  start = () => {
    if (!this.configured) {
      log.error(`Cannot start without configuring`);
      throw new Error('Need to configure first');
    }

    if (this.running) {
      return;
    }
    this.running = true;
    log.info('Starting Statmaster app');
    this.startServer();
  }

  // Configure Express middleware.
  private middleware = (): void => {
    this.server.setConfig(app => {
      app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        log.info('This is middleware');
        next();
      });
      app.use(connectLogger(log, { level: 'auto' }));

      const origin = config.corsOrigin;
      log.debug(`Allowing CORS origin: ${origin}`);
      app.use(cors( { origin, credentials: true }));

      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({ extended: false }));
      
      app.use(session({
        secret: config.sessionSecret,
        resave: false,
        saveUninitialized: false
      }));
      app.use(passport.initialize());
      app.use(passport.session());
    });

    this.server.setErrorConfig(app => {
      app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        if (err && err.clientError) {
          log.error(err.message);
          const clientError = err.clientError as ClientError;
          res.status(clientError.status);
          res.send(clientError.message);
        } else {
          log.error(err);
          res.status(500);
          res.send('Server error');
        }
      });
    });
  }

  private startServer = async (): Promise<void> => {
    try {
      this.dbAdaptor.disconnect();
      await this.dbAdaptor.connect();
      mongoose.set('debug', true);
    } catch (err) {
      log.error(err);
      process.exit(1);
    }

    const httpServer = this.expressApp.listen(this.port, (err: any) => {
      if (err) {
        return log.error(err);
      }

      return log.trace(`Server is listening on port ${this.port}`);
    });

    httpServer.on('error', this.onError);
    httpServer.on('listening', () => this.onListening(httpServer));
    return Promise.resolve();
  }

  /**
   * Event listener for HTTP server "error" event.
   */
  private onError = (error: any) => {
    if (error.syscall !== 'listen') {
      throw error;
    }

    var bind = typeof this.port === 'string'
      ? 'Pipe ' + this.port
      : 'Port ' + this.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        log.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        log.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  /**
   * Event listener for HTTP server "listening" event.
   */
  private onListening = (server: Server) => {
    var addr = server.address();
    var bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    log.debug('Listening on ' + bind);
  }
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val: any) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}