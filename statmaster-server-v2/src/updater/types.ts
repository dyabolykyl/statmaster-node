import { LocalDate, LocalTime } from 'js-joda';

import { StatGroup } from 'statmaster-common';

export enum SideSpecifier {
  HOME = 'home',
  AWAY = 'away'
}

export interface GameInfo {
  msfId: string;
  date: LocalDate;
  time: LocalTime;
  away: string;
  home: string;
}

export interface GameLog {
  gameInfo: GameInfo;
  team: string;
  side: SideSpecifier;
  opposing: string;
  statGroup: StatGroup;
}
