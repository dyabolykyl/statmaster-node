import { inject, injectable } from 'inversify';

import { DataUpdater } from './Updater';
import TYPES from '../inversify/types';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

@injectable()
export class Orchestrator {

  private readonly updater: DataUpdater;

  constructor( @inject(TYPES.Updater) updater: DataUpdater) {
    this.updater = updater;
  }

  async updateTeams() {
    log.info(`Updating teams`);
    await this.updater.updateTeams();
    log.info(`Teams updated`);
  }

}
