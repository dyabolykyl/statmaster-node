import { Stat, StatMap, getField } from 'statmaster-common';

export function calculateAggregateStats(statMaps: StatMap[]): StatMap {
  const aggregateStats = new Map<string, Stat>();

  statMaps.forEach((statsMap) => {
    statsMap.forEach((stat, key) => {
      const field = getField(stat.key.category, stat.key.fieldId);
      if (field === undefined) {
        return;
      }

      // TODO: calculate derived stats
      if (field.derived) {
        return;
      }

      let aggregateStat = aggregateStats.get(key);
      if (aggregateStat === undefined) {
        aggregateStat = stat;
      } else {
        aggregateStat.value += stat.value;
      }
      aggregateStats.set(key, aggregateStat);
    });
  });

  return aggregateStats;
}