
export const ABBREVIATION_KEY = '@abbreviation';
export const CATEGORY_KEY = '@category';
export const TEXT_KEY = '#text';

export interface StatEntry {
  '@category': string;
  '@abbreviation': string;
  '#text': string;
}

export interface GameLogEntry {
  game: GameInfoEntry;
  team: {
    ID: string;
    City: string;
    Name: string;
    Abbreviation: string;
  };
  stats: {
    [index: string]: StatEntry;
  };
}

export interface GameLogEntries {
  lastUpdatedOn: string;
  gamelogs: GameLogEntry[];
}

export interface GameInfoEntry {
  id: string;
  date: string;
  time: string;
  awayTeam: {
    ID: string;
    City: string;
    Name: string;
    Abbreviation: string;
  };
  homeTeam: {
    ID: string;
    City: string;
    Name: string;
    Abbreviation: string;
  };
}

export interface FullGameSchedule {
  fullgameschedule: {
    lastUpdatedOn: string;
    gameentry: GameInfoEntry[];
  };
}