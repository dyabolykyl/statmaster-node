import { MySportsFeed } from '../Fetcher';
import axios from 'axios';
import { getLoggerForModule } from '../../utils/log';

const log = getLoggerForModule(module);

const HOSTNAME = 'www.mysportsfeeds.com';
const BASE_URL = '/api/feed/pull';

export class RequestSender implements MySportsFeed {

  private username: string;
  private password: string;

  authenticate = (username: string, password: string) => {
    this.username = username;
    this.password = password;
  }

  getData = async (league: string, season: string, feed: string, format: string, params: Object): Promise<Object> => {
    if (!this.username || !this.password) {
      throw new Error(`Username and password are not set`);
    }

    let url = `https://${HOSTNAME}${BASE_URL}/${league}/${season}/${feed}.${format}`;
    // if (params) {
    //   url += '?';
    //   let first = true;
    //   Object.keys(params).forEach(key => {
    //     url += !first ? '&' : '';
    //     url += `${key}=${params[key]}`;
    //   });
    //   log.debug(`Making request for ${url}`);
    // }

    log.debug(`Making request for ${url}. Params: ${JSON.stringify(params)}`);
    const username = this.username;
    const password = this.password;
    return await axios.get(url, {
      auth: { username, password },
      responseType: format,
      params
    }).then(res => {
      // log.trace(JSON.stringify(res));
      if (!res.status) {
        throw new Error(`Response is missing`);
      } else if (res.status !== 200) {
        throw new Error(`Request failed with status: ${res.status}`);
      }
      return res.data;
    });
  }

}