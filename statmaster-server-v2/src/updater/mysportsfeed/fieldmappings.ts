import { BATTING_FIELDS, Field } from 'statmaster-common';
export const MSF_BATTING_MAPPINGS = new Map<string, Field>();

function addBattingMapping(msfKey: string, fieldId: string): void {
  if (BATTING_FIELDS.has(fieldId)) {
    MSF_BATTING_MAPPINGS.set(msfKey, BATTING_FIELDS.get(fieldId) as Field);
  }
}

// home runs
addBattingMapping('HR', 'hr');
// strike outs
addBattingMapping('SO', 'so');
// plate appearances
addBattingMapping('PA', 'pa');
// rbis
addBattingMapping('RBI', 'rbi');
// doubles
addBattingMapping('2B', '2b');
// triples
addBattingMapping('3B', '3b');
// hits
addBattingMapping('H', 'h');
// total bases
addBattingMapping('TB', 'tb');
// at bats
addBattingMapping('AB', 'ab');
// runs
addBattingMapping('R', 'r');
// hit by pitch
addBattingMapping('HBP', 'hbp');
// sac flies
addBattingMapping('SacF', 'sf');
// extra base hits
addBattingMapping('XBH', 'xbh');
// walks
addBattingMapping('BB', 'bb');