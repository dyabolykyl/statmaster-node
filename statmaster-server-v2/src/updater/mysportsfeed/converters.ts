import { ABBREVIATION_KEY, CATEGORY_KEY, GameInfoEntry, GameLogEntry, TEXT_KEY } from './responses';
import { BATTING_FIELDS, makeMatchupKey } from 'statmaster-common';
import {
  Category,
  FieldId,
  Stat,
  StatGroup,
  Topic,
  makeFieldKey,
  makeMatchup
} from 'statmaster-common';
import { GameInfo, GameLog, SideSpecifier } from '../types';
import { LocalDate, LocalTime } from 'js-joda';

import { MSF_BATTING_MAPPINGS } from './fieldmappings';
import { getLoggerForModule } from '../../utils/log';

const log = getLoggerForModule(module);

function convertCategory(category: string): Category | undefined {
  switch (category) {
    case 'Batting':
      return Category.BATTING;
    case 'Pitching':
      return Category.PITCHING;
    case 'Standings':
      return Category.STANDINGS;
    case 'Fielding':
      return Category.FIELDING;
    default:
      return undefined;
  }
}

export function convertGameInfoEntry(gameInfoEntry: GameInfoEntry): GameInfo {

  const pm = /PM/.test(gameInfoEntry.time);
  let timeString = gameInfoEntry.time.replace(/[AP]M/, '');
  if (timeString.match(/^\d:\d\d$/)) {
    timeString = `0${timeString}`;
  }
  const time = LocalTime.parse(timeString);
  if (pm) {
    time.plusHours(12);
  }

  return {
    msfId: gameInfoEntry.id,
    date: LocalDate.parse(gameInfoEntry.date),
    time: time,
    home: gameInfoEntry.homeTeam.Abbreviation,
    away: gameInfoEntry.awayTeam.Abbreviation
  };
}

export function convertGameLog(gameLog: GameLogEntry): GameLog {

  const gameInfo = convertGameInfoEntry(gameLog.game);

  const team = gameLog.team.Abbreviation;
  log.trace(`Converting game log for ${team}`);
  let side, opposing;
  if (gameInfo.home === team) {
    side = SideSpecifier.HOME;
    opposing = gameLog.game.awayTeam.Abbreviation;
  } else {
    side = SideSpecifier.AWAY;
    opposing = gameLog.game.homeTeam.Abbreviation;
  }

  const stats = new Map<FieldId, Stat>();
  Object.keys(gameLog.stats).forEach(key => {
    const statEntry = gameLog.stats[key];
    const category = convertCategory(statEntry[CATEGORY_KEY]);
    if (category === undefined) {
      log.error(`Unkown category: ${statEntry[CATEGORY_KEY]}`);
      return;
    }

    if (category !== Category.BATTING) {
      // TODO: handle non batting stats
      return;
    }
    const mappedField = MSF_BATTING_MAPPINGS.get(statEntry[ABBREVIATION_KEY]);
    if (mappedField === undefined || mappedField.derived) {
      return;
    }

    const value = (statEntry[TEXT_KEY] as any) * 1;

    const stat: Stat = {
      key: makeFieldKey(Topic.RESOURCE, category, mappedField.id),
      value: value
    };

    stats.set(stat.key.flatKey, stat);
  });

  log.trace(`Found ${stats.size} stats`);

  // default any missing fields to 0
  BATTING_FIELDS.forEach(field => {
    if (!field.derived) {
      const key = makeFieldKey(Topic.RESOURCE, Category.BATTING, field.id);
      if (!stats.has(key.flatKey)) {
        const stat: Stat = {
          key,
          value: 0
        };
        stats.set(stat.key.flatKey, stat);
      }
    }
  });

  const statGroup: StatGroup = {
    id: 'dummy',
    resource: makeMatchupKey(makeMatchup(team, opposing)),
    stats,
  };

  return {
    gameInfo,
    team,
    side,
    opposing,
    statGroup
  };
}