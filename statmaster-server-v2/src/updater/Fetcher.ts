import * as config from '../config';
import * as prettyjson from 'prettyjson';

import { DateTimeFormatter, LocalDate } from 'js-joda';
import { FullGameSchedule, GameLogEntries } from './mysportsfeed/responses';
import { GameInfo, GameLog } from './types';
import { StatGroup, Team } from 'statmaster-common';
import { convertGameInfoEntry, convertGameLog } from './mysportsfeed/converters';
import { inject, injectable } from 'inversify';

import { TEAMS } from '../team/definitions';
import TYPES from '../inversify/types';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

export interface Fetcher {
  fetchTeams(): Promise<Team[]>;
  fetchTeamStats(): Promise<StatGroup[]>;
  fetchTeamGameLogsByTeams(date: LocalDate, teams: string[]): Promise<GameLog[]>;
  fetchTeamGameLogsByGames(date: LocalDate, games: GameInfo[]): Promise<GameLog[]>;
  fetchGameSchedule(date: LocalDate): Promise<GameInfo[]>;
}

@injectable()
export class MySportsFeedFetcher implements Fetcher {

  private readonly league = 'mlb';
  private readonly year = '2017';
  private readonly client: MySportsFeed;
  private readonly dateFormatter = DateTimeFormatter.ofPattern('yyyyMMdd');

  constructor( @inject(TYPES.MySportsFeedFactory) clientFactory: () => MySportsFeed) {
    this.client = clientFactory();
    this.client.authenticate(config.mySportsFeedUsername, config.mySportsFeedPassword);
  }

  fetchTeams(): Promise<Team[]> {
    return Promise.resolve(TEAMS);
  }

  fetchTeamStats = async () => {
    log.debug(`Getting team stats for ${this.year}`);
    const data = await this.makeRequest('overall_team_standings');
    log.debug(`Data:\n${prettyjson.render(data)}`);
    return [];
  }

  fetchTeamGameLogsByTeams = async (date: LocalDate, teams: string[]): Promise<GameLog[]> => {
    return await this.fetchTeamGameLogs(date, teams);
  }

  fetchTeamGameLogsByGames = async (date: LocalDate, games: GameInfo[]): Promise<GameLog[]> => {
    return await this.fetchTeamGameLogs(date, undefined, games);
  }

  fetchGameSchedule = async (date: LocalDate): Promise<GameInfo[]> => {
    if (!date) {
      throw new Error('Date is missing');
    }
    log.debug(`Getting game schedule for ${date}`);
    const start = this.formatDate(date);

    const data: any = await this.makeRequest('full_game_schedule', { date: start });
    log.trace(`Data:\n${prettyjson.render(data)}`);

    const gameLogEntries = (data as FullGameSchedule).fullgameschedule.gameentry;
    if (!gameLogEntries) {
      throw new Error(`Game log entries are undefined: ${prettyjson.render(data)}`);
    }

    return gameLogEntries.map(convertGameInfoEntry);
  }

  private fetchTeamGameLogs = async (date: LocalDate, teams?: string[], games?: GameInfo[]): Promise<GameLog[]> => {
    if (!date) {
      throw new Error('Date is missing');
    }

    if ((!teams || teams.length === 0) && (!games || games.length === 0)) {
      throw new Error('Must specify either games or teams');
    }

    log.debug(`Getting team stats for ${date}. Teams: ${teams}. Games: ${JSON.stringify(games)}`);
    const start = this.formatDate(date);
    const options: any = { 'date': start };
    if (teams) {
      options.team = teams.map(t => t.toLowerCase()).join(',');
    }
    if (games) {
      options.game = games.map(this.formatGame);
    }

    const data: any = await this.makeRequest('team_gamelogs', options);
    // log.trace(`Data:\n${prettyjson.render(data)}`);
    const teamGameLogs: GameLogEntries = data.teamgamelogs as GameLogEntries;

    // log.debug(`Converting response...`);
    return teamGameLogs.gamelogs.map(convertGameLog);
  }

  private makeRequest = async (object: string, options?: any): Promise<Object> => {
    options = options || {};
    options.force = true;
    return await this.client.getData(this.league, `${this.year || '2017'}-regular`, object, 'json', options);
  }

  private formatDate = (date: LocalDate) => {
    return this.dateFormatter.format(date);
  }

  private formatGame = (game: GameInfo) => {
    if (!game.date || !game.away || !game.home) {
      throw new Error(`Cannot format game: ${JSON.stringify(game)}`);
    }
    return `${this.formatDate(game.date)}-${game.away}-${game.home}`;
  }

}

export interface MySportsFeed {
  authenticate(username: string, password: string): void;
  getData(league: string, season: string, feed: string, format: string, params: Object): Promise<Object>;
}
