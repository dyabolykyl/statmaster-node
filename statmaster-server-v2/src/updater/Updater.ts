import * as prettyjson from 'prettyjson';

import { GameLog, SideSpecifier } from './types';
import { Multimap, StatMap } from 'statmaster-common';
import {
  StatGroup,
  Topic,
  calculateDerivedStats,
  makeMatchup,
  makeMatchupKey,
  parseMatchupKey
} from 'statmaster-common';
import { StatGroupDocument, toStatsMap } from '../stats/StatGroupDocument';
import { inject, injectable } from 'inversify';

import { Fetcher } from './Fetcher';
import { GameDocument } from '../games/GameDocument';
import { GameService } from '../games/GameService';
import { LocalDate } from 'js-joda';
import { StatsService } from '../stats/StatsService';
import TYPES from '../inversify/types';
import { TeamDocument } from '../team/TeamDocument';
import { TeamService } from '../team/TeamService';
import { calculateAggregateStats } from './calculator';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

export interface DataUpdater {
  updateTeams(): Promise<any>;
  updateGames(date: LocalDate): Promise<any>;
  updateDailyStatsByTeams(date: LocalDate, teams?: string[]): Promise<any>;
  updateDailyStatsByGames(date: LocalDate): Promise<any>;
  updateAggregateTeamStats(): Promise<any>;
  updateAggregateTeamMatchupStats(): Promise<any>;
  deleteAllStats(): Promise<any>;
}

@injectable()
export class Updater implements DataUpdater {

  private readonly fetcher: Fetcher;
  private readonly teamService: TeamService;
  private readonly statsService: StatsService;
  private readonly gameService: GameService;

  constructor(
    @inject(TYPES.Fetcher) fetcher: Fetcher,
    @inject(TYPES.TeamService) teamService: TeamService,
    @inject(TYPES.StatsService) statsService: StatsService,
    @inject(TYPES.GameService) gameService: GameService) {
    this.fetcher = fetcher;
    this.teamService = teamService;
    this.statsService = statsService;
    this.gameService = gameService;
  }

  updateTeams = async (): Promise<any> => {
    const teams = await this.fetcher.fetchTeams();
    log.debug(`Fetched ${teams.length} teams`);

    const updates = await Promise.all(teams.map(async team => {
      await this.teamService.updateTeam(team);
      log.debug(`Updated team:\n${prettyjson.render(team)}`);
      return;
    }));
    log.debug(`Updated ${teams.length} teams`);
    return updates;
  }

  updateGames = async (date: LocalDate) => {
    if (!date) {
      throw new Error('Missing date');
    }
    const games = await this.fetcher.fetchGameSchedule(date);
    const teams = await this.teamService.getTeams();
    const teamsByInitials = teams.reduce(
      (map: Map<string, TeamDocument>, team) => map.set(team.abbreviation, team),
      new Map());

    // replace teams by their DB ids
    games.forEach(game => {
      const home = teamsByInitials.get(game.home);
      const away = teamsByInitials.get(game.away);
      if (!home || !away) {
        throw new Error(`Could not find team for ${game.home} vs ${game.away}`);
      }
      game.home = home.id;
      game.away = away.id;
    });

    return await this.gameService.updateGames(games);
  }

  updateDailyStatsByTeams = async (date: LocalDate): Promise<any> => {
    const teams = await this.teamService.getTeams();
    if (teams.length < 1) {
      throw new Error('No teams found. Run update teams first');
    }
    const teamIds = teams.map(t => t.abbreviation);
    log.debug(`Updating daily team stats for ${teamIds} on ${date.toString()}`);
    const gameLogs = await this.fetcher.fetchTeamGameLogsByTeams(date, teamIds);
    return this.processTeamGameLogs(gameLogs);
  }

  updateDailyStatsByGames = async (date: LocalDate): Promise<any> => {
    log.debug(`Updating daily team stats for ${date.toString()}`);
    const games = await this.fetcher.fetchGameSchedule(date);
    const gameLogs = await this.fetcher.fetchTeamGameLogsByGames(date, games);
    return await this.processTeamGameLogs(gameLogs);
  }

  updateAggregateTeamStats = async (): Promise<any> => {
    // get teams
    // for each team, get all game stats
    // for all non derived stats, add them together
    // for all derived stats, calculate later
    // save as stat, game == false

    const teams = await this.teamService.getTeams();
    const teamIds = teams.map(team => team.id);
    const allStatGroups = await this.statsService.getGameStats(teamIds);
    const statGroupsByTeam = new Multimap<string, StatGroupDocument>();
    allStatGroups.forEach(statGroup => {
      statGroupsByTeam.put(statGroup.resource.toString(), statGroup);
    });

    let updates: Promise<any> = Promise.resolve();
    teams.forEach(team => {
      updates = updates.then(async () => {
        const statDocs = statGroupsByTeam.getAll(team.id) || new Set();
        log.debug(`Calculating team stats for ${team.abbreviation} with ${statDocs.size} statGroups`);
        const statGroup = this.createAggregateStatGroup(statDocs, team.id);
        return await this.statsService.updateAggregateStats(statGroup);
      });
    });

    return updates;
  }

  updateAggregateTeamMatchupStats = async (): Promise<any> => {
    // get teams
    // for each team, get all game stats
    // sort stats by team + opponent
    // for all non derived stats, add them together
    // for all derived stats, calculate later
    // save as team, opposing, game == false

    log.debug(`Updating aggregate team matchup stats`);
    const teams = await this.teamService.getTeams();
    const teamIds = teams.map(team => team.id);
    const allStatGroups = await this.statsService.getGameStats(teamIds);
    const statGroupsByMatchup = new Multimap<string, StatGroupDocument>();
    allStatGroups.forEach(statGroup => {
      if (statGroup.opposing === undefined) {
        return;
      }
      const matchupKey = makeMatchupKey(makeMatchup(statGroup.resource.toString(), statGroup.opposing.toString()));
      statGroupsByMatchup.put(matchupKey, statGroup);
      log.debug(`Added ${matchupKey} to statGroups`);
    });

    let updates: Promise<any> = Promise.resolve();
    statGroupsByMatchup.forEach((statGroupDocs, matchupKey) => {
      log.debug(`Parsing matchup key for ${matchupKey}`);
      const matchup = parseMatchupKey(matchupKey);
      updates = updates.then(() => {
        log.debug(`Calculating team matchup stats for ${matchupKey} with ${statGroupDocs.size} statGroups`);
        const statGroup = this.createAggregateStatGroup(statGroupDocs, matchup.resource);
        return this.statsService.updateAggregateMatchupStats(statGroup, matchup.opponent);
      });
    });

    return updates;
  }

  /** Dangerous methods */
  deleteAllStats = async () => {
    log.warn(`UNSAFE OPERATION: Deleting all stats`);
    await this.statsService.deleteAllStats();
    await this.gameService.deleteAllGames();
  }

  /****************************/

  private createAggregateStatGroup(statGroupDocs: Set<StatGroupDocument>, resource: string): StatGroup {
    let statsMaps: StatMap[] = [];
    statGroupDocs.forEach(statDoc => statsMaps.push(toStatsMap(Topic.RESOURCE, statDoc.stats)));
    const aggregateStatsMap = calculateAggregateStats(statsMaps);
    const statGroup = {
      id: 'dummy', // ignored
      resource,
      stats: aggregateStatsMap
    };
    calculateDerivedStats(statGroup, Topic.RESOURCE);
    return statGroup;
  }

  private processTeamGameLogs = async (gameLogs: GameLog[]): Promise<any> => {
    let updates: Promise<any> = Promise.resolve();
    // process games serially
    gameLogs.forEach((gameLog) => {
      updates = updates.then(async () => {
        const game = await this.processTeamGameLog(gameLog);
        if (game === null) {
          log.error(`Unable to get or create game for ${gameLog.gameInfo.msfId}`);
          return;
        }

        return await this.processTeamGameStats(game, gameLog.side, gameLog.statGroup);
      });
    });

    return await updates;
  }

  private processTeamGameLog = async (gameLog: GameLog): Promise<GameDocument | null> => {
    const gameInfo = gameLog.gameInfo;
    log.debug(`Processing gameLog for game ${gameInfo.msfId}: ${gameLog.team} vs ${gameLog.opposing}`);
    const resourceId = await this.teamService.getTeamId(gameLog.team);
    if (resourceId === null) {
      log.error(`Unable to retrieve id for ${gameLog.team}`);
      return null;
    }
    const opposingId = await this.teamService.getTeamId(gameLog.opposing);
    if (opposingId === null) {
      log.error(`Unable to retrieve id for ${gameLog.opposing}`);
      return null;
    }

    // replace teams by their DB ids
    gameInfo.home = gameLog.side === SideSpecifier.HOME ? resourceId : opposingId;
    gameInfo.away = gameInfo.home === resourceId ? opposingId : resourceId;
    return await this.gameService.updateGame(gameInfo);
  }

  private processTeamGameStats =
    async (game: GameDocument, side: SideSpecifier, statGroup: StatGroup): Promise<any> => {
      const resourceId = side === SideSpecifier.HOME ? game.home.team : game.away.team;
      const opposingId = side === SideSpecifier.AWAY ? game.home.team : game.away.team;
      calculateDerivedStats(statGroup, Topic.RESOURCE);
      return await this.statsService.updateGameStats(resourceId, opposingId, game.id, statGroup);
    }

}
