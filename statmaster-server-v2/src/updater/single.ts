import * as prettyjson from 'prettyjson';

import { LocalDate, LocalTime } from 'js-joda';

import { Fetcher } from './Fetcher';
import TYPES from '../inversify/types';
// import { Updater } from './Updater';
import container from '../inversify/container';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

// const updater: Updater = container.get(TYPES.Updater);
const fetcher: Fetcher = container.get(TYPES.Fetcher);

// (async () => {
//   try {
//     const gameLogs = await fetcher.fetchTeamStatsForDate(moment('2017-08-01'), ['bos', 'cle']);
//     log.debug(prettyjson.render(gameLogs));
//     // await fetcher.fetchGameSchedule(moment('2017-07-01'));
//   } catch (err) {
//     log.error(`${err}\n${err.stacktrace}`);
//   }
// })();

const gameInfo = {
  msfId: '42055',
  date: LocalDate.now(),
  time: LocalTime.now(),
  home: 'HOU',
  away: 'NYY'
};

process.on('unhandledRejection', async (reason, p) => {
  // Will print "unhandledRejection err is not defined"
  console.log(`unhandledRejection: ${reason}. Promise: ${p}\n${reason.stack}`);
  process.exit(1);
});

fetcher.fetchTeamGameLogsByGames(LocalDate.parse('2017-07-01'), [gameInfo])
  .then(result => console.info(prettyjson.render(Array.from(result.keys()))))
  .catch(err => log.error(err));