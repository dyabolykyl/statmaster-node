import * as yargs from 'yargs';

import { DataUpdater } from './Updater';
import { DbAdaptor } from '../db/index';
import { LocalDate } from 'js-joda';
import TYPES from '../inversify/types';
import container from '../inversify/container';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

const argv = yargs
  .demandCommand(1)
  .command('update-teams', 'Update team records')
  .command('update-games', 'Update game info for date', y => {
    return y.positional('date', {
      alias: 'd',
      describe: 'Date of games to update',
    });
  })
  .command('game-stats-by-team', 'Update teams stats for date', y => {
    return y.positional('date', {
      alias: 'd',
      describe: 'Date of games to update stats for',
    });
  })
  .command('team-game-stats', 'Update teams stats for date', y => {
    return y.positional('date', {
      alias: 'd',
      describe: 'Date of games to update stats for',
    });
  })
  .command('aggregate-team-stats', 'Update aggregate teams stats')
  .command('aggregate-team-matchup-stats', 'Update aggregate teams matchup stats')
  .command('delete-all-stats', 'Delete all stats (WARNING, DANGEROUS)')
  .help('h')
  .alias('h', 'help')
  .usage('Usage: $) <command> [options]')
  .argv;

log.trace(`args: ${argv}`);
const updater = container.get<DataUpdater>(TYPES.Updater);
const dbAdaptor = container.get<DbAdaptor>(TYPES.DbAdaptor);

log.debug(`Executing update command: ${argv._[0]}`);

async function closeDb() {
  try {
    log.info(`Disconnecting from DB`);
    await dbAdaptor.disconnect();
  } catch (err) {
    log.error(`Unable to close DB: ${err}`);
  }
}

process.on('unhandledRejection', async (reason, p) => {
  // Will print "unhandledRejection err is not defined"
  console.log(`unhandledRejection: ${reason}. Promise: ${p}`);
  await closeDb();
  process.exit(1);
});

let exitCode = 0;
(async () => {
  try {
    await dbAdaptor.singleConnect();
    log.info(`Connected to DB`);

    switch (argv._[0]) {
      case 'update-teams':
        await updater.updateTeams();
        break;
      case 'update-games':
        await updater.updateGames(LocalDate.parse(argv.date));
        break;
      case 'game-stats-by-team':
        await updater.updateDailyStatsByTeams(LocalDate.parse(argv.date));
        break;
      case 'aggregate-team-stats':
        await updater.updateAggregateTeamStats();
        break;
      case 'aggregate-team-matchup-stats':
        await updater.updateAggregateTeamMatchupStats();
        break;
      case 'delete-all-stats':
        await updater.deleteAllStats();
        break;
      default:
        throw new Error(`Unknown command: ${argv._[0]}`);
    }
  } catch (err) {
    log.error(`Failed to run command: ${err}\n${err.stack}`);
    exitCode = 1;
  } finally {
    await closeDb();
    log.info(`Exiting`);
    process.exit(exitCode);
  }
})();
