import * as bcrypt from 'bcrypt-nodejs';
import * as mongoose from 'mongoose';

import { Schema } from 'mongoose';
import { UserDocument } from './UserDocument';

const SALT_WORK_FACTOR = 10;

/**
 * MongooseSchema
 * @type {"mongoose".Schema}
 * @private
 */
let UserSchema: Schema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      lowercase: true,
      index: { unique: true, dropDups: true }
    },

    password: {
      type: String,
      required: true
    },

    admin: Boolean,

    isLoggedIn: Boolean
  },
  {
    timestamps: true
  });

UserSchema.pre('save', function (this: any, next: any) {
  if (this._doc) {
    let doc = this._doc as UserDocument;
    if (!this.isModified('password')) {
      return next();
    }

    doc.password = bcrypt.hashSync(doc.password, bcrypt.genSaltSync(SALT_WORK_FACTOR));
  }

  next();
});

UserSchema.methods.comparePassword = function (this: UserDocument, candidatePassword: string): Boolean {
  return bcrypt.compareSync(candidatePassword, this.password);
};

export const UserModel: mongoose.Model<UserDocument> = mongoose.model<UserDocument>('user', UserSchema);
