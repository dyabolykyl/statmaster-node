// import * as passport from 'passport';

import { controller, httpGet, request } from 'inversify-express-utils';
import { requireAdmin, requireCorrectUser, requireLogin } from '../auth/middleware';

import { Request } from 'express';
// import TYPES from '../inversify/types';
// import { UserService } from './UserService';
import { clientError } from '../utils/ClientError';
import { getLoggerForModule } from '../utils/log';
// import { inject } from 'inversify';
import { toClientUser } from './UserDocument';

const log = getLoggerForModule(module);

@controller('/api/v1/users')
export class UserController {
/* 
  private readonly userService: UserService;

  constructor(@inject(TYPES.UserService) userService: UserService) {
    super();
    this.userService = userService;
  }
 */
  @httpGet('/:userId', requireLogin)
  public async getUser(@request() req: Request) {
    log.debug(`Sessions: ${(<any> req).sessionStore}`);
    const userId = req.params.userId;
    const user = await requireCorrectUser(req, userId);

    log.debug(`User in request: ${user}`);
    return toClientUser(user);
  }

  @httpGet('/:userId/admin', requireLogin)
  public async getAdminProperty(@request() req: Request) {
    const isAdmin = await requireAdmin(req);
    if (!isAdmin) {
      throw clientError(403, 'Unauthorized');
    }
    return { admin: true };
  }
} 