import { UserDocument } from './UserDocument';
import { UserModel } from './UserSchema';
import { injectable } from 'inversify';

export interface UserService {
  getByEmail(email: string): Promise<UserDocument>;
  getById(id: string): Promise<UserDocument>;
  create(email: string, password: string, admin?: boolean): Promise<UserDocument>;
  isRegistered(email: string): Promise<boolean>;
}

@injectable()
export class MongoUserService implements UserService {

  async getById(id: string): Promise<UserDocument> {
    return await UserModel.findById(id) as UserDocument;
  }

  async getByEmail(email: string): Promise<UserDocument> {
    return await UserModel.findOne({ email: email.trim() }) as UserDocument;
  }

  async create(email: string, password: string, admin?: boolean): Promise<UserDocument> {

    const user = new UserModel();
    user.email = email;
    user.password = password;
    user.admin = !!admin;

    return await user.save();
  }

  async isRegistered(email: string) {
    const count = await UserModel.count({ email });
    return (count > 0);
  }
}