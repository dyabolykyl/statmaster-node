import { Document } from '../db/Document';
import { User } from 'statmaster-common';

export interface UserDocument extends Document {
  email: string;
  password: string;
  admin: boolean;
  isLoggedIn: boolean;
  createdAt: Date;
  modifiedAt: Date;

  comparePassword(password: string): boolean;
}

export function toClientUser(userDoc: UserDocument): User {
  return {
    id: userDoc.id,
    email: userDoc.email,
    admin: userDoc.admin
  };
}
