import * as config from '../config';
import * as jwt from 'jsonwebtoken';

import { ExtractJwt, Strategy, StrategyOptions, VerifiedCallback } from 'passport-jwt';

import TYPES from '../inversify/types';
import { UserDocument } from '../user/UserDocument';
import { UserService } from '../user/UserService';
import container from '../inversify/container';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

export interface JWTPayload {
  id: string;
}

const jwtOptions: StrategyOptions = {  
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwtSecret,
};

const userService: UserService = container.get(TYPES.UserService);
export const jwtStrategy = new Strategy(
  jwtOptions,
  async (payload: JWTPayload, done: VerifiedCallback) => {
    log.debug(`payload received: ${payload}`);
    try {
      const user: UserDocument = await userService.getById(payload.id);

      if (!user) {
        const error = new Error('Unknown user');
        return done(error);
      }

      return done(null, user);

    } catch (err) {
      return done(err);
    }
  }
);

export function generateToken(payload: JWTPayload) {  
  return jwt.sign(payload, config.jwtSecret, {
    expiresIn: 10080 // in seconds
  });
}
