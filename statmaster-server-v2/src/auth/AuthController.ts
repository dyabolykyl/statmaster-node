import * as emailValidator from 'email-validator';
import * as passport from 'passport';

import { NextFunction, Request, Response } from 'express';
import { UserDocument, toClientUser } from '../user/UserDocument';
// import { JWTPayload, generateToken } from './jwt';
import { controller, httpPost, next, request, requestBody, response } from 'inversify-express-utils';

import TYPES from '../inversify/types';
import { UserService } from '../user/UserService';
import { clientError } from '../utils/ClientError';
import { getLoggerForModule } from '../utils/log';
import { inject } from 'inversify';

const log = getLoggerForModule(module);

const WHITELISTED_EMAILS = [
  'zack.brandes@gmail.com',
  'aaron.vlpn@gmail.com'
];

const ADMIN_EMAILS = [
  'zack.brandes@gmail.com',
];

@controller('/auth')
export class AuthController {

  private readonly userService: UserService;

  constructor( @inject(TYPES.UserService) userService: UserService) {
    this.userService = userService;
  }

  @httpPost('/register')
  public async register( @requestBody() body: any, @response() res: Response, @next() nextFunction: NextFunction) {
    log.debug(`Login request: ${Object.getOwnPropertyNames(body)}`);
    const email = body.email;
    const password = body.password;

    if (!email || !password) {
      throw clientError(400, 'Missing email or password');
    }

    const alreadyRegistered = await this.userService.isRegistered(email);
    if (alreadyRegistered) {
      throw clientError(409, 'Email already registered');
    }

    if (!emailValidator.validate(email)) {
      throw clientError(400, 'Invalid email');
    }

    let admin = false;
    if (process.env.process === 'production') {
      if (WHITELISTED_EMAILS.indexOf(email) < 0) {
        throw clientError(401, 'Access denied');
      }

      admin = ADMIN_EMAILS.indexOf(email) >= 0;
    }

    // TODO: validate password
    const user = await this.userService.create(body.email, body.password, admin);
    return {
      id: user.id
    };
  }

  @httpPost('/login', passport.authenticate('local'))
  public async login( @requestBody() body: any, @request() req: Request) {
    log.debug(`Login request: ${Object.getOwnPropertyNames(body)}`);

    const user: UserDocument = await req.user;
    log.debug(JSON.stringify(user));
    return toClientUser(user);
  }

  @httpPost('/logout')
  public async logout( @request() req: Request, @response() res: Response) {
    log.debug(`Logout request`);

    req.logout();
    return {};
  }
}
