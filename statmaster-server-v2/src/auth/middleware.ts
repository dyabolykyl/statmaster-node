import { NextFunction, Request, Response } from 'express';

import { UserDocument } from '../user/UserDocument';
import { clientError } from '../utils/ClientError';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

// route middleware to make sure a user is logged in
export function requireLogin(req: Request, res: Response, next: NextFunction) {

  log.debug(`Checking login`);
  // if user is authenticated in the session, carry on 
  if (req.isAuthenticated()) {
    log.debug('User is authenticated');
    return next();
  }

  // if they aren't redirect them to the home page
  throw clientError(401, 'Unauthenticated');
}

export async function requireCorrectUser(req: Request, userId: string): Promise<UserDocument> {
  if (!req.user) {
    throw clientError(401, 'Unauthenticated');
  }
  
  const user = await req.user;
  if (user.id !== userId)  {
    throw clientError(401, 'Unauthenticated');
  }
  return user;
}

export async function requireAdmin(req: Request) {
  log.debug(`Checking admin`);
  if (!req.user) {
    throw clientError(401, 'Unauthenticated');
  }

  const user = await req.user;
  log.debug(`Found ${user}`);
  if (user.admin) {
    log.debug(`User ${user.email} is an admin`);
    return true;
  }
  
  log.warn(`User ${user.email} is not an admin`);
  return false;
}