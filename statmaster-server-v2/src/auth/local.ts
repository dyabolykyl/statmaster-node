import { Strategy } from 'passport-local';
import TYPES from '../inversify/types';
import { UserDocument } from '../user/UserDocument';
import { UserService } from '../user/UserService';
import { clientError } from '../utils/ClientError';
import container from '../inversify/container';
import { getLoggerForModule } from '../utils/log';

const log = getLoggerForModule(module);

const userService: UserService = container.get(TYPES.UserService);
export const localStrategy = new Strategy(
  {
    usernameField: 'email'
  },
  async (email, password, done) => {
    log.debug('Running local strategy');
    try {
      const user: UserDocument = await userService.getByEmail(email);

      if (!user) {
        const error = clientError(401, 'Unknown email');
        return done(error);
      }

      if (!user.comparePassword(password)) {
        const err = clientError(401, 'Invalid password');
        return done(err);
      }
  
      return done(null, user);
    } catch (err) {
      return done(err);
    }
  }
);

export function serializeUser(user: any, cb: any) {
  cb(null, user.id);
}

export async function deserializeUser(id: string, cb: any) {
  try {
    const user = userService.getById(id);
    if (!user) {
      cb(clientError(401, 'Unknown user'));
    }
    cb(null, user);
  } catch (err) {
    cb(err);
  }
}
