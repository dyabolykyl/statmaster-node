import { Team } from 'statmaster-common';

function makeTeam(city: string, name: string, abbreviation: string): Team {
  return {
    id: 'blank',
    city,
    name,
    abbreviation
  };
}

export const TEAMS = [
  // AL East
  makeTeam('New York', 'Yankees', 'NYY'),
  makeTeam('Boston', 'Red Sox', 'BOS'),
  makeTeam('Baltimore', 'Orioles', 'BAL'),
  makeTeam('Tampa Bay', 'Rays', 'TB'),
  makeTeam('Toronto', 'Blue Jays', 'TOR'),

  // AL Central
  makeTeam('Cleveland', 'Indians', 'CLE'),
  makeTeam('Detroit', 'Tigers', 'DET'),
  makeTeam('Kansas City', 'Royals', 'KC'),
  makeTeam('Minnesota', 'Twins', 'MIN'),
  makeTeam('Chicago', 'White Sox', 'CWS'),

  // AL West
  makeTeam('Houston', 'Astros', 'HOU'),
  makeTeam('Oakland', 'Athletics', 'OAK'),
  makeTeam('Los Angeles', 'Angels', 'LAA'),
  makeTeam('Seattle', 'Mariners', 'SEA'),
  makeTeam('Texas', 'Rangers', 'TEX'),

  // NL East
  makeTeam('New York', 'Mets', 'NYM'),
  makeTeam('Atlanta', 'Braves', 'ATL'),
  makeTeam('Philadelphia', 'Phillies', 'PHI'),
  makeTeam('Washington', 'Nationals', 'WAS'),
  makeTeam('Miami', 'Marlins', 'MIA'),

  // AL Central
  makeTeam('Cincinnati', 'Reds', 'CIN'),
  makeTeam('Milwaukee', 'Brewers', 'MIL'),
  makeTeam('Pittsburgh', 'Pirates', 'PIT'),
  makeTeam('St. Louis', 'Cardinals', 'STL'),
  makeTeam('Chicago', 'Cubs', 'CHC'),

  // AL West
  makeTeam('Arizona', 'Diamondbacks', 'ARI'),
  makeTeam('Colorado', 'Rockies', 'COL'),
  makeTeam('Los Angeles', 'Dodgers', 'LAD'),
  makeTeam('San Diego', 'Padres', 'SD'),
  makeTeam('San Fransisco', 'Giants', 'SF'),

];
