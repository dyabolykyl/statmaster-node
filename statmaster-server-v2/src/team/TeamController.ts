import { controller, httpGet, httpPost } from 'inversify-express-utils';

import { DataUpdater } from '../updater/Updater';
import TYPES from '../inversify/types';
import { TeamService } from './TeamService';
import { getLoggerForModule } from '../utils/log';
import { inject } from 'inversify';
import { toClientTeam } from './TeamDocument';

const log = getLoggerForModule(module);

@controller('/api/v1/teams')
export class TeamController {

  private readonly teamService: TeamService;
  private readonly updater: DataUpdater;

  constructor(@inject(TYPES.TeamService) teamService: TeamService,
              @inject(TYPES.Updater) updater: DataUpdater) {
    this.teamService = teamService;
    this.updater = updater;
  }

  @httpGet('/')
  public async getAll() {
    const teams = await this.teamService.getTeams();
    log.debug(`Teams: ${teams}`);
    return teams.map(toClientTeam);
  }

  @httpPost('/update')
  public async updateTeams() {
    return await this.updater.updateTeams();
  }
}