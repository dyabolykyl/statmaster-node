import { Team, mockTeams } from 'statmaster-common';

import { TeamDocument } from './TeamDocument';
import { TeamModel } from './TeamModel';
import { getLoggerForModule } from '../utils/log';
import { injectable } from 'inversify';

const log = getLoggerForModule(module);

export interface TeamService {
  getTeams(): Promise<TeamDocument[]>;
  updateTeam(team: Team): Promise<TeamDocument | null>;
  getTeamId(abbreviation: string): Promise<string | null>;
}

@injectable()
export class MongoTeamService implements TeamService {

  getTeams(): Promise<TeamDocument[]> {
    return TeamModel.find().exec();
  }

  updateTeam(team: Team): Promise<TeamDocument | null> {
    const query = { abbreviation: team.abbreviation };
    const update = team;
    const options = {
      upsert: true,
      new: true,
      runValidators: true
    };
    log.debug(`Updating ${team.abbreviation}`);
    return TeamModel.findOneAndUpdate(query, update, options).exec();
  }

  getTeamId = async (abbreviation: string) => {
    log.debug(`Finding team id for ${abbreviation}`);
    const team = await TeamModel.findOne({ abbreviation }).select('_id');
    return team ? team.id : null;
  }

}

@injectable()
export class StaticTeamService implements TeamService {

  static toTeamDocument(team: Team): TeamDocument {
    return team as TeamDocument;
  }

  public getTeams = (): Promise<TeamDocument[]> => {
    return Promise.resolve(mockTeams.map(StaticTeamService.toTeamDocument));
  }

  updateTeam(team: Team) {
    return Promise.resolve(StaticTeamService.toTeamDocument(team));
  }

  getTeamId(abbreviation: string) {
    return Promise.resolve(null);
  }

}
