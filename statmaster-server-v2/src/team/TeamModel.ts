import * as mongoose from 'mongoose';

import { Model, Schema } from 'mongoose';

import { TeamDocument } from './TeamDocument';

const TeamSchema: Schema = new Schema (
  {
    name: {
      type: String,
      required: true,
    },

    city: {
      type: String,
      required: true
    },

    abbreviation: {
      type: String,
      required: true,
      match: /[A-Z]/,
      index: { unique: true }
    }
  },
  {
    timestamps: true
  }
);

export const TeamModel: Model<TeamDocument> = mongoose.model<TeamDocument>('team', TeamSchema);
