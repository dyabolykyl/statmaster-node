import { Document } from '../db/Document';
import { Team } from 'statmaster-common';

export interface TeamDocument extends Document {
  city: string;
  name: string;
  abbreviation: string;
  // sport: string
  // league: string
  // division: string
}

export function toClientTeam(team: TeamDocument): Team {
  return {
    id: team.id,
    city: team.city,
    abbreviation: team.abbreviation,
    name: team.name
  };
}
