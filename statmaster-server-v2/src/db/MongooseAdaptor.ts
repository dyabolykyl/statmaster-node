import * as config from '../config';
import * as mongoose from 'mongoose';

import { DbAdaptor } from './index';
import { getLoggerForModule } from '../utils/log';
import { injectable } from 'inversify';

const log = getLoggerForModule(module);

(mongoose as any).Promise = global.Promise;

export function objectId(id: string): mongoose.Types.ObjectId {
  return new mongoose.Types.ObjectId(id);
}

@injectable()
export class MongooseAdaptor implements DbAdaptor {
  private mongooseInstance: mongoose.MongooseThenable;
  private mongooseConnection: mongoose.Connection;

  connect(): Promise<any> {
    return new Promise(this.doConnect);
  }

  disconnect(): Promise<void> {
    return new Promise((resolve, reject) => {
      mongoose.disconnect().then(() =>
        this.mongooseConnection.close(() => {
          log.info('Mongoose default connection closed.');
          resolve();
        }));
    });
  }

  singleConnect(): Promise<any> {
    return new Promise(this.doSingleConnect);
  }

  private doSingleConnect = (resolve: () => void, reject: () => any) => {
    if (this.mongooseInstance) {
      resolve();
      return this.mongooseInstance;
    }

    let connectionString = config.mongoUrl;
    this.mongooseConnection = mongoose.connection;

    this.mongooseConnection.once('open', () => {
      log.info('Connect to mongodb is opened.');
    });

    this.mongooseInstance = mongoose.connect(connectionString, { useMongoClient: true });

    this.mongooseConnection.on('connected', () => {
      log.info('Mongoose default connection open to ' + connectionString);
      resolve();
    });

    // If the connection throws an error
    this.mongooseConnection.on('error', (msg) => {
      log.info('Mongoose default connection message:', msg);
      reject();
    });

    // If the Node process ends, close the Mongoose connection
    process.on('SIGINT', () => {
      this.mongooseConnection.close(() => {
        log.info('Mongoose default connection disconnected through app termination.');
        process.exit(0);
      });
    });

    return this.mongooseInstance;
  }

  private doConnect = (resolve: () => void, reject: () => any) => {
    if (this.mongooseInstance) {
      resolve();
      return this.mongooseInstance;
    }

    let connectionString = config.mongoUrl;
    this.mongooseConnection = mongoose.connection;

    this.mongooseConnection.once('open', () => {
      log.info('Connect to mongodb is opened.');
    });

    this.mongooseInstance = mongoose.connect(connectionString, { useMongoClient: true });

    this.mongooseConnection.on('connected', () => {
      log.info('Mongoose default connection open to ' + connectionString);
      resolve();
    });

    // If the connection throws an error
    this.mongooseConnection.on('error', (msg) => {
      log.info('Mongoose default connection message:', msg);
      reject();
    });

    // When the connection is disconnected
    this.mongooseConnection.on('disconnected', () => {
      setTimeout(() => this.mongooseInstance = mongoose.connect(connectionString), 5000);
      log.info('Mongoose default connection disconnected.');
      reject();
    });

    // When the connection is reconnected
    this.mongooseConnection.on('reconnected', () => {
      log.info('Mongoose default connection is reconnected.');
    });

    // If the Node process ends, close the Mongoose connection
    process.on('SIGINT', () => {
      this.mongooseConnection.close(() => {
        log.info('Mongoose default connection disconnected through app termination.');
        process.exit(0);
      });
    });

    return this.mongooseInstance;
  }

}
