import * as mongoose from 'mongoose';

/**
 * Defines common fields for all documents
 */
export interface Document extends mongoose.Document {
  createdAt: Date;
  modifiedAt: Date;
}