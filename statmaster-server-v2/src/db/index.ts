export interface DbAdaptor {
  connect(): Promise<any>;
  disconnect(): Promise<void>;
  singleConnect(): Promise<any>;
}
