import { BaseHttpController } from 'inversify-express-utils';

export class BaseController extends BaseHttpController {

  protected isAuthenticated(): Promise<Boolean> {
    return this.httpContext.user && this.httpContext.user.isAuthenticated();
  }

  protected async isCorrectUser(id: string): Promise<boolean> {
    const authenticated = await this.isAuthenticated();
    if (!authenticated) {
      return false;
    }

    return this.httpContext.user.isResourceOwner(id);
  }

}