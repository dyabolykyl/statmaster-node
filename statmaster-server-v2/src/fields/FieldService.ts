import { BATTING_FIELDS, Field, INFO_FIELDS } from 'statmaster-common';

import { getLoggerForModule } from '../utils/log';
import { injectable } from 'inversify';

const log = getLoggerForModule(module);

export interface FieldService {
  getFields(): Promise<Field[]>;
}

@injectable()
export class StaticFieldService implements FieldService {

  private readonly fields: Map<string, Field> = new Map();

  constructor() {
    INFO_FIELDS.forEach((value, key) => this.fields.set(key, value as Field));
    BATTING_FIELDS.forEach((value, key) => this.fields.set(key, value as Field));
    log.debug(`Initialized fields repository: ${Array.from(this.fields.keys())}`);
  }

  getFields = (): Promise<Field[]> => {
    return Promise.resolve(Array.from(this.fields.values()));
  }
}
