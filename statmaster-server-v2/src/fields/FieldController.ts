import { controller, httpGet, } from 'inversify-express-utils';

import { Field, } from 'statmaster-common';
import { FieldService } from './FieldService';
import TYPES from '../inversify/types';
import { getLoggerForModule } from '../utils/log';
import { inject } from 'inversify';

const log = getLoggerForModule(module);

@controller('/api/v1/fields')
export class FieldController {

  @inject(TYPES.FieldService) private fieldService: FieldService;

  @httpGet('/')
  async getFields(): Promise<Field[]> {
    log.trace(`GET fields`);
    return this.fieldService.getFields();
  }
}
