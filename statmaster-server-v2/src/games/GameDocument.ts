import { Game, Side } from 'statmaster-common';
import { LocalDate, LocalTime, nativeJs } from 'js-joda';

import { Document } from '../db/Document';

export interface GameDocument extends Document {
  msfId: string;
  date: Date;
  time: string;
  home: Side;
  away: Side; 
}

export function toClientGame(gameDoc: GameDocument): Game {
  return {
    id: gameDoc.id,
    date: LocalDate.from(nativeJs(gameDoc.date)),
    time: LocalTime.parse(gameDoc.time),
    home: gameDoc.home,
    away: gameDoc.away
  };
}
