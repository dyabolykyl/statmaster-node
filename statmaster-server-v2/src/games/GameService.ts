import * as prettyjson from 'prettyjson';

import { Side, toDate } from 'statmaster-common';

import { GameDocument } from './GameDocument';
import { GameInfo } from '../updater/types';
import { GameModel } from './GameModel';
import { LocalDate } from 'js-joda';
import { getLoggerForModule } from '../utils/log';
import { injectable } from 'inversify';

const log = getLoggerForModule(module);

export interface GameService {
  getGames(date: LocalDate): Promise<GameDocument[]>;
  getGame(msfId: string): Promise<GameDocument | null>;
  updateGames(games: GameInfo[]): Promise<(GameDocument | null)[]>;
  updateGame(game: GameInfo): Promise<GameDocument | null>;
  deleteAllGames(): Promise<any>;
}

@injectable()
export class MongoGameService implements GameService {

  getGames = async (date: LocalDate): Promise<GameDocument[]> => {
    return GameModel.find({ date: toDate(date) }).exec();
  }

  getGame = (msfId: string) => {
    return GameModel.findOne({ msfId }).exec();
  }

  updateGames = async (games: GameInfo[]) => {
    const updates = await Promise.all(games.map(async gameInfo => {
      const game = await this.updateGame(gameInfo);
      log.debug(`Updated game:\n${prettyjson.render(gameInfo)}`);
      return game;
    }));
    log.debug(`Updated ${games.length} games`);
    return updates;
  }

  updateGame = (game: GameInfo) => {
    log.debug(`Finding or creating game ${game.msfId}: ${game.home} @ ${game.away}`);
    const query = { msfId: game.msfId };
    const homeSide: Side = {
      team: game.home
    };
    const awaySide: Side = {
      team: game.away
    };
    const update = {
      msf: game.msfId,
      date: toDate(game.date),
      time: game.time.toString(),
      home: homeSide,
      away: awaySide
    };
    const options = {
      new: true,
      upsert: true
    };

    return GameModel.findOneAndUpdate(query, { $setOnInsert: update }, options ).exec();
  }

  deleteAllGames() {
    log.debug(`Deleting all games`);
    return GameModel.remove({}).exec();
  }
}
