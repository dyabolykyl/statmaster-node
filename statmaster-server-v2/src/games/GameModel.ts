import * as mongoose from 'mongoose';

import { Model, Schema } from 'mongoose';

import { GameDocument } from './GameDocument';

const SideSchema: Schema = new Schema(
  {
    team: {
      type: Schema.Types.ObjectId,
      ref: 'team',
    }
    // TODO: pitcher, lineup
  }
);

const GameSchema: Schema = new Schema(
  {
    msfId: {
      type: String,
      required: true,
      index: { unique: true }
    },

    date: {
      type: Date,
      required: true
    },

    time: {
      type: String,
      required: true,
    },

    home: SideSchema,

    away: SideSchema,

    // TODO: Decide if num is needed
  },
  {
    timestamps: true
  }
);

export const GameModel: Model<GameDocument> = mongoose.model<GameDocument>('game', GameSchema);
