import { controller, httpGet, queryParam } from 'inversify-express-utils';

import { Game } from 'statmaster-common';
import { GameService } from './GameService';
import { LocalDate } from 'js-joda';
import TYPES from '../inversify/types';
import { clientError } from '../utils/ClientError';
import { getLoggerForModule } from '../utils/log';
import { inject } from 'inversify';
import { toClientGame } from './GameDocument';

const log = getLoggerForModule(module);

@controller('/api/v1/games')
export class GameController {

  @inject(TYPES.GameService) private gameService: GameService;

  @httpGet('/')
  async getGames(@queryParam('date') dateParam: string): Promise<Game[]> {
    log.trace(`GET Games for ${dateParam}`);

    let date;
    if (dateParam === undefined) {
      throw clientError(400, `Missing parameter 'date'`);
    }
    try {
      date = LocalDate.parse(dateParam);
    } catch (err) {
      throw clientError(400, `Invalid date: ${dateParam}`);
    }

    const games = await this.gameService.getGames(date);
    return games.map(toClientGame);
  }
}
