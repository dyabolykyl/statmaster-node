import * as dotenv from 'dotenv';

dotenv.config();

function read(name: string): string {
  const value = process.env[name];
  if (value === undefined) {
    throw new Error(`Missing config: ${name}`);
  }
  return value;
}

// tslint:disable no-string-literal
export const port = process.env.PORT || 3000;
export const mongoUrl = read('MONGODB_URI');
export const mySportsFeedUsername = read('MYSPORTSFEED_USERNAME');
export const mySportsFeedPassword = read('MYSPORTSFEED_PASSWORD');
export const jwtSecret = read('JWT_SECRET');
export const sessionSecret = read('SESSION_SECRET');

export const corsOrigin = process.env.NODE_ENV === 'production'
  ? `https://statmaster.herokuapp.com`
  : `http://localhost:3000`;