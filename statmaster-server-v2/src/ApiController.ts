import { controller, httpGet } from 'inversify-express-utils';

@controller('/api/v1')
export class ApiController {

  @httpGet('/')
  get() {
    return 'Welcome to Statmaster!';
  }

}