import * as express from 'express';
import * as path from 'path';
import * as prettyjson from 'prettyjson';

import container from './inversify/container';
import { InversifyExpressServer, getRouteInfo } from 'inversify-express-utils';

import { App } from './App';
import { DbAdaptor } from './db/index';
import { Router } from 'express';
import TYPES from './inversify/types';
import * as passport from 'passport';
import { localStrategy, serializeUser, deserializeUser } from './auth/local';
import * as config from './config';

process.on('warning', e => console.warn(e.stack));

// Create an app instance
const router = Router();
const currentWorkingDirectory = process.cwd();
const publicPath = path.join(currentWorkingDirectory, '../statmaster-client-v2/build/');
// load client assets
router.use('/', express.static(publicPath));

// set authentication method
// passport.use(jwtStrategy);
passport.use(localStrategy);
passport.serializeUser(serializeUser);
passport.deserializeUser(deserializeUser);

const server = new InversifyExpressServer(container, router);
const dbAdaptor: DbAdaptor = container.get(TYPES.DbAdaptor);
const statmaster = new App(server, dbAdaptor, config.port);

// start the server
statmaster.configure();

const routeInfo = getRouteInfo(container);
console.log(prettyjson.render({ routes: routeInfo}));

statmaster.start();
