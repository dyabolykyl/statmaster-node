import 'reflect-metadata';
import '../team/TeamController';
import '../ApiController';
import '../stats/StatGroupController';
import '../fields/FieldController';
import '../games/GameController';
import '../auth/AuthController';
import '../user/UserController';

import { Container, interfaces } from 'inversify';
import { DataUpdater, Updater } from '../updater/Updater';
import { Fetcher, MySportsFeed, MySportsFeedFetcher } from '../updater/Fetcher';
import { FieldService, StaticFieldService } from '../fields/FieldService';
import { GameService, MongoGameService } from '../games/GameService';
import { MongoStatsService, StatsService } from '../stats/StatsService';
import { MongoTeamService, TeamService } from '../team/TeamService';
import { MongoUserService, UserService } from '../user/UserService';

import { DbAdaptor } from '../db/index';
import { MongooseAdaptor } from '../db/MongooseAdaptor';
import { Orchestrator } from '../updater/Orchestrator';
import { RequestSender } from '../updater/mysportsfeed/RequestSender';
import TYPES from './types';

const container = new Container();

container.bind<DbAdaptor>(TYPES.DbAdaptor).to(MongooseAdaptor).inSingletonScope();

container.bind<TeamService>(TYPES.TeamService).to(MongoTeamService).inSingletonScope();

container.bind<StatsService>(TYPES.StatsService).to(MongoStatsService).inSingletonScope();

container.bind<FieldService>(TYPES.FieldService).to(StaticFieldService).inSingletonScope();

container.bind<GameService>(TYPES.GameService).to(MongoGameService).inSingletonScope();

container.bind<UserService>(TYPES.UserService).to(MongoUserService).inSingletonScope();

container.bind<Fetcher>(TYPES.Fetcher).to(MySportsFeedFetcher).inSingletonScope();
container.bind<DataUpdater>(TYPES.Updater).to(Updater).inSingletonScope();
container.bind<Orchestrator>(TYPES.Orchestrator).to(Orchestrator).inSingletonScope();
container.bind<interfaces.Factory<Object>>(TYPES.MySportsFeedFactory)
  .toFactory<MySportsFeed>((context: interfaces.Context) => () => new RequestSender());
  // .toFactory<Object>((context: interfaces.Context) => () => new MySportsFeed('1.0', true, null, null));

export default container;
