
const TYPES = {
  App: Symbol.for('App'),
  DbAdaptor: Symbol.for('DbAdaptor'),

  UserService: Symbol.for('UserService'),
  TeamService: Symbol.for('TeamService'),
  StatsService: Symbol.for('StatGroupService'),
  FieldService: Symbol.for('FieldService'),
  GameService: Symbol.for('GameService'),

  // updater
  Fetcher: Symbol.for('Fetcher'),
  Updater: Symbol.for('DataUpdater'),
  Orchestrator: Symbol.for('Orchestrator'),
  MySportsFeedFactory: Symbol.for('MySportsFeed'),
  
};

export default TYPES;
