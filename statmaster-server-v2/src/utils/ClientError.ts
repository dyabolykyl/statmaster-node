
export interface ClientError {
  status: number;
  message: string;
}

export function clientError(status: number, message: string): Error {
  const error = new Error(message) as any;
  error.clientError = {
    status,
    message
  };
  return error;
}
