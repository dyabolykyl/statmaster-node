import { configure, getLogger, Logger } from 'log4js';

const logMap = new Map();

function logger(name: string): Logger {

  if (logMap.has(name)) {
    return logMap.get(name);
  }

  const log = getLogger(name);

  configure({
    appenders: {
      out: { type: 'stdout' },
      app: { type: 'file', filename: 'application.log' }
    },
    categories: {
      default: { appenders: [ 'out', 'app' ], level: 'debug' }
    }
  });

  // no logging in tests
  // let TEST_LOGGING = false; // TODO: make this an env variable

  logMap.set(name, log);
  return log;

}

export function getLoggerForModule(moduleName: any): Logger {
  const id: string = moduleName.id;
  return logger(id.replace(/.*(dist|src)\//, ''));
}
