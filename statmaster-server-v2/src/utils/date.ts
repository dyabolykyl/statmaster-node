import { LocalDate } from 'js-joda';
import { clientError } from './ClientError';

export function parseDate(date: string): LocalDate {
  if (!date) {
    throw clientError(400, `Date missing`);
  }
  
  try {
    return LocalDate.parse(date);
  } catch (err) {
    throw clientError(400, `Invalid date: ${date}`);
  }
}