import { DataUpdater } from '../src/updater/Updater';

export function arrangeUpdater: DataUpdater {
  return {
    updateTeams: jest.fn(() => Promise.resolve()),
    updateAggregateTeamMatchupStats: jest.fn(() => Promise.resolve()),
    updateDailyStatsByGames: jest.fn(() => Promise.resolve()),
    updateAggregateTeamStats: jest.fn(() => Promise.resolve()),
    updateDailyStatsByTeams: jest.fn(() => Promise.resolve()),
    deleteAllStats: jest.fn(() => Promise.resolve()),
  };
}