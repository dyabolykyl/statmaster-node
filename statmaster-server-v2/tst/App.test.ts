import * as container from '../src/inversify/container';
import * as request from 'supertest';

import { GameService, StaticGameService } from '../src/games/GameService';
import { INFO_FIELDS, MOCK_GAMES } from 'statmaster-common';
import { StaticStatsService, StatsService } from '../src/stats/StatsService';
import { StaticTeamService, TeamService } from '../src/team/TeamService';
import {
   mockStatGroups,
   mockTeams,
   statGroupResponseToStatGroup,
   statGroupToResponse
} from 'statmaster-common';

import { App } from '../src/App';
import { DbAdaptor } from '../src/db/index';
import { InversifyExpressServer } from 'inversify-express-utils';
import TYPES from '../src/inversify/types';

describe('App', () => {
  let app: App;
  beforeAll(() => {
    container.default.rebind<TeamService>(TYPES.TeamService).to(StaticTeamService);
    container.default.rebind<StatsService>(TYPES.StatsService).to(StaticStatsService);
    container.default.rebind<GameService>(TYPES.GameService).to(StaticGameService);
    let dbAdaptor: DbAdaptor = {
      connect: jest.fn(() => Promise.resolve()),
      disconnect: jest.fn(),
      singleConnect: jest.fn()
    };
    app = new App(new InversifyExpressServer(container.default), dbAdaptor, 1);
    app.configure();
  });

  test('GET client error', () => {
    return request(app.expressApp)
        .post('/api/v1/stats/query')
        .send({date: 'abc'})
        .then(response => {
      expect(response.status).toBe(400);
      expect(response.text).toBeDefined();
    });
  });

  test('GET /api/v1', () => {
    return request(app.expressApp).get('/api/v1').then(response => {
      expect(response.status).toBe(200);
      expect(response.type).toBe('text/html');
      expect(response.text).toBe('Welcome to Statmaster!');
    });
  });

  test('GET /api/v1/fields', () => {
    return request(app.expressApp).get('/api/v1/fields').then(response => {
      expect(response.status).toBe(200);
      expect(response.type).toBe('application/json');
      expect(response.body[0]).toEqual(Array.from(INFO_FIELDS.values())[0]);
    });
  });

  test('GET /api/v1/teams', () => {
    return request(app.expressApp).get('/api/v1/teams').then(response => {
      expect(response.status).toBe(200);
      expect(response.type).toBe('application/json');
      expect(response.body).toEqual(mockTeams);
    });
  });

  test('GET /api/v1/stats/query', () => {
    return request(app.expressApp)
        .post('/api/v1/stats/query')
        .send({resources: ['1']})
        .then(response => {
      expect(response.status).toBe(200);
      expect(response.type).toBe('application/json');

      let expectedStatGroups = mockStatGroups.filter(s => s.resource === '1');
      let expectedResponse = expectedStatGroups.map(statGroupToResponse);

      expect(response.body).toEqual(expectedResponse);
      expect(response.body.map(statGroupResponseToStatGroup)).toEqual(expectedStatGroups);
    });
  });

  test('GET /api/v1/games[date]', () => {
    return request(app.expressApp).get('/api/v1/games?date=2017-04-08').then(response => {
      expect(response.status).toBe(200);
      expect(response.type).toBe('application/json');
      expect(response.body).toEqual(MOCK_GAMES.map(g => JSON.parse(JSON.stringify(g))));
    });
  });

  test('GET /api/v1/games[unkown date]', () => {
    return request(app.expressApp).get('/api/v1/games?date=2017-12-01').then(response => {
      expect(response.status).toBe(200);
      expect(response.type).toBe('application/json');
      expect(response.body).toHaveLength(0);
    });
  });
});
