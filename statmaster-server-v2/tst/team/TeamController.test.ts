import 'reflect-metadata';

import { DataUpdater } from '../../src/updater/Updater';
import { TeamController } from '../../src/team/TeamController';
import { TeamService } from '../../src/team/TeamService';
import { arrangeUpdater } from '../mocks';
import { mockTeams } from 'statmaster-common';

describe('Teams', () => {
  let teamService: TeamService;
  let updater: DataUpdater;

  beforeEach(() => {
    teamService = {
      getTeams: jest.fn(() => Promise.resolve(mockTeams)),
      updateTeam: jest.fn(() => mockTeams[0]),
      getTeamId: jest.fn(() => mockTeams[0].id)
    };
    updater = arrangeUpdater();
  });

  test('GET /', () => {
    const sut = new TeamController(teamService, updater);
    return sut.getAll().then(teams => {
      expect(teams).toEqual(mockTeams);
      expect(teamService.getTeams).toHaveBeenCalledTimes(1);
    });
  });

  test('POST /update', () => {
    const sut = new TeamController(teamService, updater);
    return sut.updateTeams().then(() => {
      expect(updater.updateTeams).toHaveBeenCalledTimes(1);
    });
  });
});
