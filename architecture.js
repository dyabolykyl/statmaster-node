UI = {
  navigation: {
    sport: {
      stats,
      selectors,
      picks,
      games,
      account
    }
  },
  header: [
    'STATMASTER',
    banner,
    color
  ],
  statsTable: {
    date,
    table: {
      resources: {
        name,
        markAsPick
      },
      matchups,
      stats: {
        team,
        batting,
        pitching,
        custom
      },
      columnOrder,
      columnVisibility,
      playing: [yes, no, maybe],
      sort,
      filters,
      hoverInfo: {
        roster,
        schedule
      }
    },
    tableConfig: {
      stats: category / stat / split,
      order,
      filters: [
        AND, OR, NOT
      ],
      sort,
      customStatDefinitions: {
        name,
        description,
        equation: {
          availableParameters,
          operators: ["/", "*", "+", "-", MAX, MIN, AVG],
        }
      }
    }
  },
  selectors: {
    listWithShortDesc,
    selector: {
      id,
      name,
      filters,
      sort,
      successCriteria: {
        stat,
        condition
      },
      [selections]: {
        date,
        statsTableSnapshot: {
          picks,
          stats,
          results: {
            success,
            daily_stats
          }
        },
      }
    }
  },
  picks: {
    dates,
    pick: {
      date,
      conditionOptional,
      stats
    }
  },
  games: {
    date,
    lineups: {
      home,
      away
    }
  },
  user: {
    login,
    friends,
  }
}

state = {
  UI: {
    columnOrder,
    visibileFields,
    
  },
  data: {
    fields: {
      type: {
        category: {
          field
        }
      }
    },
    statGroups: {
      id,
      type,
      stats: {
        category: [
          field => value
        ]
      }
    },
    selectors: {
      id,
      definition,
      data: {
        date,
        statGroupIds,
      }
    },
    picks: {
      id,
      date,
      successCriteria
    }
  }
}