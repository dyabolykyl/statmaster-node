import { Category, FieldKey, FieldValue, ResourceDataValue, Topic } from '../../stats/types';

import { FilterTree } from './FilterTree';
import { PLAYING_KEY } from './filtering';
import { isEqual } from 'lodash';
import { makeFieldKey } from '../../stats/fields/fields';

export type Filterable = Map<string, ResourceDataValue>;

export interface Filter {
  accept(container: Filterable): boolean;
}

export interface FilterNode extends Filter {
  parent?: FilterJoiner;
  remove(key: string): void;
  matchesKey(key: string): boolean;
  matchesField(fieldKey: FieldKey): boolean;
  toMap(map: Map<string, FilterNode>): void;
  clone(): FilterNode;
}

export abstract class AbstractFilterNode implements FilterNode {
  parent?: FilterJoiner;
  abstract accept(container: Filterable): boolean;
  remove(key: string): void {
    return;
  }
  abstract matchesKey(key: string): boolean;
  abstract matchesField(fieldKey: FieldKey): boolean;
  abstract toMap(map: Map<string, FilterNode>): void;
  abstract clone(): FilterNode;
}

export class FilterJoiner extends AbstractFilterNode  {
  joinType: FilterJoinerType;
  left: AbstractFilterNode;
  right: AbstractFilterNode;
  constructor(left: AbstractFilterNode, right: AbstractFilterNode, type: FilterJoinerType) {
    super();
    this.left = left;
    this.right = right;
    this.left.parent = this;
    this.right.parent = this;
    this.joinType = type;
  }

  accept(container: Filterable): boolean {
    if (!this.left || !this.right) {
      return false;
    }

    switch (this.joinType) {
      case FilterJoinerType.AND:
        return this.left.accept(container) && this.right.accept(container);
      case FilterJoinerType.OR:
        return this.left.accept(container) || this.right.accept(container);
      default:
        return true;
    }
  }

  remove(key: string): void {
    if (!this.parent) {
      return;
    }

    if (this.left.matchesKey(key)) {
      if (this.parent.left === this) {
        this.parent.left = this.right;
      } else {
        this.parent.right = this.right;
      }
    } else if (this.right.matchesKey(key)) {
      if (this.parent.left === this) {
        this.parent.left = this.left;
      } else {
        this.parent.right = this.left;
      }
    }
  }

  matchesKey(key: string) {
    return false;
  }

  matchesField(fieldKey: FieldKey) {
    return false;
  }

  toMap(map: Map<string, FilterNode>): void {
    this.left.toMap(map);
    this.right.toMap(map);
  }

  clone() {
    return new FilterJoiner(this.left.clone(), this.right.clone(), this.joinType);
  }
}

export enum FilterJoinerType {
  AND = '&&',
  OR = '||'
}

export enum FilterOperator {
  EQUAL_TO = '==',
  NOT_EQUAL_TO = '!=',
  GREATER_THAN = '>',
  LESS_THAN = '<'
}
export const FilterOperators = [
  FilterOperator.EQUAL_TO,
  FilterOperator.NOT_EQUAL_TO,
  FilterOperator.GREATER_THAN,
  FilterOperator.LESS_THAN
];

export enum DataType {
  'STRING',
  'NUMBER',
  'BOOLEAN'
}

export class FilterExpression extends AbstractFilterNode {
  fieldKey: FieldKey;
  dataType: DataType;
  operator: FilterOperator;
  value: FieldValue;
  key: string;
  constructor(fieldKey: FieldKey, operator: FilterOperator, value: FieldValue, dataType: DataType) {
    super();
    this.fieldKey = fieldKey;
    this.operator = operator;
    this.value = value;
    this.dataType = dataType;
    this.key = `${this.fieldKey.flatKey}${this.operator}${this.value}`;
  }

  accept(container: Filterable): boolean {
    let actualValue = container.get(this.fieldKey.flatKey);
    if (!actualValue) {
      return false;
    }

    switch (this.operator) {
      case FilterOperator.EQUAL_TO:
        return actualValue.value === this.value;
      case FilterOperator.NOT_EQUAL_TO:
        return actualValue.value !== this.value;
      case FilterOperator.GREATER_THAN:
        return actualValue.value > this.value;
      case FilterOperator.LESS_THAN:
        return actualValue.value < this.value;
      default:
        throw new Error(`Unknown filter operator: ${this.operator}`);
    }
  }

  matchesKey(key: string) {
    return key === this.key;
  }

  matchesField(fieldKey: FieldKey) {
    return isEqual(this.fieldKey, fieldKey);
  }

  toMap(map: Map<string, FilterNode>): void {
    map.set(this.fieldKey.flatKey, this);
  }

  clone(): FilterNode {
    return new FilterExpression(this.fieldKey, this.operator, this.value, this.dataType);
  }

  toString(): string {
    return `Filter: (${this.fieldKey} ${this.operator} ${this.value})`;
  }
}

export const isFilteredOnPlaying = (filterTree: FilterTree): boolean => {
  return filterTree.getFilterByField(PLAYING_KEY) !== undefined;
};