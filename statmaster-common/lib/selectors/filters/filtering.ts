import { Category, Topic } from '../../stats/types';
import { DataType, FilterExpression, FilterNode, FilterOperator, Filterable } from './types';
import { FieldKey, makeFieldKey } from '../../stats';

export const PLAYING_KEY: FieldKey = makeFieldKey(Topic.RESOURCE, Category.INFO, 'playing');

export const filterOnPlayingExpression = (filterOnPlaying: boolean): FilterExpression => {
  return new FilterExpression(PLAYING_KEY, FilterOperator.EQUAL_TO, filterOnPlaying, DataType.BOOLEAN);
};

export const EMPTY_FILTER: FilterNode = {
  accept(cotainer: Filterable): boolean {
    return true;
  },
  remove(key: string) {
    return;
  },
  matchesKey(key: string) {
    return false;
  },
  matchesField(fieldKey: FieldKey) {
    return false;
  },
  toMap(map: Map<string, FilterNode>) {
    return map;
  },
  clone(): FilterNode {
    return this;
  }
};