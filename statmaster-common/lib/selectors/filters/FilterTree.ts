import { FieldKey, ResourceDataValue } from '../../stats/types';
import { Filter, FilterExpression, Filterable } from './types';

export interface FilterTree extends Filter {
  clone(): FilterTree;
  addFilter(filter: FilterExpression): void;
  removeFilter(filterKey: string): void;
  removeFiltersForField(fieldKey: FieldKey): void;
  getFilterByField(fieldKey: FieldKey): FilterExpression | undefined;
  getFilters(): FilterExpression[];
}

export class FlatFilterTree implements FilterTree {
  private filters: Map<string, FilterExpression>;
  constructor(filters?: Map<string, FilterExpression>) {
    this.filters = filters || new Map();
  }

  addFilter = (filter: FilterExpression) => {
    this.filters.set(filter.key, filter);
  }

  removeFilter = (filterKey: string) => {
    this.filters.delete(filterKey);
  }

  removeFiltersForField = (fieldKey: FieldKey) => {
    this.filters.forEach((filter, key, map) => {
      if (filter.matchesField(fieldKey)) {
        map.delete(key);
      }
    });
  }

  getFilterByField = (fieldKey: FieldKey): FilterExpression | undefined => {
    return Array.from(this.filters.values()).find(filter => filter.matchesField(fieldKey));
  }

  getFilters = (): FilterExpression[] => {
    return Array.from(this.filters.values());
  }

  accept = (container: Filterable): boolean => {
    let accepts = true;
    this.filters.forEach((filter: FilterExpression, key: string) => {
      if (!filter.accept(container)) {
        accepts = false;
      }
    });
    return accepts;
  }

  clone = (): FilterTree => {
    let newFilters: Map<string, FilterExpression> = new Map();
    this.filters.forEach((filter, key) => {
      newFilters.set(key, filter);
    });
    return new FlatFilterTree(newFilters);
  }
}