import { DateTimeFormatter, LocalDate } from 'js-joda';
import { FilterExpression, FilterTree } from './filters';

import { ResourceId } from '../stats/types';
import { Sort } from './sort/types';

export type SelectorId = string;

export class Selector {
  readonly id: SelectorId;
  readonly name: string;
  readonly filterTree: FilterTree;
  readonly feature: Sort;
  readonly successCriteria?: FilterExpression;

  constructor(id: string, 
              name: string, 
              filterTree: FilterTree, 
              feature: Sort, 
              successCriteria?: FilterExpression) {
    this.id = id;
    this.name = name;
    this.filterTree = filterTree;
    this.feature = feature;
    this.successCriteria = successCriteria;
  }
}

export class SelectionList {
  readonly selectorId: SelectorId; 
  readonly selections: Map<string, Selection>;

  constructor(selectorId: SelectorId, selections: Selection[]) {
    this.selectorId = selectorId;
    this.selections = new Map();
    selections.forEach(selection => this.selections.set(selection.getDateKey(), selection));
  }

}

export function makeDateKey(date: LocalDate): string {
  return DateTimeFormatter.ofPattern('MMddyyyy').format(date);
}

export class Selection {
  date: LocalDate;
  picks: SelectionPick[];

  constructor(date: LocalDate, picks: SelectionPick[]) {
    this.date = date;
    this.picks = picks;
  }

  getDateKey() {
    return makeDateKey(this.date);
  }
}

export interface SelectionPick {
  resource: ResourceId;
  result?: boolean;
}
