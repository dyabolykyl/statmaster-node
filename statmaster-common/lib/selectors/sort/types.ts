import { FieldKey } from '../../stats/types';

export enum SortStyle {
  ASCENDING = 'asc',
  DESCENDING = 'desc'
}

export interface Sort {
  columnKey: FieldKey;
  style: SortStyle;
}