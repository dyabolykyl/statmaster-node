export * from './responses';

export function mapToObj(map: Map<string, any>): Object {
  const obj = Object.create(null);
  map.forEach((v, k) => {
    obj[k] = v;
  });
  return obj;
}

export function objToMap(obj: any): Map<string, any> {
  let map = new Map();
  for (let k of Object.keys(obj)) {
      map.set(k, obj[k]);
  }
  return map;
}
