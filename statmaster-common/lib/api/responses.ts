import { StatGroup } from '../stats';
import { mapToObj, objToMap } from './index';

export interface StatGroupResponse {
  id: string;
  resource: string;
  // maps can be represented as an object in JSON
  stats: Object;
}

export function statGroupToResponse(statGroup: StatGroup): StatGroupResponse {
  return {
    id: statGroup.id,
    resource: statGroup.resource,
    stats: mapToObj(statGroup.stats)
  };
}

export function statGroupResponseToStatGroup(statGroupResponse: StatGroupResponse): StatGroup {
  return {
    id: statGroupResponse.id,
    resource: statGroupResponse.resource,
    stats: objToMap(statGroupResponse.stats)
  };
}
