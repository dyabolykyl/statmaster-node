import * as moment from 'moment-timezone';

import { LocalDate, LocalDateTime, LocalTime } from 'js-joda';

import { Game } from './types';
import { Moment } from 'moment-timezone';
import { mockTeams } from '../stats/';

export const STATIC_DATE: LocalDateTime = LocalDateTime.of(LocalDate.of(2017, 4, 8), LocalTime.of(12, 0, 0));

export const MOCK_GAMES: Game[] = [
  {
    id: 'g1',
    date: STATIC_DATE.toLocalDate(),
    time: STATIC_DATE.toLocalTime(),
    home: {
      team: mockTeams[0].id,
      lineup: []
    },
    away: {
      team: mockTeams[2].id,
      lineup: [],
    }
  },
];
