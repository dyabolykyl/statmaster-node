import { LocalDate, LocalTime } from 'js-joda';
import { Resource, ResourceId } from '../stats/types';

export interface Side {
  team: ResourceId;
  pitcher?: ResourceId;
  lineup?: ResourceId[];
}

export interface Game extends Resource {
  date: LocalDate;
  time: LocalTime;
  home: Side;
  away: Side;
  num?: number; // Should always be 1, unless a double header
}
