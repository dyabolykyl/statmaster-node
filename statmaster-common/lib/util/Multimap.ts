export class Multimap<K, V> {
  private readonly map: Map<K, Set<V>>;

  constructor() {
    this.map = new Map();
  }

  put(key: K, value: V) {
    let set = this.map.get(key);
    if (set === undefined) {
      set = new Set();
      this.map.set(key, set);
    }

    set.add(value);
  }

  remove(key: K, value: V): void {
    const set = this.map.get(key);
    if (set === undefined) {
      return;
    }

    set.delete(value);

    if (set.size === 0) {
      this.map.delete(key);
    }
  }

  getAll(key: K): Set<V> {
    return this.map.get(key) || new Set();
  }

  forEach(fn: (values: Set<V>, key: K) => void): void {
    this.map.forEach((v, k) => fn(v, k));
  }

} 