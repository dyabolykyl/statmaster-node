import * as joda from 'js-joda';
import * as moment from 'moment-timezone';

import { LocalDate, LocalDateTime, nativeJs } from 'js-joda';

import { Moment } from 'moment-timezone';

export function toDate(val: LocalDate | LocalDateTime): Date {
  return joda.convert(val).toDate();
}

export function toMoment(val: LocalDate | LocalDateTime): Moment {
  return moment(toDate(val));
}

export function momentToJoda(val: Moment): LocalDateTime {
  return LocalDateTime.from(nativeJs(val));
}

export function dateToJoda(val: Date): LocalDateTime {
  return LocalDateTime.from(nativeJs(val));
}