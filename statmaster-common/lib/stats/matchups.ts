import { Resource, ResourceId } from './types';

export interface Matchup {
  resource: ResourceId;
  opponent: ResourceId;
}

export function makeMatchup(resource: ResourceId, opponent: ResourceId) {
  return {
    resource,
    opponent
  };
}

export function makeMatchupKey(matchup: Matchup) {
  return `${matchup.resource}_${matchup.opponent}`;
}

export function parseMatchupKey(matchupKey: string): Matchup {
  if (!matchupKey.match(/^[a-z0-9]+_[a-z0-9]+$/)) {
    throw new Error(`Invalid matchup key: ${matchupKey}`);
  }
  const parts = matchupKey.split('_');

  return {
    resource: parts[0],
    opponent: parts[1]
  };
}