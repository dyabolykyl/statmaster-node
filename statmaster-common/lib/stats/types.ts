export type ResourceId = string;

export interface Resource {
  id: ResourceId;
}

export interface ResourceDataValue {
  key: FieldKey;
  value: FieldValue;
}

export interface Team extends Resource {
  name: string;
  city: string;
  abbreviation: string;
}

export interface Player extends Resource {
  firstName: string;
  lastName: string;
  team: ResourceId;
  position: string; // TODO position enum
}

export type StatMap = Map<string, Stat>;

export interface StatGroup extends Resource {
  resource: ResourceId;
  stats: StatMap;
}

export interface Stat {
  key: FieldKey;
  value: number;
}

export enum Type {
  TEAM = 'team',
  PLAYER = 'player'
}

export enum Topic {
  RESOURCE = 'resource',
  OPPOSING = 'opposing',
  RESOURCE_VS_OPPOSING = 'resource_vs_opposing',
  OPPOSING_VS_RESOURCE = 'opposing_vs_resource'
}
export const Topics: Topic[] = [
  Topic.RESOURCE,
  Topic.OPPOSING,
  Topic.RESOURCE_VS_OPPOSING,
  Topic.OPPOSING_VS_RESOURCE
];

export enum Category {
  INFO = 'info',
  STANDINGS = 'standing',
  PITCHING = 'pitching',
  BATTING = 'batting',
  FIELDING = 'fielding',
  CUSTOM = 'custom'
}
export const Categories: Category[] = [
  Category.INFO,
  Category.BATTING,
  Category.PITCHING,
  Category.FIELDING,
  Category.CUSTOM
];

export type FieldId = string;

export interface FieldKey {
  topic: Topic;
  category: Category;
  fieldId: FieldId;
  flatKey: string;
}

export type FieldValue = number | string | boolean;

export interface Field {
  id: FieldId;
  category: Category;
  label: string;
  description: string;
  // TODO replace with equation
  derived?: boolean;
  negative?: boolean;
}

export interface DerivedFieldDefinition {
  id: FieldId;
  category: Category;
  equation: string;
  variables: string[];
}