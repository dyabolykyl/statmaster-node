import { Category, Field, FieldKey, Stat, StatGroup, Team, Topic } from './types';

import { BATTING_FIELDS } from './fields/batting';
import { INFO_FIELDS } from './fields/info';
import { Matchup } from './matchups';
import { makeFieldKey } from './fields';

export function makeObjectId(id: string): string {
  const padding = 12 - id.length;
  return id + Array(padding).join('0');
}

export const mockTeams: Array<Team> = [
  {
    id: makeObjectId('1'),
    city: 'New York',
    name: 'Yankees',
    abbreviation: 'NYY'
  },
  {
    id: makeObjectId('2'),
    city: 'Baltimore',
    name: 'Orioles',
    abbreviation: 'BAL'
  },
  {
    id: makeObjectId('3'),
    city: 'Boston',
    name: 'Red Sox',
    abbreviation: 'BOS'
  },
];

export const mockMatchups: Matchup[] = [
  {
    resource: mockTeams[0].id,
    opponent: mockTeams[2].id
  },
  {
    resource: mockTeams[2].id,
    opponent: mockTeams[0].id,
  }
];

const battingFieldsArr = [BATTING_FIELDS.get('hr'), BATTING_FIELDS.get('so'), BATTING_FIELDS.get('rbi')] as Field[];
export const mockStatGroups: Array<StatGroup> = [
  {
    id: makeObjectId('s1'),
    resource: makeObjectId('1'),
    stats: arrangeStats(battingFieldsArr, [56, 456, 125])
  },
  {
    id: makeObjectId('s2'),
    resource: makeObjectId('2'),
    stats: arrangeStats(battingFieldsArr, [28, 304, 405])
  },
  {
    id: makeObjectId('s3'),
    resource: makeObjectId('3'),
    stats: arrangeStats(battingFieldsArr, [25, 400, 248])
  },
  {
    id: makeObjectId('sm1'),
    resource: `${makeObjectId('1')}_${makeObjectId('3')}`,
    stats: arrangeStats(battingFieldsArr, [32, 94, 39])
  },
  {
    id: makeObjectId('sm2'),
    resource: `${makeObjectId('3')}_${makeObjectId('1')}`,
    stats: arrangeStats(battingFieldsArr, [18, 6, 78])
  }
];

export function arrangeStat(key: FieldKey, value: number): Stat {
  return {key, value};
}

export function arrangeStats(fields: Field[], values: number[]): Map<string, Stat> {
  let stats = new Map();
  for (let i = 0; i < fields.length; i++) {
    let stat = arrangeStat(makeFieldKey(Topic.RESOURCE, fields[i].category, fields[i].id), values[i]);
    stats.set(stat.key.flatKey, stat);
  }
  return stats;
}

export const mockFields = Array.from(INFO_FIELDS.values());
mockFields.push(...battingFieldsArr);