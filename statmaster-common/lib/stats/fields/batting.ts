import { Category, DerivedFieldDefinition } from '../types';

import { Field } from '..';

const fields: Array<Field> = [
  {
    id: 'avg',
    category: Category.BATTING,
    label: 'AVG',
    description: 'Batting Average',
    derived: true
  },
  {
    id: 'obp',
    category: Category.BATTING,
    label: 'OBP',
    description: 'On Base Percemtage',
    derived: true
  },
  {
    id: 'slg',
    category: Category.BATTING,
    label: 'SLG',
    description: 'Slugging Percemtage',
    derived: true
  },
  {
    id: 'ops',
    category: Category.BATTING,
    label: 'OPS',
    description: 'OBP + SLG',
    derived: true
  },
  {
    id: 'hr',
    category: Category.BATTING,
    label: 'HR',
    description: 'Home Runs',
  },
  {
    id: 'pa',
    category: Category.BATTING,
    label: 'PA',
    description: 'Plate Appearances',
  },
  {
    id: 'rbi',
    category: Category.BATTING,
    label: 'RBI',
    description: 'Runs Batted In',
  },
  {
    id: 'r',
    category: Category.BATTING,
    label: 'R',
    description: 'Runs',
  },
  {
    id: 'h',
    category: Category.BATTING,
    label: 'H',
    description: 'Hits',
  },
  {
    id: 'bb',
    category: Category.BATTING,
    label: 'BB',
    description: 'Walks',
  },
  {
    id: 'ab',
    category: Category.BATTING,
    label: 'AB',
    description: 'At Bats',
  },
  {
    id: '2b',
    category: Category.BATTING,
    label: '2B',
    description: 'Doubles',
  },
  {
    id: '3b',
    category: Category.BATTING,
    label: '3B',
    description: 'Triples',
  },
  {
    id: 'so',
    category: Category.BATTING,
    label: 'SO',
    description: 'Strike outs',
    negative: true,
  },
  {
    id: 'hbp',
    category: Category.BATTING,
    label: 'HBP',
    description: 'Hit By Pitch',
  },
  {
    id: 'sf',
    category: Category.BATTING,
    label: 'SF',
    description: 'Sac. Flies',
  },
  {
    id: 'tb',
    category: Category.BATTING,
    label: 'TB',
    description: 'Total Bases',
  },
  {
    id: 'xbh',
    category: Category.BATTING,
    label: 'XBH',
    description: 'Extra Base Hits',
  },
];

export const BATTING_FIELDS = new Map<string, Field>();
fields.forEach(f => BATTING_FIELDS.set(f.id, f));

const derivedFieldDefintitions: DerivedFieldDefinition[] = [
  {
    id: 'avg',
    category: Category.BATTING,
    equation: 'h / ab',
    variables: ['h', 'ab']
  },
  {
    id: 'obp',
    category: Category.BATTING,
    equation: '(h + bb + hbp) / (ab + bb + hbp + sf)',
    variables: ['h', 'bb', 'hbp', 'ab', 'sf']
  },
  {
    id: 'slg',
    category: Category.BATTING,
    equation: 'tb / ab',
    variables: ['tb', 'ab']
  },
  {
    id: 'ops',
    category: Category.BATTING,
    equation: 'slg + obp',
    variables: ['slg', 'obp']
  },
];

export const DERIVED_BATTING_DEFS = new Map<string, DerivedFieldDefinition>();
derivedFieldDefintitions.forEach(f => DERIVED_BATTING_DEFS.set(f.id, f));
