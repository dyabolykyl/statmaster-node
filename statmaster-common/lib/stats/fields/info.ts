import { Category } from '../types';
import { Field } from '..';

const fields: Array<Field> = [
  {
    id: 'city',
    category: Category.INFO,
    label: 'City',
    description: 'City',
  },
  {
    id: 'name',
    category: Category.INFO,
    label: 'Team',
    description: 'Team',
  },
  {
    id: 'abbreviation',
    category: Category.INFO,
    label: 'Team',
    description: 'Abbreviation',
  }
];

export const INFO_FIELDS = new Map<string, Field>();
fields.forEach(f => INFO_FIELDS.set(f.id, f));