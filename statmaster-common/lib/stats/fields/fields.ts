import * as math from 'mathjs';

import { BATTING_FIELDS, DERIVED_BATTING_DEFS } from './batting';
import { Category, DerivedFieldDefinition, FieldId, FieldKey, Stat, StatGroup, Topic } from '../types';

import { Field } from '..';
import { INFO_FIELDS } from './info';

export function makeFieldKey(topic: Topic, category: Category, fieldId: FieldId): FieldKey {
  return {
    topic,
    category,
    fieldId,
    flatKey: makeFlatKey(fieldId, category, topic)
  };
}

export const makeFlatKey = (fieldId: FieldId, category: Category, topic?: Topic): string => {
  return `${topic || Topic.RESOURCE}_${category}_${fieldId}`;
};

export function getField(category: Category, fieldId: string): Field | undefined {
  switch (category) {
    case Category.BATTING:
      return BATTING_FIELDS.get(fieldId);
    case Category.INFO:
      return INFO_FIELDS.get(fieldId);
    default:
      return undefined;
  }
}

export function calculateDerivedStat(definition: DerivedFieldDefinition, statGroup: StatGroup, topic: Topic) {
  let scope;

  try {
    scope = createScope(definition, statGroup, topic);
  } catch (err) {
    console.error(err);
    return;
  }

  const value = Number(math.eval(definition.equation, scope));
  if (isNaN(value) || value === null || value === undefined) {
    console.warn(`Derived stat produced NaN for ${statGroup.resource}: ${definition}`);
    return;
  }
  const stat: Stat = {
    key: makeFieldKey(topic, definition.category, definition.id),
    value: value
  };
  statGroup.stats.set(stat.key.flatKey, stat);

}

export function calculateDerivedStats(statGroup: StatGroup, topic: Topic) {
  DERIVED_BATTING_DEFS.forEach((definition, field) => {
    calculateDerivedStat(definition, statGroup, topic);
  });
}

export function createScope(definition: DerivedFieldDefinition, statGroup: StatGroup, topic: Topic): any {
  const scope: any = {};
  definition.variables.forEach(variable => {
    const flatKey = makeFlatKey(variable, definition.category, topic);
    const stat = statGroup.stats.get(flatKey);
    if (stat === undefined) {
      throw new Error(`Invalid scope for ${definition.id}, ${statGroup.resource}. Missing ${flatKey}`);
    } else {
      scope[variable] = stat.value;
    }
  });

  return scope;
}