import { Category, DerivedFieldDefinition, Stat, StatGroup, StatMap, Topic } from '../../lib/stats/types';
import { calculateDerivedStat, makeFieldKey } from '../../lib/stats/fields/fields';

let identifier = 1;

function arrangeDefinition(equation: string, variables: string[]): DerivedFieldDefinition {
  return {
    id: 'derived_stat' + identifier++,
    category: Category.BATTING,
    equation,
    variables
  };
}

function arrangeStat(field: string, value: number) {
  return {
    key: makeFieldKey(Topic.RESOURCE, Category.BATTING, field),
    value
  };
}

function arrangeStatGroup(...stats: Stat[]): StatGroup {
  const statMap = new Map();
  stats.forEach(stat => {
    statMap.set(stat.key.flatKey, stat);
  });
  return {
    id: 'id',
    resource: 'resource',
    stats: statMap
  };
}

describe('DerivedStats', () => {

  test('a', () => {
    const definition = arrangeDefinition('a', ['a']);
    const stat = arrangeStat('a', 45);
    const statGroup = arrangeStatGroup(stat);

    calculateDerivedStat(definition, statGroup, Topic.RESOURCE);

    const expectedFieldKey = makeFieldKey(Topic.RESOURCE, Category.BATTING, definition.id);
    expect(statGroup.stats.get(expectedFieldKey.flatKey)).toBeDefined();
    expect((statGroup.stats.get(expectedFieldKey.flatKey) as Stat).value).toEqual(45);
  });

  test('a + b', () => {
    const definition = arrangeDefinition('a + b', ['a', 'b']);
    const statGroup = arrangeStatGroup(arrangeStat('a', 45), arrangeStat('b', 10));

    calculateDerivedStat(definition, statGroup, Topic.RESOURCE);

    const expectedFieldKey = makeFieldKey(Topic.RESOURCE, Category.BATTING, definition.id);
    expect(statGroup.stats.get(expectedFieldKey.flatKey)).toBeDefined();
    expect((statGroup.stats.get(expectedFieldKey.flatKey) as Stat).value).toEqual(55);
  });

  test('a / b', () => {
    const definition = arrangeDefinition('a / b', ['a', 'b']);
    const statGroup = arrangeStatGroup(arrangeStat('a', 100), arrangeStat('b', 40));

    calculateDerivedStat(definition, statGroup, Topic.RESOURCE);

    const expectedFieldKey = makeFieldKey(Topic.RESOURCE, Category.BATTING, definition.id);
    expect(statGroup.stats.get(expectedFieldKey.flatKey)).toBeDefined();
    expect((statGroup.stats.get(expectedFieldKey.flatKey) as Stat).value).toEqual(2.5);
  });

});