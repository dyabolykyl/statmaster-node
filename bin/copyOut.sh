#!/bin/bash

function sync-common {
    target=$1
    targetDir="$target/node_modules/statmaster-common"
    rm -rf "$targetDir"
    mkdir -p "$targetDir"
    cp -r ./statmaster-common/package.json ./statmaster-common/dist "$targetDir"/
}

sync-common statmaster-server-v2
sync-common statmaster-client-v2
# rm -rf statmaster-common
