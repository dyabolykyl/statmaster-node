#!/bin/bash

. ~/.bash_profile

SESSION="statmaster"

cd "/d/Zack/Dev/StatMaster/statmaster-v2/"
tmux -2 new-session -d -s $SESSION

# New window for client
tmux new-window -t $SESSION:1 -n 'client'
tmux send-keys "cd statmaster-client-v2" C-m
tmux split-window -h -c "#{pane_current_path}" 
tmux select-pane -t 0
tmux send-keys "npm test" C-m
tmux select-pane -t 1
tmux send-keys "cd statmaster-client-v2" C-m
tmux send-keys "npm start" C-m

# New window for server
tmux new-window -t $SESSION:2 -n 'server'
tmux send-keys "cd statmaster-server-v2" C-m
tmux split-window -h -c "#{pane_current_path}" 
tmux select-pane -t 0
tmux send-keys "PORT=3001 npm run start:dev" C-m
tmux split-window -v -c "#{pane_current_path}"
tmux select-pane -t 2
tmux send-keys "cd statmaster-server-v2" C-m
tmux send-keys "npm test" C-m

# Set default wind
tmux select-window -t $SESSION:0
tmux split-window -h
tmux select-pane -t 1
tmux send-keys "cd statmaster-common" C-m

# Attach to session
tmux -2 attach-session -t statmaster
